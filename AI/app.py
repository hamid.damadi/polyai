
import services.HTR.index as TextRecognition
import services.EmotionRecognition.index as EmotionRecognition
import services.Sen.index as Sent
import services.CGan.index as CGan
import services.NER.index as NER
import services.ImageRecognition.index as ImageRecoginition
import services.ImageCaptioning.index as ImageCaptioning
import services.ImageObjectDetection.index as ImageObjectDetection
import asyncio
import tensorflow as tf
from flask import Flask, request, jsonify
import keras_cv

# from waitress import serve

app = Flask(__name__)

SHARE_FOLDER_WITH_NODE = '../server/images/temp/services/'

####################################################################
####################################################################
model_htr_iam = None


async def promise_all(funcs):
    tasks = [asyncio.create_task(func()) for func in funcs]
    return [await task for task in tasks]


async def model_htr_iam_func():
    model = tf.keras.models.load_model(
        f"./models/prediction_htr_iam_cpu.keras", compile=False)
    return model


async def model_mnist_func():
    model = tf.keras.models.load_model(
        f"./models/tf_serving_keras_mobilenetv2.h5", safe_mode=False, compile=False)
    return model


async def model_tf_sentiment_func():
    model = tf.keras.models.load_model(
        f"models/tf_sentiment.h5", custom_objects={'EmbeddingLayer': Sent.EmbeddingLayer}, compile=False)
    return model


async def model_emotion_func():
    model = tf.keras.models.load_model(
        f"./models/emotion_model.keras", safe_mode=False, compile=False)
    return model


async def model_tf_ner_func():
    model = tf.keras.models.load_model(f"models/tf_ner.h5", compile=False)
    return model


async def model_gen_cgan_mnist_func():
    model = tf.keras.models.load_model(
        f"models/gen-cgan-mnist.h5", compile=False)
    return model


async def model_image_captioning_coco_func():
    model = tf.keras.models.load_model(
        f"models/image_captioning_coco.keras", safe_mode=False, compile=False,
        custom_objects={'ImageCaptioningModel': ImageCaptioning.ImageCaptioningModel})
    return model


async def model_image_object_detection_func():
    model = keras_cv.models.YOLOV8Detector.from_preset(
        "yolo_v8_m_pascalvoc", bounding_box_format="xywh"
    )
    return model

model_htr_iam, model_mnist, model_tf_sentiment, model_emotion, model_tf_ner, model_gen_cgan_mnist, model_image_captioning_coco, model_image_object_detection = asyncio.run(
    promise_all([
        model_htr_iam_func,
        model_mnist_func,
        model_tf_sentiment_func,
        model_emotion_func,
        model_tf_ner_func,
        model_gen_cgan_mnist_func,
        model_image_captioning_coco_func,
        model_image_object_detection_func
    ]))

#########################################################################
#########################################################################


@app.route('/imageRecognitionPredict', methods=['POST'])
def imageRecognitionPredict():
    result = []
    try:
        imageDet = ImageRecoginition.ImageRecognitionService(
            model_mnist, SHARE_FOLDER_WITH_NODE)
        data = request.get_json()
        index = data['index']
        result = imageDet.predict(index)
    except:
        print("something is wrong!")

    return jsonify({
        "prediction": result[0].tolist(),
        "argMax": result[1].tolist()
    })


@app.route('/imageRecognitionRandomSelections', methods=['GET'])
def imageRecognitionRandomSelections():
    result = []
    try:
        imageDet = ImageRecoginition.ImageRecognitionService(
            model_mnist, SHARE_FOLDER_WITH_NODE)
        result = imageDet.getSamples(4)
    except:
        print("something is wrong!")

    return jsonify({"result": result})


@app.route('/getSamplesNER', methods=['GET'])
def getSamplesNER():
    ner = NER.NERService(model_tf_ner)
    result = []
    # try:
    result = ner.getSamples()
    # except:
    #     print("something is wrong!")

    return jsonify({
        "sentence": result[0],
        "index": result[1]
    })


@app.route('/NERPredict', methods=['POST'])
def NERPredict():
    ner = NER.NERService(model_tf_ner)
    result = []
    try:
        data = request.get_json()
        index = data['index']
        result = ner.predict(index)
    except:
        print("something is wrong!")

    return jsonify({
        "prediction": result
    })


@app.route('/cGanPredict', methods=['POST'])
def cGanPredict():
    cGan = CGan.CGanService(model_gen_cgan_mnist, SHARE_FOLDER_WITH_NODE)
    result = []
    try:
        data = request.get_json()
        index = data['index']
        result = cGan.predict(index)
    except:
        print("something is wrong!")

    return jsonify({"result": result})


@app.route('/sentPredict', methods=['POST'])
def sentPredict():
    sent = Sent.SentService(model_tf_sentiment)
    result = []
    try:
        data = request.get_json()
        sentence = data['sentence']
        result = sent.predict(sentence)
        print(result)
    except:
        print("something is wrong!")

    return jsonify({"result": result})


@app.route('/emotionRecognitionPredict', methods=['POST'])
def emotionRecognitionPredict():
    emotionRecognition = EmotionRecognition.EmotionRecognitionService(
        model_emotion,
        SHARE_FOLDER_WITH_NODE)
    result = []
    try:
        data = request.get_json()
        path = data['path']
        result = emotionRecognition.predict(path)
        print(result)
    except:
        print("something is wrong!")

    return jsonify({"result": result})


@app.route('/textRecognitionPredict', methods=['POST'])
def textRecognitionPredict():
    result = []
    try:
        htr = TextRecognition.TextRecognitionService(
            model_htr_iam, SHARE_FOLDER_WITH_NODE)
        data = request.get_json()
        path = data['path']
        result = htr.predict(path)
    except:
        print("something is wrong!")

    return jsonify({
        "prediction": result
    })


@app.route('/imageCaptioningPredict', methods=['POST'])
def imageCaptioningPredict():
    imageCaptioning = ImageCaptioning.ImageCaptioningService(
        model_image_captioning_coco,
        SHARE_FOLDER_WITH_NODE)
    result = []
    try:
        data = request.get_json()
        path = data['path']
        result = imageCaptioning.predict(path)
    except:
        print("something is wrong!")

    return jsonify({"result": result})


@app.route('/imageObjectDetectionPredict', methods=['POST'])
def imageObjectDetectionPredict():
    imageObjectDetection = ImageObjectDetection.ImageObjectDetectionService(
        model_image_object_detection,
        SHARE_FOLDER_WITH_NODE)
    result = []
    try:
        data = request.get_json()
        path = data['path']
        result = imageObjectDetection.predict(path)
    except:
        print("something is wrong!")

    return jsonify({"result": result})


if __name__ == "__main__":
    # serve(app, host="0.0.0.0", port=5000)
    app.run(port=5000, debug=True)
