"""
Image Captioning Model using Transformer Architecture.
The code is inspired by the architecture presented in the Keras Image Captioning example
(source: "https://keras.io/examples/vision/handwriting_recognition/").

This script defines and trains an image captioning model using a transformer
architecture with TensorFlow and Keras. It includes data preprocessing, model
architecture, training, and evaluation.
"""

import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
import re
import os
from pycocotools.coco import COCO


os.environ["KERAS_BACKEND"] = "tensorflow"


tf.keras.utils.set_random_seed(42)

# Desired image dimensions
IMAGE_SIZE = (299, 299)

# Vocabulary size
VOCAB_SIZE = 10000

# Fixed length allowed for any sequence
SEQ_LENGTH = 25

# Dimension for the image embeddings and token embeddings
EMBED_DIM = 512

# Per-layer units in the feed-forward network
FF_DIM = 512

# Other training parameters
BATCH_SIZE = 64
EPOCHS = 40
AUTOTUNE = tf.data.AUTOTUNE


@tf.keras.saving.register_keras_serializable(package="my_package", name="TransformerEncoderBlock")
class TransformerEncoderBlock(tf.keras.layers.Layer):
    """
    Transformer Encoder Block.

    Args:
        embed_dim: Dimension of the embedding.
        dense_dim: Dimension of the dense layer.
        num_heads: Number of attention heads.
    """

    def __init__(self, embed_dim, dense_dim, num_heads, **kwargs):
        super().__init__(**kwargs)
        self.embed_dim = embed_dim
        self.dense_dim = dense_dim
        self.num_heads = num_heads
        self.attention_1 = tf.keras.layers.MultiHeadAttention(
            num_heads=num_heads, key_dim=embed_dim, dropout=0.0
        )
        self.layernorm_1 = tf.keras.layers.LayerNormalization()
        self.layernorm_2 = tf.keras.layers.LayerNormalization()
        self.dense_1 = tf.keras.layers.Dense(embed_dim, activation="relu")

    def call(self, inputs, training, mask=None):
        """
        Forward pass for the Transformer Encoder Block.

        Args:
            inputs: Input tensor.
            training: Boolean, whether the model is training.
            mask: Masking tensor.

        Returns:
            Tensor after passing through the Transformer Encoder Block.
        """
        inputs = self.layernorm_1(inputs)
        inputs = self.dense_1(inputs)

        attention_output_1 = self.attention_1(
            query=inputs,
            value=inputs,
            key=inputs,
            attention_mask=None,
            training=training,
        )
        out_1 = self.layernorm_2(inputs + attention_output_1)
        return out_1

    def get_config(self):
        """
        Get the configuration of the layer.

        Returns:
            Configuration dictionary.
        """
        config = super().get_config()
        config.update({
            'embed_dim': self.embed_dim,
            'dense_dim': self.dense_dim,
            'num_heads': self.num_heads,
        })
        return config

    @classmethod
    def from_config(cls, config):
        """
        Create layer from config.

        Args:
            config: Configuration dictionary.

        Returns:
            Instance of the layer.
        """
        return cls(**config)


@tf.keras.saving.register_keras_serializable(package="my_package", name="PositionalEmbedding")
class PositionalEmbedding(tf.keras.layers.Layer):
    """
    Positional Embedding layer for Transformer.

    Args:
        sequence_length: Length of the input sequence.
        vocab_size: Vocabulary size.
        embed_dim: Dimension of the embedding.
    """

    def __init__(self, sequence_length, vocab_size, embed_dim, **kwargs):
        super().__init__(**kwargs)
        self.token_embeddings = tf.keras.layers.Embedding(
            input_dim=vocab_size, output_dim=embed_dim
        )
        self.position_embeddings = tf.keras.layers.Embedding(
            input_dim=sequence_length, output_dim=embed_dim
        )
        self.sequence_length = sequence_length
        self.vocab_size = vocab_size
        self.embed_dim = embed_dim
        self.embed_scale = tf.math.sqrt(tf.cast(embed_dim, tf.float32))

    def call(self, inputs):
        """
        Forward pass for the Positional Embedding layer.

        Args:
            inputs: Input tensor.

        Returns:
            Positionally embedded tensor.
        """
        length = tf.shape(inputs)[-1]
        positions = tf.range(start=0, limit=length, delta=1)
        embedded_tokens = self.token_embeddings(inputs)
        embedded_tokens = embedded_tokens * self.embed_scale
        embedded_positions = self.position_embeddings(positions)
        return embedded_tokens + embedded_positions

    def compute_mask(self, inputs, mask=None):
        """
        Compute mask for the input tensor.

        Args:
            inputs: Input tensor.
            mask: Existing mask.

        Returns:
            Computed mask.
        """
        return tf.math.not_equal(inputs, 0)

    def get_config(self):
        """
        Get the configuration of the layer.

        Returns:
            Configuration dictionary.
        """
        config = super().get_config()
        config.update({
            'sequence_length': self.sequence_length,
            'vocab_size': self.vocab_size,
            'embed_dim': self.embed_dim,
        })
        return config

    @classmethod
    def from_config(cls, config):
        """
        Create layer from config.

        Args:
            config: Configuration dictionary.

        Returns:
            Instance of the layer.
        """
        return cls(**config)


@tf.keras.saving.register_keras_serializable(package="my_package", name="TransformerDecoderBlock")
class TransformerDecoderBlock(tf.keras.layers.Layer):
    """
    Transformer Decoder Block.

    Args:
        embed_dim: Dimension of the embedding.
        ff_dim: Dimension for the feed-forward network.
        num_heads: Number of attention heads.
    """

    def __init__(self, embed_dim, ff_dim, num_heads, **kwargs):
        super().__init__(**kwargs)
        self.embed_dim = embed_dim
        self.ff_dim = ff_dim
        self.num_heads = num_heads
        self.attention_1 = tf.keras.layers.MultiHeadAttention(
            num_heads=num_heads, key_dim=embed_dim, dropout=0.1
        )
        self.attention_2 = tf.keras.layers.MultiHeadAttention(
            num_heads=num_heads, key_dim=embed_dim, dropout=0.1
        )
        self.ffn_layer_1 = tf.keras.layers.Dense(ff_dim, activation="relu")
        self.ffn_layer_2 = tf.keras.layers.Dense(embed_dim)

        self.layernorm_1 = tf.keras.layers.LayerNormalization()
        self.layernorm_2 = tf.keras.layers.LayerNormalization()
        self.layernorm_3 = tf.keras.layers.LayerNormalization()

        self.embedding = PositionalEmbedding(
            embed_dim=EMBED_DIM,
            sequence_length=SEQ_LENGTH,
            vocab_size=VOCAB_SIZE,
        )
        self.out = tf.keras.layers.Dense(VOCAB_SIZE, activation="softmax")

        self.dropout_1 = tf.keras.layers.Dropout(0.3)
        self.dropout_2 = tf.keras.layers.Dropout(0.5)
        self.supports_masking = True

    def call(self, inputs, encoder_outputs, training, mask=None):
        """
        Forward pass for the Transformer Decoder Block.

        Args:
            inputs: Input tensor.
            encoder_outputs: Outputs from the encoder.
            training: Whether the model is in training mode.
            mask: Masking tensor.

        Returns:
            Predictions.
        """
        inputs = self.embedding(inputs)
        causal_mask = self.get_causal_attention_mask(inputs)
        combined_mask = None  # Initialize combined_mask
        if mask is not None:
            padding_mask = tf.cast(mask[:, :, tf.newaxis], dtype=tf.int32)
            combined_mask = tf.cast(mask[:, tf.newaxis, :], dtype=tf.int32)
            combined_mask = tf.minimum(combined_mask, causal_mask)

        attention_output_1 = self.attention_1(
            query=inputs,
            value=inputs,
            key=inputs,
            attention_mask=combined_mask,
            training=training,
        )
        out_1 = self.layernorm_1(inputs + attention_output_1)

        attention_output_2 = self.attention_2(
            query=out_1,
            value=encoder_outputs,
            key=encoder_outputs,
            attention_mask=padding_mask,
            training=training,
        )
        out_2 = self.layernorm_2(out_1 + attention_output_2)

        ffn_out = self.ffn_layer_1(out_2)
        ffn_out = self.dropout_1(ffn_out, training=training)
        ffn_out = self.ffn_layer_2(ffn_out)

        ffn_out = self.layernorm_3(ffn_out + out_2, training=training)
        ffn_out = self.dropout_2(ffn_out, training=training)
        preds = self.out(ffn_out)
        return preds

    def get_causal_attention_mask(self, inputs):
        """
        Generate causal attention mask.

        Args:
            inputs: Input tensor.

        Returns:
            Causal attention mask.
        """
        input_shape = tf.shape(inputs)
        batch_size, sequence_length = input_shape[0], input_shape[1]
        i = tf.range(sequence_length)[:, tf.newaxis]
        j = tf.range(sequence_length)
        mask = tf.cast(i >= j, dtype="int32")
        mask = tf.reshape(mask, (1, input_shape[1], input_shape[1]))
        mult = tf.concat(
            [
                tf.expand_dims(batch_size, -1),
                tf.constant([1, 1], dtype=tf.int32),
            ],
            axis=0,
        )
        return tf.tile(mask, mult)

    def get_config(self):
        """
        Get the configuration of the layer.

        Returns:
            Configuration dictionary.
        """
        config = super().get_config()
        config.update({
            'embed_dim': self.embed_dim,
            'ff_dim': self.ff_dim,
            'num_heads': self.num_heads,
        })
        return config

    @classmethod
    def from_config(cls, config):
        """
        Create layer from config.

        Args:
            config: Configuration dictionary.

        Returns:
            Instance of the layer.
        """
        return cls(**config)


def make_image_augmentation():
    """
    Create an image augmentation model.

    Returns:
        tf.keras.Sequential: Image augmentation model.
    """
    # Data augmentation for image data
    image_augmentation = tf.keras.Sequential(
        [
            tf.keras.layers.RandomFlip("horizontal"),
            tf.keras.layers.RandomRotation(0.2),
            tf.keras.layers.RandomContrast(0.3),
        ]
    )
    return image_augmentation


def get_cnn_model():
    """
    Build and return the CNN (Convolutional Neural Network) model based on EfficientNetB0.

    Returns:
        tf.keras.Model: CNN model.
    """
    base_model = tf.keras.applications.efficientnet.EfficientNetB0(
        input_shape=(*IMAGE_SIZE, 3),
        include_top=False,
        weights="imagenet",
    )
    # We freeze our feature extractor
    base_model.trainable = False
    base_model_out = base_model.output
    base_model_out = tf.keras.layers.Reshape(
        (-1, base_model_out.shape[-1]))(base_model_out)
    cnn_model = tf.keras.models.Model(base_model.input, base_model_out)
    return cnn_model


def get_encoder():
    """
    Build and return the Transformer Encoder block.

    Returns:
        TransformerEncoderBlock: Instance of the TransformerEncoderBlock.
    """
    encoder = TransformerEncoderBlock(
        embed_dim=EMBED_DIM, dense_dim=FF_DIM, num_heads=1, name='encoder_layer')
    return encoder


def get_decoder():
    """
    Build and return the Transformer Decoder block.

    Returns:
        TransformerDecoderBlock: Instance of the TransformerDecoderBlock.
    """
    decoder = TransformerDecoderBlock(
        embed_dim=EMBED_DIM, ff_dim=FF_DIM, num_heads=2, name='decoder_layer')
    return decoder


def custom_standardization(input_string):
    """
    Perform custom standardization on the input string.

    Args:
        input_string (tf.Tensor): Input string to be standardized.

    Returns:
        tf.Tensor: Standardized string.
    """
    lowercase = tf.strings.lower(input_string)
    strip_chars = "!\"#$%&'()*+,-./:;<=>?@[\]^_`{|}~"
    strip_chars = strip_chars.replace("<", "")
    strip_chars = strip_chars.replace(">", "")
    return tf.strings.regex_replace(lowercase, "[%s]" % re.escape(strip_chars), "")


def get_text_vectorization():
    """
    Build and return the TextVectorization layer for processing text data.

    Returns:
        tf.keras.layers.TextVectorization: Instance of the TextVectorization layer.
    """
    vectorization = tf.keras.layers.TextVectorization(
        max_tokens=VOCAB_SIZE,
        output_mode="int",
        output_sequence_length=SEQ_LENGTH,
        standardize=custom_standardization,
        name='vectorization'
    )
    return vectorization


@tf.keras.saving.register_keras_serializable(package="my_package", name="ImageCaptioningModel")
class ImageCaptioningModel(tf.keras.Model):
    def __init__(
        self,
        cnn_model,
        encoder,
        decoder,
        vectorization,
        num_captions_per_image=5,
        image_aug=None,
    ):
        """
        Initialize the ImageCaptioningModel.

        Args:
            cnn_model (tf.keras.Model): CNN model for image feature extraction.
            encoder (TransformerEncoderBlock): Transformer encoder.
            decoder (TransformerDecoderBlock): Transformer decoder.
            vectorization (tf.keras.layers.TextVectorization): Text vectorization layer.
            num_captions_per_image (int): Number of captions generated per image.
            image_aug (tf.keras.Sequential): Image augmentation model.
        """
        super().__init__()
        self.cnn_model = cnn_model
        self.encoder = encoder
        self.decoder = decoder
        self.vectorization = vectorization
        self.loss_tracker = tf.keras.metrics.Mean(name="loss")
        self.acc_tracker = tf.keras.metrics.Mean(name="accuracy")
        self.num_captions_per_image = num_captions_per_image
        self.image_aug = image_aug

    def build(self, input_shape):
        """
        Build the model.

        Args:
            input_shape (tuple): Input shape.
        """
        if (not self.image_aug.layers or len(self.image_aug.layers) == 0):
            image_augmentation_model = make_image_augmentation()
            image_augmentation_model.build(input_shape=input_shape[0])
            self.image_aug = image_augmentation_model
            cnn_model = get_cnn_model()
            cnn_model.build(input_shape=input_shape[0])
            self.cnn_model = cnn_model
            encoder = get_encoder()
            encoder.build(input_shape=(None, 100, 1280))
            self.encoder = encoder
            decoder = get_decoder()
            decoder.build((None, 100, 512))
            self.decoder = decoder
            vectorization = get_text_vectorization()
            vectorization.build((None, 100, 512))
            self.vectorization = vectorization
        # Call the build method of the parent class
        super().build(input_shape)

    def call(self, inputs, training=None, mask=None):
        """
        Forward pass of the model.

        Args:
            inputs (tuple): Tuple containing image and captions.
            training (bool): Whether the model is in training mode.
            mask: Not used in this model.

        Returns:
            tf.Tensor: Output tensor.
        """
        image = inputs[0]
        captions = inputs[1]

        x = self.image_aug(image, training=training)
        x = self.cnn_model(image, training=training)
        encoder_outputs = self.encoder(x, training=training)
        tokenized_caption = self.vectorization(captions)
        mask = tf.math.not_equal(tokenized_caption, 0)
        outputs = self.decoder(
            tokenized_caption, encoder_outputs, training=training, mask=mask)

        return outputs

    def calculate_loss(self, y_true, y_pred, mask):
        """
        Calculate the loss.

        Args:
            y_true (tf.Tensor): True labels.
            y_pred (tf.Tensor): Predicted labels.
            mask (tf.Tensor): Mask tensor.

        Returns:
            tf.Tensor: Loss value.
        """
        loss = self.loss(y_true, y_pred)
        mask = tf.cast(mask, dtype=loss.dtype)
        loss *= mask
        return tf.reduce_sum(loss) / tf.reduce_sum(mask)

    def calculate_accuracy(self, y_true, y_pred, mask):
        """
        Calculate the accuracy.

        Args:
            y_true (tf.Tensor): True labels.
            y_pred (tf.Tensor): Predicted labels.
            mask (tf.Tensor): Mask tensor.

        Returns:
            tf.Tensor: Accuracy value.
        """
        accuracy = tf.equal(y_true, tf.argmax(y_pred, axis=2))
        accuracy = tf.math.logical_and(mask, accuracy)
        accuracy = tf.cast(accuracy, dtype=tf.float32)
        mask = tf.cast(mask, dtype=tf.float32)
        return tf.reduce_sum(accuracy) / tf.reduce_sum(mask)

    def _compute_caption_loss_and_acc(self, img_embed, batch_seq, training=True):
        """
        Compute loss and accuracy for a single caption.

        Args:
            img_embed (tf.Tensor): Image embeddings.
            batch_seq (tf.Tensor): Tokenized caption.
            training (bool): Whether the model is in training mode.

        Returns:
            tuple: Loss and accuracy values.
        """
        encoder_out = self.encoder(img_embed, training=training)
        batch_seq_inp = batch_seq[:, :-1]
        batch_seq_true = batch_seq[:, 1:]
        mask = tf.math.not_equal(batch_seq_true, 0)
        batch_seq_pred = self.decoder(
            batch_seq_inp, encoder_out, training=training, mask=mask
        )
        loss = self.calculate_loss(batch_seq_true, batch_seq_pred, mask)
        acc = self.calculate_accuracy(batch_seq_true, batch_seq_pred, mask)
        return loss, acc

    def train_step(self, batch_data):
        """
        Training step.

        Args:
            batch_data: Batch data containing images and captions.

        Returns:
            dict: Dictionary containing loss and accuracy values.
        """
        batch_img, batch_seq = batch_data
        batch_loss = 0
        batch_acc = 0

        if self.image_aug:
            batch_img = self.image_aug(batch_img)

        # 1. Get image embeddings
        img_embed = self.cnn_model(batch_img)

        # 2. Pass each of the five captions one by one to the decoder
        # along with the encoder outputs and compute the loss as well as accuracy
        # for each caption.
        for i in range(self.num_captions_per_image):
            with tf.GradientTape() as tape:
                loss, acc = self._compute_caption_loss_and_acc(
                    img_embed, batch_seq[:, i, :], training=True
                )

                # 3. Update loss and accuracy
                batch_loss += loss
                batch_acc += acc

            # 4. Get the list of all the trainable weights
            train_vars = (
                self.encoder.trainable_variables + self.decoder.trainable_variables
            )

            # 5. Get the gradients
            grads = tape.gradient(loss, train_vars)

            # 6. Update the trainable weights
            self.optimizer.apply_gradients(zip(grads, train_vars))

        # 7. Update the trackers
        batch_acc /= self.num_captions_per_image
        self.loss_tracker.update_state(batch_loss)
        self.acc_tracker.update_state(batch_acc)

        # 8. Return the loss and accuracy values
        return {
            "loss": self.loss_tracker.result(),
            "acc": self.acc_tracker.result(),
        }

    def test_step(self, batch_data):
        """
        Testing step.

        Args:
            batch_data: Batch data containing images and captions.

        Returns:
            dict: Dictionary containing loss and accuracy values.
        """
        batch_img, batch_seq = batch_data
        batch_loss = 0
        batch_acc = 0

        # 1. Get image embeddings
        img_embed = self.cnn_model(batch_img, training=False)

        # 2. Pass each of the five captions one by one to the decoder
        # along with the encoder outputs and compute the loss as well as accuracy
        # for each caption.
        for i in range(self.num_captions_per_image):
            loss, acc = self._compute_caption_loss_and_acc(
                img_embed, batch_seq[:, i, :], training=False
            )

            # 3. Update loss and accuracy
            batch_loss += loss
            batch_acc += acc

        # 4. Update the trackers
        batch_acc /= self.num_captions_per_image
        self.loss_tracker.update_state(batch_loss)
        self.acc_tracker.update_state(batch_acc)

        # 5. Return the loss and accuracy values
        return {
            "loss": self.loss_tracker.result(),
            "acc": self.acc_tracker.result(),
        }

    def compute_output_shape(self, input_shape):
        """
        Compute the output shape.

        Args:
            input_shape (tuple): Input shape.

        Returns:
            tuple: Output shape.
        """
        return input_shape

    def get_config(self):
        """
        Get the configuration of the model.

        Returns:
            dict: Configuration dictionary.
        """
        config = super().get_config()
        config.update({
            'cnn_model': self.cnn_model,
            'encoder': self.encoder,
            'decoder': self.decoder,
            'vectorization': self.vectorization,
            'num_captions_per_image': self.num_captions_per_image,
            'image_aug': self.image_aug
        })
        return config

    @classmethod
    def from_config(cls, config):
        """
        Create an instance of the model from its config.

        Args:
            config (dict): Configuration dictionary.

        Returns:
            ImageCaptioningModel: Instance of the ImageCaptioningModel.
        """
        return cls(**config)


@tf.keras.saving.register_keras_serializable(package="my_package", name="LearningRateSchedule")
class LRSchedule(tf.keras.optimizers.schedules.LearningRateSchedule):
    def __init__(self, post_warmup_learning_rate, warmup_steps):
        """
        Initialize the learning rate schedule.

        Args:
            d_model (int): Dimensionality of the model.
            warmup_steps (int): Number of warmup steps.
        """
        super().__init__()
        self.post_warmup_learning_rate = post_warmup_learning_rate
        self.warmup_steps = warmup_steps

    def __call__(self, step):
        """
        Call method to compute the learning rate for a given step.

        Args:
            step (tf.Tensor): Current training step.

        Returns:
            tf.Tensor: Learning rate for the given step.
        """
        global_step = tf.cast(step, tf.float32)
        warmup_steps = tf.cast(self.warmup_steps, tf.float32)
        warmup_progress = global_step / warmup_steps
        warmup_learning_rate = self.post_warmup_learning_rate * warmup_progress
        return tf.cond(
            global_step < warmup_steps,
            lambda: warmup_learning_rate,
            lambda: self.post_warmup_learning_rate,
        )

    def get_config(self):
        """
        Get the configuration of the learning rate schedule.

        Returns:
            dict: Configuration dictionary.
        """
        return {
            'post_warmup_learning_rate': self.post_warmup_learning_rate,
            'warmup_steps': self.warmup_steps
        }

    @classmethod
    def from_config(cls, config):
        """
        Create an instance of the learning rate schedule from its config.

        Args:
            config (dict): Configuration dictionary.

        Returns:
            LRSchedule: Instance of the LRSchedule.
        """
        return cls(**config)


class ImageCaptioningService():
    def __init__(self):
        """
        Initialize the ImageCaptioningService.

        Attributes:
            ANNOTATIONS_FILE (str): Path to the COCO annotations file.
            IMAGES_DIR (str): Path to the directory containing images.
            TEST_ANNOTATIONS_FILE (str): Path to the COCO test annotations file.
            TEST_IMAGES_DIR (str): Path to the directory containing test images.
            MODEL_NAME (str): Name of the model.
            MODEL_OUTPUT_PATH (str): Path to save the trained model.
            text_data (list): List to store captions data.
            training_data (dict): Dictionary to store training data.
            validation_data (dict): Dictionary to store validation data.
            model (ImageCaptioningModel): Image captioning model.
            train_dataset (tf.data.Dataset): Training dataset.
            valid_dataset (tf.data.Dataset): Validation dataset.
            test_dataset (tf.data.Dataset): Test dataset.
            vectorization (tf.keras.layers.TextVectorization): Text vectorization layer.
        """
        # Set the path to the COCO annotations file
        self.ANNOTATIONS_FILE = '### ANNOTATIONS_FILE ###'
        self.IMAGES_DIR = '### IMAGES_DIR ###'
        self.TEST_ANNOTATIONS_FILE = '### TEST_ANNOTATIONS_FILE ###'
        self.TEST_IMAGES_DIR = '### TEST_IMAGES_DIR ###'
        self.MODEL_NAME = '### MODEL_NAME ###'
        self.MODEL_OUTPUT_PATH = '### MODEL_OUTPUT_PATH ###'
        self.text_data = []
        self.training_data = {}
        self.validation_data = {}
        self.model = None
        self.train_dataset = None
        self.valid_dataset = None
        self.test_dataset = None
        self.vectorization = None

    def load_captions_data(self, ann_file, img_dir, isTest=False):
        """
        Load captions data from the COCO annotations file and image directory.

        Parameters:
            ann_file (str): Path to the COCO annotations file.
            img_dir (str): Path to the directory containing images.
            isTest (bool): Whether loading data for testing (default is False).

        Returns:
            caption_mapping (dict): Mapping of image names to lists of captions.
        """
        # Initialize the COCO API for captions
        coco = COCO(ann_file)
        # Load image ids and captions
        image_ids = coco.getImgIds()
        caption_mapping = {}
        text_data = []
        images_to_skip = set()
        for imgid in image_ids:
            img = coco.loadImgs(imgid)[0]
            img_name = img['file_name'].strip()
            img_name = os.path.join(img_dir, img_name)

            if os.path.isfile(img_name) and os.path.getsize(img_name):
                annIds = coco.getAnnIds(imgIds=img['id'])
                anns = coco.loadAnns(annIds)
                for ann in anns:
                    caption = ann['caption'].strip()
                    tokens = caption.split()
                    if len(tokens) < 5 or len(tokens) > SEQ_LENGTH:
                        images_to_skip.add(img_name)
                        continue
                    if img_name.endswith("jpg") and img_name not in images_to_skip:
                        caption = "<start> " + caption + " <end>"
                        if img_name in caption_mapping:
                            caption_mapping[img_name].append(caption)
                        else:
                            caption_mapping[img_name] = [caption]
                        if (not isTest):
                            text_data.append(caption)

        for img_name in images_to_skip:
            if img_name in caption_mapping:
                del caption_mapping[img_name]

        if (not isTest):
            self.text_data = text_data
        return caption_mapping

    def train_val_split(self, caption_data, train_size=0.8, shuffle=True):
        """
        Split the caption data into training and validation sets.

        Parameters:
            caption_data (dict): Mapping of image names to lists of captions.
            train_size (float): Proportion of data to use for training (default is 0.8).
            shuffle (bool): Whether to shuffle the data (default is True).
        """
        all_images = list(caption_data.keys())
        if shuffle:
            np.random.shuffle(all_images)
        train_size = int(len(caption_data) * train_size)

        self.training_data = {
            img_name: caption_data[img_name] for img_name in all_images[:train_size]
        }
        self.validation_data = {
            img_name: caption_data[img_name] for img_name in all_images[train_size:]
        }

    def clean_unbalanced_captions(self, caption_mapping):
        """
        Remove images with an unbalanced number of captions.

        Parameters:
            caption_mapping (dict): Mapping of image names to lists of captions.

        Returns:
            cleaned_mapping (dict): Mapping after removing unbalanced captions.
        """
        unbalanced_captions = []
        for x in caption_mapping:
            if (len(caption_mapping[x]) != 5):
                unbalanced_captions.append(x)
        for y in unbalanced_captions:
            del (caption_mapping[y])
        return caption_mapping

    def prepare_dataset(self):
        """
        Prepare the dataset by loading captions data, splitting into train-validation sets,
        and cleaning unbalanced captions.
        """
        caption_mapping = self.load_captions_data(
            self.ANNOTATIONS_FILE, self.IMAGES_DIR)
        caption_mapping = self.clean_unbalanced_captions(caption_mapping)
        self.train_val_split(caption_mapping)

        print("Number of training samples: ", len(self.training_data))
        print("Number of validation samples: ", len(self.validation_data))

    def decode_and_resize(self, img_path):
        """
        Decode and resize an image from the given path.

        Parameters:
            img_path (str): Path to the image.

        Returns:
            img (tf.Tensor): Decoded and resized image.
        """
        img = tf.io.read_file(img_path)
        img = tf.image.decode_jpeg(img, channels=3)
        img = tf.image.resize(img, IMAGE_SIZE)
        img = tf.image.convert_image_dtype(img, tf.float32)
        return img

    def process_input(self, img_path, captions):
        """
        Process input by decoding and resizing the image.

        Parameters:
            img_path (str): Path to the image.
            captions (str): Captions associated with the image.

        Returns:
            img (tf.Tensor): Decoded and resized image.
            vectorization (tf.Tensor): Vectorized captions.
        """
        return self.decode_and_resize(img_path), self.vectorization(captions)

    def make_dataset(self, images, captions):
        """
        Create a dataset from the given images and captions.

        Parameters:
            images (list): List of image paths.
            captions (list): List of captions corresponding to the images.

        Returns:
            dataset (tf.data.Dataset): Processed dataset.
        """
        dataset = tf.data.Dataset.from_tensor_slices((images, captions))
        dataset = dataset.shuffle(BATCH_SIZE * 8)
        dataset = dataset.map(self.process_input, num_parallel_calls=AUTOTUNE)
        dataset = dataset.batch(BATCH_SIZE).prefetch(AUTOTUNE)

        return dataset

    def generator(self):
        """
        Generate training and validation datasets.
        """
        self.vectorization = get_text_vectorization()
        self.vectorization.adapt(self.text_data)
        # Pass the list of images and the list of corresponding captions
        self.train_dataset = self.make_dataset(
            list(self.training_data.keys()), list(self.training_data.values()))

        self.valid_dataset = self.make_dataset(
            list(self.validation_data.keys()), list(self.validation_data.values()))

    def build_model(self):
        """
        Build the image captioning model.
        """
        self.cnn_model = get_cnn_model()
        self.encoder = get_encoder()
        self.decoder = get_decoder()
        self.image_augmentation = make_image_augmentation()

        caption_model = ImageCaptioningModel(
            cnn_model=self.cnn_model,
            encoder=self.encoder,
            decoder=self.decoder,
            image_aug=self.image_augmentation,
            vectorization=self.vectorization
        )

        # Define the loss function
        cross_entropy = tf.keras.losses.SparseCategoricalCrossentropy(
            from_logits=False,
            reduction='none',
        )

        # Create a learning rate schedule
        num_train_steps = len(self.train_dataset) * EPOCHS
        num_warmup_steps = num_train_steps // 15
        lr_schedule = LRSchedule(post_warmup_learning_rate=1e-4,
                                 warmup_steps=num_warmup_steps)
        caption_model.build(input_shape=[(None, 299, 299, 3), (5,)])

        # Compile the model
        caption_model.compile(optimizer=tf.keras.optimizers.Adam(
            lr_schedule), loss=cross_entropy)

        caption_model.summary()

        self.model = caption_model

    def train_model(self):
        """
        Train the image captioning model.
        """
        # EarlyStopping criteria
        early_stopping = tf.keras.callbacks.EarlyStopping(
            patience=3, restore_best_weights=True)
        # Fit the model
        self.hist = self.model.fit(
            self.train_dataset,
            epochs=EPOCHS,
            validation_data=self.valid_dataset,
            callbacks=[early_stopping],
        )

    def make_test_dataset(self):
        """
        Make the test dataset.
        """
        caption_mapping = self.load_captions_data(
            self.TEST_ANNOTATIONS_FILE, self.TEST_IMAGES_DIR)
        test_data = self.clean_unbalanced_captions(caption_mapping)
        self.test_dataset = self.make_dataset(
            list(test_data.keys()), list(test_data.values()))

    def evaluate_model(self):
        """
        Evaluate the image captioning model on the test dataset.
        """
        self.make_test_dataset()
        accuracy = self.model.evaluate(self.test_dataset)
        print("Test Accuracy:", accuracy)

    def save_model(self):
        """
        Save the trained image captioning model.
        """
        os.makedirs(self.MODEL_OUTPUT_PATH, exist_ok=True)
        self.model.save(os.path.join(
            self.MODEL_OUTPUT_PATH, f'{self.MODEL_NAME}.keras'))

    def visualize_train_history(self, history):
        """
        Visualize the training history (accuracy and loss).

        Parameters:
            history (tf.keras.callbacks.History): Training history.
        """
        plt.figure(figsize=(12, 4))
        # Plot training & validation accuracy values
        plt.subplot(1, 2, 1)
        plt.plot(history.history['acc'])
        plt.plot(history.history['val_acc'])
        plt.title('Model accuracy')
        plt.xlabel('Epoch')
        plt.ylabel('Accuracy')
        plt.legend(['Train', 'Validation'], loc='upper left')

        # Plot training & validation loss values
        plt.subplot(1, 2, 2)
        plt.plot(history.history['loss'])
        plt.plot(history.history['val_loss'])
        plt.title('Model loss')
        plt.xlabel('Epoch')
        plt.ylabel('Loss')
        plt.legend(['Train', 'Validation'], loc='upper left')

        plt.tight_layout()
        plt.show()

    def main_process(self):
        """
        Main process to prepare dataset, generate datasets, build, train, evaluate,
        save model, and visualize training history.
        """
        self.prepare_dataset()
        self.generator()
        self.build_model()
        self.train_model()
        self.evaluate_model()
        self.save_model()
        self.visualize_train_history(self.hist)


service = ImageCaptioningService()
service.main_process()
