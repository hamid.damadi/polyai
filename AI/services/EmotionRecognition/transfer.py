"""
Emotion Recognition Service

This script implements an Emotion Recognition service using TensorFlow and Keras.
The service is designed to classify facial expressions into five emotion categories:
Fear, Happy, Sad, Surprise, and Neutral.

The model is based on a Convolutional Neural Network (CNN) architecture and is trained
on the FER2013 dataset, which consists of grayscale facial images.

Key functionalities include:
1. Loading the FER2013 dataset from a CSV file.
2. Organizing the dataset into train, validation, and test sets.
3. Preprocessing the pixel values of facial images.
4. Creating data generators for train, validation, and test sets.
5. Building a CNN model for emotion recognition.
6. Training the model using data generators.
7. Evaluating the trained model on the test set.
8. Saving the trained model for future use.
9. Generating a classification report and visualizing the training history.

Author: Hamid Damadi
Date: 2023
"""

import tensorflow as tf
import numpy as np
import pandas as pd
import os
from sklearn.metrics import confusion_matrix, classification_report
import seaborn as sns
import matplotlib.pyplot as plt


class EmotionRecognition:

    def __init__(self):
        """
        Initialize the EmotionRecognition class with default parameters and placeholders.

        Attributes:
        - DATA_INPUT_PATH (str): Path to the FER2013 dataset CSV file.
        - Model_NAME (str): Name for the custom emotion recognition model.
        - MODEL_OUTPUT_PATH (str): Output path for saving the trained model.
        - image_width (int): Width of the facial images.
        - image_height (int): Height of the facial images.
        - image_depth (int): Number of color channels in the images.
        - class_labels (list): List of emotion labels used in the classification.
        - num_classes (int): Number of emotion classes.
        - class_names (list): Names corresponding to the emotion classes.
        - batch_size (int): Batch size for training the model.
        - epochs (int): Number of epochs for training.
        - df (pd.DataFrame): DataFrame to store the entire FER2013 dataset.
        - train_dataFrame (pd.DataFrame): DataFrame for the training set.
        - val_dataFrame (pd.DataFrame): DataFrame for the validation set.
        - test_dataFrame (pd.DataFrame): DataFrame for the test set.
        - train_labels (np.array): One-hot encoded labels for the training set.
        - val_labels (np.array): One-hot encoded labels for the validation set.
        - test_labels (np.array): One-hot encoded labels for the test set.
        - train_generator (tf.keras.preprocessing.image.DirectoryIterator): Data generator for training.
        - validation_generator (tf.keras.preprocessing.image.DirectoryIterator): Data generator for validation.
        - test_generator (tf.keras.preprocessing.image.DirectoryIterator): Data generator for testing.
        - model (tf.keras.models.Sequential): Sequential model for emotion recognition.
        - hist (tf.keras.callbacks.History): Training history of the model.
        """
        # Paths and names
        self.DATA_INPUT_PATH = "###INPUT_PATH_OF_FER2013_DATASET###"
        self.Model_NAME = "###YOUR_CUSTOM_Model_NAME###"
        self.MODEL_OUTPUT_PATH = "###YOUR_MODEL_OUTPUT_PATH###"

        # Image dimensions
        self.image_width = 48
        self.image_height = 48
        self.image_depth = 1

        # Class information
        self.class_labels = [2, 3, 4, 5, 6]
        self.num_classes = 5
        self.class_names = ['Fear', 'Happy', 'Sad', 'Surprise', 'Neutral']

        # Model training parameters
        self.batch_size = 32
        self.epochs = 60

        # DataFrames for dataset storage
        self.df = pd.DataFrame()
        self.train_dataFrame = pd.DataFrame()
        self.val_dataFrame = pd.DataFrame()
        self.test_dataFrame = pd.DataFrame()

        # Labels for train, validation, and test sets
        self.train_labels = None
        self.val_labels = None
        self.test_labels = None

        # Data generators
        self.train_generator = None
        self.validation_generator = None
        self.test_generator = None

        # Model architecture
        self.model = tf.keras.models.Sequential([])

        # Training history
        self.hist = None

    def load_csv(self):
        """
        Load the FER2013 dataset from a CSV file.
        """
        try:
            if os.path.exists(self.DATA_INPUT_PATH):
                self.df = pd.read_csv(self.DATA_INPUT_PATH)
        except Exception as e:
            raise FileNotFoundError(
                f"Error occurred during data download: {str(e)}")

    # Organize dataset into train, validation, and test sets

    def organize_dataset(self):
        """
        Organize the FER2013 dataset into train, validation, and test sets.

        This method creates separate CSV files for each split.
        """

        train_dir = os.path.join(self.output_path, 'train/')
        validation_dir = os.path.join(self.output_path, 'val/')
        test_dir = os.path.join(self.output_path, 'test/')

        os.makedirs(train_dir, exist_ok=True)
        os.makedirs(validation_dir, exist_ok=True)
        os.makedirs(test_dir, exist_ok=True)

        train_df = self.df[self.df['Usage'] == 'Training']
        val_df = self.df[self.df['Usage'] == 'PublicTest']
        test_df = self.df[self.df['Usage'] == 'PrivateTest']

        train_df.to_csv(os.path.join(train_dir, 'train.csv'), index=False)
        val_df.to_csv(os.path.join(validation_dir, 'val.csv'), index=False)
        test_df.to_csv(os.path.join(test_dir, 'test.csv'), index=False)

    def preprocess_pixels(self, pixel_string):
        """
        Preprocess pixel values of facial images.

        Args:
        - pixel_string (str): String containing pixel values.

        Returns:
        - np.array: Reshaped array of pixel values.
        """

        pixels = np.array(pixel_string.split(), dtype=int)
        # Assuming images are grayscal
        return pixels.reshape((self.image_width, self.image_height, self.image_depth))

    def preprocess_dataset(self):
        """
        Preprocess the FER2013 dataset.

        This method reads CSV files, preprocesses pixels, and converts labels to one-hot encoding.
        """

        self.train_dataFrame = pd.read_csv(
            os.path.join(self.output_path, 'train/train.csv'))
        self.val_dataFrame = pd.read_csv(
            os.path.join(self.output_path, 'val/val.csv'))
        self.test_dataFrame = pd.read_csv(
            os.path.join(self.output_path, 'test/test.csv'))

        self.train_dataFrame = self.train_dataFrame[self.train_dataFrame.emotion.isin(
            self.class_labels)]
        self.val_dataFrame = self.val_dataFrame[self.val_dataFrame.emotion.isin(
            self.class_labels)]
        self.test_dataFrame = self.test_dataFrame[self.test_dataFrame.emotion.isin(
            self.class_labels)]

        # Convert categorical labels to one-hot encoded format
        self.train_labels = tf.keras.utils.to_categorical(
            self.train_dataFrame['emotion'].apply(lambda x: x-2))
        self.val_labels = tf.keras.utils.to_categorical(
            self.val_dataFrame['emotion'].apply(lambda x: x-2))
        self.test_labels = tf.keras.utils.to_categorical(
            self.test_dataFrame['emotion'].apply(lambda x: x-2))

        self.train_dataFrame['pixels'] = self.train_dataFrame['pixels'].apply(
            self.preprocess_pixels)
        self.val_dataFrame['pixels'] = self.val_dataFrame['pixels'].apply(
            self.preprocess_pixels)
        self.test_dataFrame['pixels'] = self.test_dataFrame['pixels'].apply(
            self.preprocess_pixels)

    def make_generators(self):
        """
        Create data generators for train, validation, and test sets.

        This method utilizes the Keras ImageDataGenerator for data augmentation.
        """

        # Create data generators for train, validation, and test sets
        train_datagen = tf.keras.preprocessing.image.ImageDataGenerator(
            rescale=1./255,
            rotation_range=15,
            width_shift_range=0.15,
            height_shift_range=0.15,
            shear_range=0.15,
            zoom_range=0.15,
            horizontal_flip=True,
            zca_whitening=False,
        )
        validation_datagen = tf.keras.preprocessing.image.ImageDataGenerator(
            rescale=1./255)
        test_datagen = tf.keras.preprocessing.image.ImageDataGenerator(
            rescale=1./255)

        # Define generators
        self.train_generator = train_datagen.flow(
            x=np.array(self.train_dataFrame['pixels'].tolist()),  # input data
            y=self.train_labels,
            batch_size=self.batch_size
        )

        self.validation_generator = validation_datagen.flow(
            x=np.array(self.val_dataFrame['pixels'].tolist()),  # input data
            y=self.val_labels,
            batch_size=self.batch_size,
            shuffle=False
        )

        self.test_generator = test_datagen.flow(
            x=np.array(self.test_dataFrame['pixels'].tolist()),  # input data
            y=self.test_labels,
            batch_size=self.batch_size,
            shuffle=False
        )

    def resize_images(x):
        return tf.image.resize(x, (224, 224))

    # Define CNN model for emotion recognition

    tf.keras.layers.AveragePooling2D

    def build_model(self):
        """
        Build the CNN model for emotion recognition.

        This method defines the architecture of the neural network.
        """

        self.model = tf.keras.models.Sequential([
            # Repeat the grayscale channel to match the RGB expectation
            tf.keras.layers.Input(shape=(48, 48, 1)),
            #             tf.keras.layers.Lambda(lambda x: tf.concat([x, x, x], axis=-1)),
            tf.keras.layers.Conv2D(64, (5, 5), activation='elu',
                                   padding='same', kernel_initializer='he_normal'),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Conv2D(64, (5, 5), activation='elu',
                                   padding='same', kernel_initializer='he_normal'),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.MaxPooling2D((2, 2)),
            tf.keras.layers.Conv2D(128, (3, 3), activation='elu',
                                   padding='same', kernel_initializer='he_normal'),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Conv2D(128, (5, 5), activation='elu',
                                   padding='same', kernel_initializer='he_normal'),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.MaxPooling2D((2, 2)),
            tf.keras.layers.Dropout(0.4),
            tf.keras.layers.Conv2D(256, (3, 3), activation='elu',
                                   padding='same', kernel_initializer='he_normal'),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Conv2D(256, (3, 3), activation='elu',
                                   padding='same', kernel_initializer='he_normal'),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.MaxPooling2D((2, 2)),
            tf.keras.layers.Dropout(0.3),
            tf.keras.layers.Conv2D(128, (3, 3), activation='elu',
                                   padding='same', kernel_initializer='he_normal'),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.MaxPooling2D((2, 2)),
            tf.keras.layers.Dropout(0.3),
            tf.keras.layers.Conv2D(64, (3, 3), activation='elu',
                                   padding='same', kernel_initializer='he_normal'),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.MaxPooling2D((2, 2)),
            tf.keras.layers.Dropout(0.3),
            tf.keras.layers.Flatten(),
            tf.keras.layers.Dense(128, activation='elu',
                                  kernel_initializer='he_normal'),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Dense(64, activation='elu',
                                  kernel_initializer='he_normal'),
            tf.keras.layers.Dropout(0.3),
            tf.keras.layers.Dense(self.num_classes, activation='softmax')
        ])

        # Resize images using Lambda layer

        # model.add(tf.keras.layers.Lambda(resize_images, input_shape=(48, 48, 1)))

        optimizer = tf.keras.optimizers.Adam(lr=0.001)
        self.model.compile(optimizer=optimizer,
                           loss='categorical_crossentropy', metrics=['accuracy'])

    # Train the CNN model

    def train_model(self):
        """
        Train the CNN model using data generators.

        This method uses early stopping and learning rate reduction callbacks.
        """

        early_stopping = tf.keras.callbacks.EarlyStopping(
            monitor='val_accuracy',
            min_delta=0.00008,
            patience=11,
            verbose=1,
            restore_best_weights=True,
        )

        lr_scheduler = tf.keras.callbacks.ReduceLROnPlateau(
            monitor='val_accuracy',
            min_delta=0.0001,
            factor=0.25,
            patience=4,
            min_lr=1e-7,
            verbose=1,
        )
        self.hist = self.model.fit(
            self.train_generator,
            steps_per_epoch=len(self.train_generator),
            epochs=self.epochs,
            validation_data=self.validation_generator,
            validation_steps=len(self.validation_generator),
            callbacks=[
                early_stopping,
                lr_scheduler
            ]
        )

    # Evaluate the trained model

    def evaluate_model(self):
        """
        Evaluate the trained model on the test set.

        This method prints the test accuracy.
        """

        accuracy = self.model.evaluate(self.test_generator)
        print("Test Accuracy:", accuracy[1])

    def save_model(self):
        """
        Save the trained model for future use.

        The model is saved in the specified output path.
        """

        os.makedirs(self.MODEL_OUTPUT_PATH, exist_ok=True)
        self.model.save(os.path.join(
            self.MODEL_OUTPUT_PATH, f'{self.Model_NAME}.keras'))

    def report(self):
        """
        Generate a classification report and visualize the confusion matrix.

        This method prints and displays the classification report and confusion matrix.
        """

        # Make predictions
        predictions = self.model.predict(self.test_generator)

        # Convert predicted probabilities to class labels
        predicted_labels = np.argmax(predictions, axis=1)
        true_labels = np.argmax(self.test_labels, axis=1)

        # Compute confusion matrix
        cm = confusion_matrix(true_labels, predicted_labels)

        # Print the classification report

        report = classification_report(
            true_labels, predicted_labels, target_names=self.class_names, zero_division=1)
        print("Classification Report:")
        print(report)

        # Visualize confusion matrix using seaborn
        plt.figure(figsize=(8, 6))
        sns.heatmap(cm, annot=True, fmt='d', cmap='Blues',
                    xticklabels=range(self.num_classes), yticklabels=range(self.num_classes))
        plt.xlabel('Predicted')
        plt.ylabel('True')
        plt.title('Confusion Matrix')
        plt.show()

    def visualize_train_history(self):
        """
        Visualize the training history including accuracy and loss curves.

        This method plots the training and validation accuracy/loss curves.
        """

        plt.figure(figsize=(12, 4))

        # Plot training & validation accuracy values
        plt.subplot(1, 2, 1)
        plt.plot(self.hist.history['accuracy'])
        plt.plot(self.hist.history['val_accuracy'])
        plt.title('Model accuracy')
        plt.xlabel('Epoch')
        plt.ylabel('Accuracy')
        plt.legend(['Train', 'Validation'], loc='upper left')

        # Plot training & validation loss values
        plt.subplot(1, 2, 2)
        plt.plot(self.hist.history['loss'])
        plt.plot(self.hist.history['val_loss'])
        plt.title('Model loss')
        plt.xlabel('Epoch')
        plt.ylabel('Loss')
        plt.legend(['Train', 'Validation'], loc='upper left')

        plt.tight_layout()
        plt.show()

    def predict_and_visualize(self):
        """
        Make predictions and visualize a few sample images.

        This method randomly selects and displays a few images along with their predictions.
        """

        # Make predictions
        predictions = self.model.predict(self.test_generator)

        # Select a few images for visualization
        sample_indices = np.random.choice(
            len(self.test_dataFrame), size=5, replace=False)

        for idx in sample_indices:
            # Get the original image
            original_image = np.array(self.test_dataFrame['pixels'].iloc[idx])

            # Reshape the image to (48, 48) assuming it was flattened
            original_image = original_image.reshape((48, 48))

            # Make a prediction for the current image
            current_prediction = predictions[idx]
            predicted_class = np.argmax(current_prediction)
            true_class = np.argmax(self.test_labels[idx])

            # Create a figure and axis
            _, ax = plt.subplots()

            # Display the image
            ax.imshow(original_image, cmap='gray')

            # Add a bounding box around the detected face
            rect = plt.Rectangle((0, 0), 48, 48, linewidth=1,
                                 edgecolor='r', facecolor='none')
            ax.add_patch(rect)

            # Add the predicted and true labels as text
            plt.text(
                0, 50, f'Predicted: {self.class_names[predicted_class]}', color='r')
            plt.text(0, 45, f'True: {self.class_names[true_class]}', color='g')

            # Save or display the image
            plt.show()

    def main_process(self):
        """
        Execute the main processing pipeline.

        This method orchestrates the entire processing pipeline.
        """

        self.load_csv()
        self.organize_dataset()
        self.preprocess_dataset()
        self.make_generators()
        self.build_model()
        self.train_model()
        self.evaluate_model()
        self.save_model()
        self.report()
        self.visualize_train_history()

# Instantiate the EmotionRecognition class and execute the main process


imgRec = EmotionRecognition()
imgRec.main_process()
