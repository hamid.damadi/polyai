import numpy as np
import cv2
import dlib


class EmotionRecognitionService:
    def __init__(self, model, share_folder):
        self.labels = ['Fear', 'Happy', 'Sad', 'Surprise', 'Neutral']
        self.model = model
        self.share_folder = share_folder

    def preprocess_image(self, image):
        cropped_img = np.expand_dims(np.expand_dims(
            cv2.resize(image, (48, 48)), -1), 0)
        cv2.normalize(cropped_img, cropped_img, alpha=0,
                      beta=1, norm_type=cv2.NORM_L2, dtype=cv2.CV_32F)
        cropped_img = cropped_img / 255.0
        return cropped_img

    def predict(self, path):
        full_size_image = cv2.imread(self.share_folder + path)
        gray_image = cv2.cvtColor(full_size_image, cv2.COLOR_BGR2GRAY)
        faces = dlib.get_frontal_face_detector()(gray_image, 1)

        if (len(faces) > 0):
            # Draw bounding boxes around detected faces and add labels
            for face in faces:
                x, y, w, h = face.left(), face.top(), face.width(), face.height()
                # Crop the face region for emotion prediction
                face_image = gray_image[y:y + h, x:x + w]

                # Predict emotion for the cropped face
                cropped_img = self.preprocess_image(face_image)
                predicted_emotion = self.model.predict(cropped_img)
                predicted_emotion_index = np.argmax(predicted_emotion)
                predicted_emotion_label = self.labels[predicted_emotion_index]
                print(f'Predicted Emotion: {predicted_emotion_label}')
                # Add emotion label
                cv2.rectangle(full_size_image, (x, y),
                              (x + w, y + h), (0, 255, 0), 1)
                cv2.putText(img=full_size_image, text=predicted_emotion_label, org=(x, y-8),
                            fontFace=cv2.FONT_HERSHEY_COMPLEX_SMALL, fontScale=0.8, color=(0, 255, 0),
                            thickness=1, lineType=cv2.LINE_AA)

                cv2.imwrite(
                    f"{self.share_folder}pred-{path}", full_size_image)

            return [predicted_emotion_label]
        else:
            return ['None']
