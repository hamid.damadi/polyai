import pandas as pd
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import random


class ImageRecognitionService():
    def __init__(self, model, share_folder):
        self.model = model
        self.share_folder = share_folder

    def preprocess_image(self, x, num):
        x = x.values.reshape((num, 28, 28))
        x = x.astype('float32') / 255.0
        x = tf.image.grayscale_to_rgb(tf.expand_dims(x, axis=3))
        return x

    def predict(self, index):
        df = pd.read_csv("./datasets/mnist/mnist.csv",
                         skiprows=int(index), nrows=1, header=None)
        image = df.iloc[0, :784]
        image = self.preprocess_image(image, 1)
        prediction = self.model.predict(image)
        argmax = np.argmax(prediction)
        return prediction, argmax

    def getSamples(self, count):
        imageArr = []
        for _ in range(0, count):
            rand = random.randint(60000, 70000)
            df = pd.read_csv("./datasets/mnist/mnist.csv",
                             skiprows=rand, nrows=1, header=None)
            x = df.iloc[0, :784]
            x = x.values.reshape((1, 28, 28))
            fileRand = random.randint(1000000, 10000000)
            filePath = f"{self.share_folder}image_{fileRand}.png"
            plt.imsave(filePath, list(x)[0])
            imageArr.append({
                "path": f"image_{fileRand}.png",
                "index": rand
            })
        return imageArr
