"""
Image Recognition using Transfer Learning

This script demonstrates the process of building an image recognition model using transfer learning,
specifically leveraging the MobileNetV2 pre-trained model. The dataset used is the MNIST dataset,
which is loaded from a CSV file. The script covers data loading, preprocessing, model building,
training, evaluation, and reporting.

Author: Hamid Damadi
Date: 2013

Dependencies:
- TensorFlow
- pandas
- numpy
- os
- matplotlib
- scikit-learn
- seaborn

Note: Ensure that the required dependencies are installed before running the script.

"""

import tensorflow as tf
import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix, classification_report
import seaborn as sns


class ImageRecognition:
    def __init__(self):
        """
        Initialize the ImageRecognition class with default parameters.

        Attributes:
            data_input_path (str): Path to the MNIST dataset CSV file.
            model_name (str): Custom name for the trained model.
            model_output_path (str): Path to save the trained model.
            image_width (int): Width of the input images.
            image_height (int): Height of the input images.
            image_depth (int): Depth (number of channels) of the input images.
            num_classes (int): Number of classes (output categories).
            class_names (list): List of class names corresponding to the output categories.
            epochs (int): Number of epochs for initial training.
            fine_tune_epochs (int): Number of additional epochs for fine-tuning.
            batch_size (int): Batch size for training and evaluation.
            pre_train_model_width (int): Width of the pre-trained model input.
            pre_train_model_height (int): Height of the pre-trained model input.
            df (pd.DataFrame): DataFrame to store the loaded dataset.
            x_train (pd.DataFrame): DataFrame for training input data.
            y_train (pd.DataFrame): DataFrame for training output labels.
        """
        self.data_input_path = "###INPUT_PATH_OF_MNIST_DATASET###"
        self.model_name = "###YOUR_CUSTOME_MODEL_NAME###"
        self.model_output_path = "###YOUR_MODEL_OUTPUT_PATH###"
        self.image_width = 28
        self.image_height = 28
        self.image_depth = 1
        self.num_classes = 10
        self.class_names = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
        self.epochs = 12
        self.fine_tune_epochs = 4
        self.batch_size = 64
        self.pre_train_model_width = 224
        self.pre_train_model_height = 224
        self.df = pd.DataFrame()
        self.x_train = pd.DataFrame()
        self.y_train = pd.DataFrame()

    def load_csv(self):
        """
        Load the MNIST dataset from a CSV file.

        Raises:
            FileNotFoundError: If the specified file is not found.
        """
        try:
            if os.path.exists(self.data_input_path):
                self.df = pd.read_csv(self.data_input_path)
        except Exception as e:
            raise FileNotFoundError(
                f"Error occurred during data download: {str(e)}")

    def preprocess_dataset(self):
        """
        Preprocess the MNIST dataset into training, validation, and test sets.
        Convert labels to categorical format.

        This method sets the following attributes:
            x_train, y_train: Training data and labels.
            x_val, y_val: Validation data and labels.
            x_test, y_test: Test data and labels.
        """
        self.x_train, self.y_train = self.df.iloc[:60000,
                                                  :784], self.df.iloc[:60000, 784]
        self.x_val, self.y_val = self.df.iloc[60000:65000,
                                              :784], self.df.iloc[60000:65000, 784]
        self.x_test, self.y_test = self.df.iloc[65000:,
                                                :784], self.df.iloc[65000:, 784]

        self.y_train = tf.keras.utils.to_categorical(
            self.y_train, self.num_classes)
        self.y_val = tf.keras.utils.to_categorical(
            self.y_val, self.num_classes)
        self.y_test = tf.keras.utils.to_categorical(
            self.y_test, self.num_classes)

    def make_generators(self):
        """
        Create data generators for train, validation, and test sets.

        This method sets the following attributes:
            train_generator: Data generator for training set.
            validation_generator: Data generator for validation set.
            test_generator: Data generator for test set.
        """
        train_datagen = tf.keras.preprocessing.image.ImageDataGenerator(
            rescale=1./255,
            rotation_range=15,
            width_shift_range=0.15,
            height_shift_range=0.15,
            shear_range=0.15,
            zoom_range=0.15,
            horizontal_flip=True,
            zca_whitening=False,
        )

        validation_datagen = tf.keras.preprocessing.image.ImageDataGenerator(
            rescale=1./255)

        test_datagen = tf.keras.preprocessing.image.ImageDataGenerator(
            rescale=1./255)

        self.train_generator = train_datagen.flow(
            x=np.array(self.x_train).reshape(
                (60000, self.image_width, self.image_height, self.image_depth)),
            y=self.y_train,
            batch_size=self.batch_size
        )

        self.validation_generator = validation_datagen.flow(
            x=np.array(self.x_val).reshape((5000, self.image_width,
                                            self.image_height, self.image_depth)),
            y=self.y_val,
            batch_size=self.batch_size,
            shuffle=False
        )

        self.test_generator = test_datagen.flow(
            x=np.array(self.x_test).reshape((5000, self.image_width,
                                             self.image_height, self.image_depth)),
            y=self.y_test,
            batch_size=self.batch_size,
            shuffle=False
        )

    def build_model(self):
        """
        Build the image recognition model using MobileNetV2 as a base.

        This method sets the following attributes:
            base_model: Pre-trained MobileNetV2 model.
            model: Custom image recognition model.
        """
        self.base_model = tf.keras.applications.MobileNetV2(input_shape=(
            self.pre_train_model_width, self.pre_train_model_height, 3),
            include_top=False,
            weights='imagenet')

        self.model = tf.keras.models.Sequential([
            tf.keras.layers.Input(
                shape=(self.image_width, self.image_height, self.image_depth)),
            tf.keras.layers.Resizing(
                height=self.pre_train_model_height, width=self.pre_train_model_width),
            tf.keras.layers.Lambda(lambda x: tf.concat([x, x, x], axis=-1)),
            self.base_model,
            tf.keras.layers.GlobalAveragePooling2D(),
            tf.keras.layers.Dense(256, activation='relu'),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Dropout(0.3),
            tf.keras.layers.Dense(128, activation='relu'),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Dense(self.num_classes, activation='softmax')
        ])

        for layer in self.base_model.layers:
            layer.trainable = False

        self.model.compile(optimizer=tf.keras.optimizers.Adam(
            learning_rate=0.001),
            loss='categorical_crossentropy',
            metrics=['accuracy'])
        self.model.summary()
        diagram_path = os.path.join(
            self.model_output_path, 'diagram')
        os.makedirs(diagram_path, exist_ok=True)
        tf.keras.utils.plot_model(
            self.model, to_file=os.path.join(
                diagram_path, f'{self.model_name}.png'), show_shapes=True, show_layer_names=True)

    def train_model(self):
        """
        Train the image recognition model.

        This method sets the following attribute:
            hist: Training history.
        """
        early_stopping = tf.keras.callbacks.EarlyStopping(
            monitor='val_accuracy',
            min_delta=0.00008,
            patience=11,
            verbose=1,
            restore_best_weights=True,
        )

        lr_scheduler = tf.keras.callbacks.ReduceLROnPlateau(
            monitor='val_accuracy',
            min_delta=0.0001,
            factor=0.25,
            patience=4,
            min_lr=1e-7,
            verbose=1,
        )

        self.hist = self.model.fit(
            self.train_generator,
            steps_per_epoch=len(self.train_generator),
            epochs=self.epochs,
            validation_data=self.validation_generator,
            validation_steps=len(self.validation_generator),
            callbacks=[
                early_stopping,
                lr_scheduler
            ]
        )

    def fine_tune_model(self):
        """
        Fine-tune the pre-trained MobileNetV2 model.

        This method sets the following attribute:
            hist2: Fine-tuning history.
        """
        early_stopping = tf.keras.callbacks.EarlyStopping(
            monitor='val_accuracy',
            min_delta=0.00008,
            patience=11,
            verbose=1,
            restore_best_weights=True,
        )

        lr_scheduler = tf.keras.callbacks.ReduceLROnPlateau(
            monitor='val_accuracy',
            min_delta=0.0001,
            factor=0.25,
            patience=4,
            min_lr=1e-7,
            verbose=1,
        )

        # Allow the last 40 layers of the base model to be trainable
        for layer in self.base_model.layers[-40:]:
            layer.trainable = True

        self.model.compile(optimizer=tf.keras.optimizers.Adam(
            learning_rate=0.0001),
            loss='categorical_crossentropy',
            metrics=['accuracy'])
        self.model.summary()
        self.hist2 = self.model.fit(
            self.train_generator,
            steps_per_epoch=len(self.train_generator),
            epochs=self.epochs + self.fine_tune_epochs,
            initial_epoch=self.hist.epoch[-1] + 1,
            validation_data=self.validation_generator,
            validation_steps=len(self.validation_generator),
            callbacks=[
                early_stopping,
                lr_scheduler
            ]
        )

    def evaluate_model(self):
        """
        Evaluate the trained image recognition model on the test set.

        Prints:
            Test Accuracy.
        """
        accuracy = self.model.evaluate(self.test_generator)
        print("Test Accuracy:", accuracy[1])

    def save_model(self, model_output_path):
        """
        Save the trained image recognition model.

        Parameters:
            model_output_path (str): Path to save the model.
        """
        os.makedirs(model_output_path, exist_ok=True)
        self.model.save(os.path.join(
            model_output_path, f'{self.model_name}.keras'))

    def report(self):
        """
        Generate and print a classification report.

        Prints:
            Classification Report.
            Confusion Matrix (visualization).
        """
        predictions = self.model.predict(self.test_generator)
        predicted_labels = np.argmax(predictions, axis=1)
        true_labels = np.argmax(self.y_test, axis=1)

        cm = confusion_matrix(true_labels, predicted_labels)
        report = classification_report(
            true_labels, predicted_labels, target_names=self.class_names, zero_division=1)
        print("Classification Report:")
        print(report)

        plt.figure(figsize=(8, 6))
        sns.heatmap(cm, annot=True, fmt='d', cmap='Blues',
                    xticklabels=range(self.num_classes), yticklabels=range(self.num_classes))
        plt.xlabel('Predicted')
        plt.ylabel('True')
        plt.title('Confusion Matrix')
        plt.show()

    def visualize_train_history(self, history):
        """
        Visualize the training history (accuracy and loss).

        Parameters:
            history (tf.keras.callbacks.History): Training history object.
        """
        plt.figure(figsize=(12, 4))

        # Plot training & validation accuracy values
        plt.subplot(1, 2, 1)
        plt.plot(history.history['accuracy'])
        plt.plot(history.history['val_accuracy'])
        plt.title('Model accuracy')
        plt.xlabel('Epoch')
        plt.ylabel('Accuracy')
        plt.legend(['Train', 'Validation'], loc='upper left')

        # Plot training & validation loss values
        plt.subplot(1, 2, 2)
        plt.plot(history.history['loss'])
        plt.plot(history.history['val_loss'])
        plt.title('Model loss')
        plt.xlabel('Epoch')
        plt.ylabel('Loss')
        plt.legend(['Train', 'Validation'], loc='upper left')

        plt.tight_layout()
        plt.show()

    def main_process(self):
        """
        Main execution of the image recognition process, including data loading,
        preprocessing, model building, training, evaluation, and reporting.
        """
        self.load_csv()
        self.preprocess_dataset()
        self.make_generators()
        self.build_model()
        self.train_model()
        self.evaluate_model()
        self.fine_tune_model()
        self.evaluate_model()
        self.save_model(self.model_output_path)
        self.report()
        self.visualize_train_history(self.hist)
        self.visualize_train_history(self.hist2)


# Create an instance of the ImageRecognition class and execute the main process
imgRec = ImageRecognition()
imgRec.main_process()
