"""
Sentiment Analysis Model Training

This script implements a sentiment analysis model using TensorFlow and Keras.
The model is designed to classify movie reviews into positive and negative sentiments.

Key functionalities include:
1. Downloading and extracting the IMDb sentiment analysis dataset.
2. Cleaning and preprocessing the text data.
3. Tokenizing the text and preparing it for model training.
4. Building a sentiment analysis model with an Embedding layer and Bidirectional LSTM.
5. Training the model on the prepared dataset.
6. Evaluating the model on a separate test set.
7. Saving the trained model for future use.
8. Visualizing the training history.

Author: Hamid Damadi
Date: 2023
"""

import requests
import tarfile
import numpy as np
import pandas as pd
import os
import re
import matplotlib.pyplot as plt

import tensorflow as tf

# Define main paths
MAIN_PATH = '../../datasets/'
DATA_INPUT_PATH = MAIN_PATH + 'aclImdb/'


class PreProcessSentiment:
    """
    The PreProcessSentiment class handles the data preprocessing steps for sentiment analysis.

    Attributes:
    - DATA_INPUT_PATH (str): Path to the input data.
    - MAIN_PATH (str): Main directory path.

    Methods:
    - get_data(): Downloads and extracts the sentiment analysis dataset if not already present.
    - text_clean(text: str) -> str: Cleans the input text by removing special characters and converting to lowercase.
    - process_train() -> pd.DataFrame: Processes the training dataset, performs text cleaning, and saves the processed data.
    - process_main(): Main preprocessing function that orchestrates data download, cleaning, and saving.
    """

    def __init__(self):
        self.DATA_INPUT_PATH = DATA_INPUT_PATH
        self.MAIN_PATH = MAIN_PATH

    def get_data(self):
        """
        Downloads and extracts the sentiment analysis dataset if not already present.
        """
        try:
            data_path = os.path.join(self.DATA_INPUT_PATH, 'aclImdb')
            if not os.path.exists(data_path):
                # Download the dataset
                url = "http://ai.stanford.edu/~amaas/data/sentiment/aclImdb_v1.tar.gz"
                r = requests.get(url, stream=True)
                with open(os.path.join(self.MAIN_PATH, 'aclImdb_v1.tar.gz'), 'wb') as f:
                    f.write(r.content)

                # Extract the dataset
                file = tarfile.open(
                    os.path.join(self.MAIN_PATH, 'aclImdb_v1.tar.gz'), mode="r|gz")
                file.extractall(path=self.MAIN_PATH)
        except Exception as e:
            raise RuntimeError(
                f"Error occurred during data download: {str(e)}")

    def text_clean(self, text):
        """
        Cleans the input text by removing special characters and converting to lowercase.

        Args:
        - text (str): Input text.

        Returns:
        - str: Cleaned text.
        """
        letters = re.sub("[^a-zA-z0-9\s]", " ", text)
        words = letters.lower().split()
        text = " ".join(words)
        return text

    def process_train(self):
        """
        Processes the training dataset, performs text cleaning, and saves the processed data.

        Returns:
        - pd.DataFrame: Processed training data.
        """
        review_dest = []
        reviews = []
        train_review_files_pos = os.listdir(
            self.DATA_INPUT_PATH + 'train/pos/')
        review_dest.append(self.DATA_INPUT_PATH + 'train/pos/')
        train_review_files_neg = os.listdir(
            self.DATA_INPUT_PATH + 'train/neg/')
        review_dest.append(self.DATA_INPUT_PATH + 'train/neg/')
        test_review_files_pos = os.listdir(self.DATA_INPUT_PATH + 'test/pos/')
        review_dest.append(self.DATA_INPUT_PATH + 'test/pos/')
        test_review_files_neg = os.listdir(self.DATA_INPUT_PATH + 'test/neg/')
        review_dest.append(self.DATA_INPUT_PATH + 'test/neg/')

        sentiment_label = [1]*len(train_review_files_pos) +  \
            [0]*len(train_review_files_neg) + \
            [1]*len(test_review_files_pos) + \
            [0]*len(test_review_files_neg)

        review_train_test = ['train']*len(train_review_files_pos) + \
                            ['train']*len(train_review_files_neg) + \
                            ['test']*len(test_review_files_pos) + \
                            ['test']*len(test_review_files_neg)

        reviews_count = 0

        for dest in review_dest:
            files = os.listdir(dest)
            for f in files:
                fl = open(dest + f, 'r')
                review = fl.readlines()
                review_clean = self.text_clean(review[0])
                reviews.append(review_clean)
                reviews_count += 1
                fl.close()

        df = pd.DataFrame()
        df['Train_test_ind'] = review_train_test
        df['review'] = reviews
        df['sentiment_label'] = sentiment_label
        df.to_csv(self.DATA_INPUT_PATH + 'processed_file.csv', index=False)
        return df

    def process_main(self):
        """
        Main preprocessing function that orchestrates data download, cleaning, and saving.
        """
        self.get_data()
        df = self.process_train()
        # We will tokenize the text for the most common 50000 words.
        max_features = 50000
        tokenizer = tf.keras.preprocessing.text.Tokenizer(
            num_words=max_features, split=' ')
        tokenizer.fit_on_texts(df['review'].values)
        X = tokenizer.texts_to_sequences(df['review'].values)
        X_ = []
        for x in X:
            x = x[:1000]
            X_.append(x)
        X_ = tf.keras.preprocessing.sequence.pad_sequences(X_)
        y = df['sentiment_label'].values
        index = list(range(X_.shape[0]))
        np.random.shuffle(index)
        train_record_count = int(len(index)*0.7)
        validation_record_count = int(len(index)*0.15)

        train_indices = index[:train_record_count]
        validation_indices = index[train_record_count:train_record_count +
                                   validation_record_count]
        test_indices = index[train_record_count + validation_record_count:]
        X_train, y_train = X_[train_indices], y[train_indices]
        X_val, y_val = X_[validation_indices], y[validation_indices]
        X_test, y_test = X_[test_indices], y[test_indices]

        np.save(os.path.join(self.DATA_INPUT_PATH, 'X_train'), X_train)
        np.save(os.path.join(self.DATA_INPUT_PATH, 'y_train'), y_train)
        np.save(os.path.join(self.DATA_INPUT_PATH, 'X_val'), X_val)
        np.save(os.path.join(self.DATA_INPUT_PATH, 'y_val'), y_val)
        np.save(os.path.join(self.DATA_INPUT_PATH, 'X_test'), X_test)
        np.save(os.path.join(self.DATA_INPUT_PATH, 'y_test'), y_test)

        # saving the tokenizer object for inference
        with open(os.path.join(self.DATA_INPUT_PATH, 'tokenizer.json'), 'w') as handle:
            handle.write(tokenizer.to_json())


# Instantiate the PreProcessSentiment class
preProcessSentiment = PreProcessSentiment()
preProcessSentiment.process_main()


@tf.keras.saving.register_keras_serializable(package="my_package", name="EmbeddingLayer")
class EmbeddingLayer(tf.keras.layers.Layer):
    """
    Custom Keras layer for embedding with pre-trained GloVe embeddings.

    Attributes:
    - input_length (int): Length of input sequences.
    - n_words (int): Number of words considered in the vocabulary.
    - embedding_dim (int): Dimension of the embedding space.
    - word_index (dict): Dictionary mapping words to their indices in the vocabulary.

    Methods:
    - build(input_shape): Builds the embedding layer and loads pre-trained GloVe embeddings.
    - call(inputs): Performs the embedding lookup during the forward pass.
    - get_config(): Returns the configuration of the layer.
    - from_config(config): Creates a layer from its config.
    """

    def __init__(self, input_length, n_words, embedding_dim, word_index, **kwargs):
        super(EmbeddingLayer, self).__init__(**kwargs)
        self.input_length = input_length
        self.n_words = n_words
        self.embedding_dim = embedding_dim
        self.word_index = word_index

    def build(self, input_shape):
        """
        Builds the embedding layer and loads pre-trained GloVe embeddings.
        """
        # Load pre-trained GloVe embeddings
        glove_path = os.path.join(MAIN_PATH, 'glove.6B.100d.txt')
        if not os.path.exists(glove_path):
            raise FileNotFoundError(
                "GloVe file not found at {}".format(glove_path))

        embeddings_index = {}
        with open(glove_path, 'r', encoding='utf-8') as f:
            for line in f:
                values = line.split()
                word = values[0]
                coefs = np.asarray(values[1:], dtype='float32')
                embeddings_index[word] = coefs

        # Create embedding matrix
        embedding_matrix = np.zeros(
            (self.n_words + 1, self.embedding_dim))  # Adjust size here
        for word, i in self.word_index.items():
            if i <= self.n_words:  # Make sure the index is within bounds
                embedding_vector = embeddings_index.get(word)
                if embedding_vector is not None:
                    embedding_matrix[i] = embedding_vector

        self.embedding = self.add_weight(
            name='embedding',
            shape=(self.n_words + 1, self.embedding_dim),  # Adjust shape here
            trainable=True,
            initializer=tf.keras.initializers.Constant(embedding_matrix),
            dtype=tf.float32
        )
        super(EmbeddingLayer, self).build(input_shape)

    def call(self, inputs):
        """
        Performs the embedding lookup during the forward pass.

        Args:
        - inputs: Input tensor (sequence of word indices).

        Returns:
        - Tensor: Embedded representation of the input sequence.
        """
        inputs = tf.cast(inputs, dtype=tf.int32)
        return tf.nn.embedding_lookup(self.embedding, inputs)

    def get_config(self):
        """
        Returns the configuration of the layer.

        Returns:
        - dict: Configuration of the layer.
        """
        config = {
            'input_length': self.input_length,
            'n_words': self.n_words,
            'embedding_dim': self.embedding_dim,
            'word_index': self.word_index
        }
        base_config = super(EmbeddingLayer, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))

    @classmethod
    def from_config(cls, config):
        """
        Creates a layer from its config.

        Args:
        - config (dict): Configuration of the layer.

        Returns:
        - EmbeddingLayer: Instance of the EmbeddingLayer.
        """
        return cls(**config)


class ReviewSentiment:
    """
    Class for building, training, and evaluating a sentiment analysis model using TensorFlow and Keras.

    Attributes:
    - path (str): Main path for storing and loading data and models.
    - MODEL_OUTPUT_PATH (str): Output path for saving the trained model.
    - MODEL_NAME (str): Name of the model.
    - batch_size (int): Batch size for training.
    - train_to_val_ratio (float): Ratio of training data to validation data.
    - batch_size_val (int): Batch size for validation.
    - epochs (int): Number of training epochs.
    - hidden_states (int): Number of hidden states in the LSTM layer.
    - embedding_dim (int): Dimension of the word embeddings.
    - learning_rate (float): Learning rate for the optimizer.
    - n_words (int): Number of words considered in the vocabulary.
    - sentence_length (int): Maximum length of input sequences.
    - lambda1 (float): L2 regularization parameter.
    - hist (tf.keras.callbacks.History): History of training metrics.
    - X_train (numpy.ndarray): Training data.
    - y_train (numpy.ndarray): Training labels.
    - X_val (numpy.ndarray): Validation data.
    - y_val (numpy.ndarray): Validation labels.
    - X_test (numpy.ndarray): Test data.
    - y_test (numpy.ndarray): Test labels.

    Methods:
    - load_tokenizer(): Loads the tokenizer used for text preprocessing.
    - build_model(): Builds the sentiment analysis model using TensorFlow and Keras.
    - train_model(): Trains the sentiment analysis model on the training data.
    - evaluate_model(): Evaluates the model on the test data.
    - save_model(): Saves the trained model for future use.
    - visualize_train_history(history): Visualizes the training history (accuracy and loss).
    - main_process(): Executes the main processes of loading, building, training, and evaluating the model.
    """

    def __init__(self):
        self.path = MAIN_PATH
        self.MODEL_OUTPUT_PATH = "### MODEL_OUTPUT_PATH ###"
        self.MODEL_NAME = "### MODEL_NAME ###"
        self.batch_size = 250
        self.train_to_val_ratio = 5.0
        self.batch_size_val = int(self.batch_size / self.train_to_val_ratio)
        self.epochs = 7
        self.hidden_states = 100
        self.embedding_dim = 100
        self.learning_rate = 1e-3
        self.n_words = 50000 + 1
        self.sentence_length = 1000
        self.lambda1 = 0.01
        self.hist = None
        self.X_train = np.load(os.path.join(self.path, "aclImdb/X_train.npy"))
        self.y_train = np.load(os.path.join(self.path, "aclImdb/y_train.npy"))
        self.y_train = np.reshape(self.y_train, (-1, 1))
        self.X_val = np.load(os.path.join(self.path, "aclImdb/X_val.npy"))
        self.y_val = np.load(os.path.join(self.path, "aclImdb/y_val.npy"))
        self.y_val = np.reshape(self.y_val, (-1, 1))
        self.X_test = np.load(os.path.join(self.path, "aclImdb/X_test.npy"))
        self.y_test = np.load(os.path.join(self.path, "aclImdb/y_test.npy"))
        self.y_test = np.reshape(self.y_test, (-1, 1))
        print(np.shape(self.X_train), np.shape(self.y_train))
        print(np.shape(self.X_val), np.shape(self.y_val))
        print(np.shape(self.X_test), np.shape(self.y_test))
        print('no of positive class in train:', np.sum(self.y_train))
        print('no of positive class in test:', np.sum(self.y_val))

    def load_tokenizer(self):
        """
        Loads the tokenizer used for text preprocessing.

        Returns:
        - tf.keras.preprocessing.text.Tokenizer: Tokenizer object.
        """
        try:
            path_tokenizer = os.path.join(self.path, 'aclImdb/tokenizer.json')
            with open(path_tokenizer, 'r') as handle:
                tokenizer = tf.keras.preprocessing.text.tokenizer_from_json(
                    handle.read())
            return tokenizer
        except FileNotFoundError:
            raise FileNotFoundError(
                f"Tokenizer file not found at {path_tokenizer}")
        except Exception as e:
            raise RuntimeError(
                f"Error occurred during tokenizer loading: {str(e)}")

    def build_model(self):
        """
        Builds the sentiment analysis model using TensorFlow and Keras.
        """
        tokenizer = self.load_tokenizer()

        model = tf.keras.Sequential()

        # Define embedding layer
        embedding_layer = EmbeddingLayer(
            input_length=self.sentence_length,
            n_words=self.n_words,
            embedding_dim=self.embedding_dim,
            word_index=tokenizer.word_index
        )
        model.add(embedding_layer)

        # Define LSTM layer
        lstm_layer = tf.keras.layers.Bidirectional(
            tf.keras.layers.LSTM(self.hidden_states))
        model.add(lstm_layer)

        # Flatten before Dense layer
        model.add(tf.keras.layers.Flatten())

        # Define Dense layer with sigmoid activation for binary classification
        model.add(tf.keras.layers.Dense(1, activation='sigmoid',
                  kernel_regularizer=tf.keras.regularizers.l2(self.lambda1)))

        self.model = model

        # Compile the model
        model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=self.learning_rate),
                      loss='binary_crossentropy',
                      metrics=['accuracy'])

        # Build the model
        model.build((None, self.sentence_length))

        model.summary()

    def train_model(self):
        """
        Trains the sentiment analysis model on the training data.
        """
        try:
            train_dataset = tf.data.Dataset.from_tensor_slices(
                (self.X_train, self.y_train)).batch(self.batch_size)
            val_dataset = tf.data.Dataset.from_tensor_slices(
                (self.X_val, self.y_val)).batch(self.batch_size_val)

            self.hist = self.model.fit(
                train_dataset, epochs=self.epochs, validation_data=val_dataset)

        except Exception as e:
            raise RuntimeError(f"Error occurred during training: {str(e)}")

    def evaluate_model(self):
        """
        Evaluates the model on the test data.
        """
        test_dataset = tf.data.Dataset.from_tensor_slices(
            (self.X_test, self.y_test)).batch(self.batch_size_val)
        accuracy = self.model.evaluate(test_dataset)
        print("Test Accuracy:", accuracy)

    def save_model(self):
        """
        Saves the trained model for future use.

        The model is saved in the specified output path.
        """
        os.makedirs(self.MODEL_OUTPUT_PATH, exist_ok=True)
        self.model.save(os.path.join(
            self.MODEL_OUTPUT_PATH, f'{self.MODEL_NAME}.keras'))

    def visualize_train_history(self, history):
        """
        Visualizes the training history (accuracy and loss).

        Args:
        - history (tf.keras.callbacks.History): Training history.
        """
        plt.figure(figsize=(12, 4))

        # Plot training & validation accuracy values
        plt.subplot(1, 2, 1)
        plt.plot(history.history['accuracy'])
        plt.plot(history.history['val_accuracy'])
        plt.title('Model accuracy')
        plt.xlabel('Epoch')
        plt.ylabel('Accuracy')
        plt.legend(['Train', 'Validation'], loc='upper left')

        # Plot training & validation loss values
        plt.subplot(1, 2, 2)
        plt.plot(history.history['loss'])
        plt.plot(history.history['val_loss'])
        plt.title('Model loss')
        plt.xlabel('Epoch')
        plt.ylabel('Loss')
        plt.legend(['Train', 'Validation'], loc='upper left')

        plt.tight_layout()
        plt.show()

    def main_process(self):
        """
        Executes the main processes of loading, building, training, and evaluating the model.
        """
        self.build_model()
        self.train_model()
        self.evaluate_model()
        self.visualize_train_history(self.hist)
        self.save_model()


# Create an instance of the ReviewSentiment class
review_sentiment_model = ReviewSentiment()

# Execute the main process
review_sentiment_model.main_process()
