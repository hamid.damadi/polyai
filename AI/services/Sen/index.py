import tensorflow as tf
import numpy as np
import re
import os
from keras.preprocessing.sequence import pad_sequences


main_path = './datasets/'


class EmbeddingLayer(tf.keras.layers.Layer):
    def __init__(self, input_length, n_words, embedding_dim, word_index, **kwargs):
        super(EmbeddingLayer, self).__init__(**kwargs)
        self.input_length = input_length
        self.n_words = n_words
        self.embedding_dim = embedding_dim
        self.word_index = word_index

    def build(self, input_shape):
        # Load pre-trained GloVe embeddings
        glove_path = main_path + 'glove.6B.100d.txt'  # Update with your GloVe path
        embeddings_index = {}
        with open(glove_path, 'r', encoding='utf-8') as f:
            for line in f:
                values = line.split()
                word = values[0]
                coefs = np.asarray(values[1:], dtype='float32')
                embeddings_index[word] = coefs

        # Create embedding matrix
        embedding_matrix = np.zeros(
            (self.n_words + 1, self.embedding_dim))  # Adjust size here
        for word, i in self.word_index.items():
            if i <= self.n_words:  # Make sure the index is within bounds
                embedding_vector = embeddings_index.get(word)
                if embedding_vector is not None:
                    embedding_matrix[i] = embedding_vector

        self.embedding = self.add_weight(
            name='embedding',
            shape=(self.n_words + 1, self.embedding_dim),  # Adjust shape here
            trainable=True,
            initializer=tf.keras.initializers.Constant(embedding_matrix),
            dtype=tf.float32
        )
        super(EmbeddingLayer, self).build(input_shape)

    def call(self, inputs):
        inputs = tf.cast(inputs, dtype=tf.int32)
        return tf.nn.embedding_lookup(self.embedding, inputs)


class SentService:
    def __init__(self, model):
        self.max_length = 1000
        self.path = main_path
        self.model = model

    def load_tokenizer(self):
        try:
            path_tokenizer = os.path.join(self.path, 'aclImdb/tokenizer.json')
            with open(path_tokenizer, 'r') as handle:
                tokenizer = tf.keras.preprocessing.text.tokenizer_from_json(
                    handle.read())
            return tokenizer
        except FileNotFoundError:
            raise FileNotFoundError(
                f"Tokenizer file not found at {path_tokenizer}")
        except Exception as e:
            raise RuntimeError(
                f"Error occurred during tokenizer loading: {str(e)}")

    def text_clean(self, text):
        letters = re.sub("[^a-zA-z0-9\s]", " ", text)
        words = letters.lower().split()
        text = " ".join(words)
        return text

    def binary_to_rating(self, binary_prediction):
        return 10 * binary_prediction

    def predict(self, sentence):
        cleaned_sentence = self.text_clean(sentence)
        self.tokenizer = self.load_tokenizer()
        sequence = self.tokenizer.texts_to_sequences([cleaned_sentence])
        padded_sequence = pad_sequences(sequence, maxlen=self.max_length)

        predictions = self.model.predict(padded_sequence)
        rate = self.binary_to_rating(predictions[0])

        print(predictions[0])
        return predictions[0][0].tolist(), rate[0].tolist()
