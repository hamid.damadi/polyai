"""
Handwritten Text Recognition (HTR) Service

This script implements a Handwritten Text Recognition (HTR) service using TensorFlow and Keras.
The HTR service is designed to recognize text content in handwritten images using a Convolutional
Neural Network (CNN) and Long Short-Term Memory (LSTM) layers with 
Connectionist Temporal Classification (CTC) loss.

The model is inspired by the Keras example for handwriting recognition
(https://keras.io/examples/vision/handwriting_recognition/), and the dataset used is IAM.

The key functionalities include:
1. Preprocessing the IAM dataset for training.
2. Building a CNN-LSTM model with CTC loss for HTR.
3. Training the model on the preprocessed dataset.
4. Evaluating the model's performance on a test set.
5. Saving the trained model for future use.
6. Visualizing the training history, including accuracy and loss curves.

Author: Hamid Damadi
Date: 2024
"""

import os
import numpy as np
import tensorflow as tf
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt


class CTCLayer(tf.keras.layers.Layer):
    """
    Custom layer for Connectionist Temporal Classification (CTC) loss.

    This layer computes the CTC loss between true labels and predicted logits.

    Args:
        name (str): Name of the layer.

    Attributes:
        loss_fn: CTC loss function from TensorFlow backend.

    Methods:
        call(y_true, y_pred): Computes the CTC loss and adds it to the model's total loss.

    """

    def __init__(self, name=None):
        super().__init__(name=name)
        self.loss_fn = tf.keras.backend.ctc_batch_cost

    def call(self, y_true, y_pred):
        """
        Computes the CTC loss and adds it to the model's total loss.

        Args:
            y_true (tf.Tensor): True labels.
            y_pred (tf.Tensor): Predicted logits.

        Returns:
            tf.Tensor: Predicted logits.

        """
        batch_len = tf.cast(tf.shape(y_true)[0], dtype="int64")
        input_length = tf.cast(tf.shape(y_pred)[1], dtype="int64")
        label_length = tf.cast(tf.shape(y_true)[1], dtype="int64")

        input_length = input_length * \
            tf.ones(shape=(batch_len, 1), dtype="int64")
        label_length = label_length * \
            tf.ones(shape=(batch_len, 1), dtype="int64")
        loss = self.loss_fn(y_true, y_pred, input_length, label_length)
        self.add_loss(loss)

        # At test time, just return the computed predictions.
        return y_pred


class EditDistanceCallback(tf.keras.callbacks.Callback):
    """
    Custom callback to calculate and print the mean edit distance during model training.

    Args:
        pred_model (tf.keras.Model): Model for making predictions.
        max_len (int): Maximum length of sequences.
        validation_images (List[tf.Tensor]): List of validation images.
        validation_labels (List[tf.Tensor]): List of validation labels.

    Attributes:
        prediction_model: Model for making predictions.
        max_len (int): Maximum length of sequences.
        validation_images (List[tf.Tensor]): List of validation images.
        validation_labels (List[tf.Tensor]): List of validation labels.

    Methods:
        calculate_edit_distance(labels, predictions, max_len): Calculates the mean edit distance between labels and predictions.
        on_epoch_end(epoch, logs): Callback function called at the end of each epoch.

    """

    def __init__(self, pred_model, max_len, validation_images, validation_labels):
        super().__init__()
        self.prediction_model = pred_model
        self.max_len = max_len
        self.validation_images = validation_images
        self.validation_labels = validation_labels

    def calculate_edit_distance(self, labels, predictions, max_len):
        """
        Calculates the mean edit distance between labels and predictions.

        Args:
            labels (tf.Tensor): True labels.
            predictions (np.ndarray): Predicted logits.
            max_len (int): Maximum length of sequences.

        Returns:
            tf.Tensor: Mean edit distance.

        """
        # Get a single batch and convert its labels to sparse tensors.
        saprse_labels = tf.cast(tf.sparse.from_dense(labels), dtype=tf.int64)

        # Make predictions and convert them to sparse tensors.
        input_len = np.ones(predictions.shape[0]) * predictions.shape[1]
        predictions_decoded = tf.keras.backend.ctc_decode(
            predictions, input_length=input_len, greedy=True
        )[0][0][:, :max_len]
        sparse_predictions = tf.cast(
            tf.sparse.from_dense(predictions_decoded), dtype=tf.int64
        )

        # Compute individual edit distances and average them out.
        edit_distances = tf.edit_distance(
            sparse_predictions, saprse_labels, normalize=False
        )
        return tf.reduce_mean(edit_distances)

    def on_epoch_end(self, epoch, logs=None):
        """
        Callback function called at the end of each epoch.

        Args:
            epoch (int): Current epoch.
            logs (dict): Dictionary containing metrics.

        """
        edit_distances = []

        for i in range(len(self.validation_images)):
            labels = self.validation_labels[i]
            predictions = self.prediction_model.predict(
                self.validation_images[i])
            edit_distances.append(self.calculate_edit_distance(
                labels, predictions, self.max_len).numpy())

        print(
            f"Mean edit distance for epoch {epoch + 1}: {np.mean(edit_distances):.4f}"
        )


class HTRService:
    """
        Handwritten Text Recognition (HTR) service for building, training, and evaluating a model.

        Attributes:
            DATA_INPUT_PATH (str): Path to the dataset.
            MODEL_OUTPUT_PATH (str): Path to save the trained model.
            MODEL_NAME (str): Name of the model.
            image_size (Tuple[int, int]): Size of input images.
            train_set (tf.data.Dataset): Training dataset.
            val_set (tf.data.Dataset): Validation dataset.
            test_set (tf.data.Dataset): Test dataset.
            model (tf.keras.Model): HTR model.
            prediction_model (tf.keras.Model): Prediction model.
            hist (tf.keras.callbacks.History): Training history.
            batch_size (int): Batch size for training.
            epochs (int): Number of training epochs.
            num_to_char (tf.keras.layers.StringLookup): Mapping integers back to original characters.
            char_to_num (tf.keras.layers.StringLookup): Mapping characters to integers.
            max_len (int): Maximum sequence length.
            characters (set): Set of unique characters in the dataset.
            padding_token (int): Padding token value.
            images_path (List[str]): List of image file paths.
            labels (List[str]): List of corresponding labels.

        Methods:
            preprocess_dataset(): Preprocesses the dataset to extract image paths and labels.
            distortion_free_resize(image, img_size): Resizes an image without distortion.
            preprocess_image(image_path, img_size): Reads, decodes, and preprocesses an image.
            vectorize_label(label): Converts a label string to numerical format.
            process_images_labels(image_path, label): Preprocesses a pair of image and label.
            prepare_dataset(image_paths, labels): Prepares a TensorFlow dataset.
            split_dataset(): Splits the dataset into training, validation, and test sets.
            build_model(): Builds the HTR model.
            train_model(): Trains the HTR model.
            evaluate_model(): Evaluates the trained model on the test set.
            save_model(): Saves the trained model to the specified path.
            visualize_train_history(history): Visualizes the training history.

    """

    def __init__(self):
        self.DATA_INPUT_PATH = "###YOUR_DATASET_INPUT_PATH###"
        self.MODEL_OUTPUT_PATH = "###YOUR_MODEL_OUTPUT_PATH###"
        self.MODEL_NAME = "###YOUR_MODEL_NAME###"
        self.image_size = (128, 32)
        self.train_set = None
        self.val_set = None
        self.test_set = None
        self.model = None
        self.prediction_model = None
        self.hist = None
        self.batch_size = 64
        self.epochs = 50
        self.num_to_char = None
        self.char_to_num = None
        self.max_len = 0
        self.characters = set()
        self.padding_token = 99
        self.images_path = []
        self.labels = []

    # Preprocessing function

    def preprocess_dataset(self):
        """
            Preprocesses the dataset to extract image paths and labels.

            Reads the IAM dataset and extracts image paths and corresponding labels.

        """
        images_path = []
        labels = []

        with open(os.path.join(self.DATA_INPUT_PATH, 'iam_words', 'words.txt'), 'r') as file:
            lines = file.readlines()

            for line_number, line in enumerate(lines):
                # Skip comments and empty lines
                if line.startswith('#') or line.strip() == '':
                    continue

                # Split the line and extract information
                parts = line.strip().split()

                # Continue with the rest of the code
                word_id = parts[0]

                first_folder = word_id.split("-")[0]
                second_folder = first_folder + '-' + word_id.split("-")[1]

                # Construct the image filename
                image_filename = f"{word_id}.png"
                image_path = os.path.join(
                    self.DATA_INPUT_PATH, 'iam_words', 'words', first_folder, second_folder, image_filename)

                # Check if the image file exists
                if os.path.isfile(image_path) and os.path.getsize(image_path):

                    images_path.append(image_path)

                    # Extract labels
                    label = parts[-1].strip()
                    for char in label:
                        self.characters.add(char)

                    self.max_len = max(self.max_len, len(label))
                    labels.append(label)
                else:
                    print(f"Error in Line: ${line_number}")

        self.images_path = images_path
        self.labels = labels

        self.characters = sorted(list(self.characters))

        print('characters: ', self.characters)
        print('max_len: ', self.max_len)
        # Mapping characters to integers.
        self.char_to_num = tf.keras.layers.StringLookup(
            vocabulary=list(self.characters), mask_token=None)

        # Mapping integers back to original characters.
        self.num_to_char = tf.keras.layers.StringLookup(
            vocabulary=self.char_to_num.get_vocabulary(), mask_token=None, invert=True
        )

    def distortion_free_resize(self, image, img_size):
        """
            Resizes an image without distortion.

            Resizes an image to the target size without introducing distortion.

            Args:
                image (tf.Tensor): Input image.
                img_size (Tuple[int, int]): Target size.

            Returns:
                tf.Tensor: Resized image.

        """
        w, h = img_size
        image = tf.image.resize(image, size=(h, w), preserve_aspect_ratio=True)

        # Check tha amount of padding needed to be done.
        pad_height = h - tf.shape(image)[0]
        pad_width = w - tf.shape(image)[1]

        # Only necessary if you want to do same amount of padding on both sides.
        if pad_height % 2 != 0:
            height = pad_height // 2
            pad_height_top = height + 1
            pad_height_bottom = height
        else:
            pad_height_top = pad_height_bottom = pad_height // 2

        if pad_width % 2 != 0:
            width = pad_width // 2
            pad_width_left = width + 1
            pad_width_right = width
        else:
            pad_width_left = pad_width_right = pad_width // 2

        image = tf.pad(
            image,
            paddings=[
                [pad_height_top, pad_height_bottom],
                [pad_width_left, pad_width_right],
                [0, 0],
            ],
        )

        image = tf.transpose(image, perm=[1, 0, 2])
        image = tf.image.flip_left_right(image)
        return image

    def preprocess_image(self, image_path, img_size):
        """
            Reads, decodes, and preprocesses an image.

            Reads an image from the specified path, decodes it, and preprocesses it.

            Args:
                image_path (str): Path to the image file.
                img_size (Tuple[int, int]): Target size.

            Returns:
                tf.Tensor: Preprocessed image.

        """
        image = tf.io.read_file(image_path)
        image = tf.image.decode_png(image, 1)
        image = self.distortion_free_resize(image, img_size)
        image = tf.cast(image, tf.float32) / 255.0
        return image

    def vectorize_label(self, label):
        """
            Converts a label string to numerical format.

            Converts a label string to a numerical format using character-to-number mapping.

            Args:
                label (str): Input label string.

            Returns:
                tf.Tensor: Numerical representation of the label.

        """
        label = self.char_to_num(tf.strings.unicode_split(
            label, input_encoding="UTF-8"))
        length = tf.shape(label)[0]
        pad_amount = self.max_len - length
        label = tf.pad(label, paddings=[[0, pad_amount]],
                       constant_values=self.padding_token)
        return label

    def process_images_labels(self, image_path, label):
        """
        Preprocesses a pair of image and label.

        Preprocesses a pair of image and label, converting them to the desired format.

        Args:
            image_path (str): Path to the image file.
            label (str): Input label string.

        Returns:
            Dict[str, tf.Tensor]: Dictionary containing the preprocessed image and label.

        """
        image = self.preprocess_image(image_path, self.image_size)
        label = self.vectorize_label(label)
        return {"image": image, "label": label}

    def prepare_dataset(self, image_paths, labels):
        """
        Prepares a TensorFlow dataset.

        Prepares a TensorFlow dataset from the provided image paths and labels.

        Args:
            image_paths (List[str]): List of image file paths.
            labels (List[str]): List of corresponding labels.

        Returns:
            tf.data.Dataset: Prepared TensorFlow dataset.

        """
        AUTOTUNE = tf.data.AUTOTUNE
        dataset = tf.data.Dataset.from_tensor_slices((image_paths, labels)).map(
            self.process_images_labels, num_parallel_calls=AUTOTUNE
        )
        return dataset.batch(self.batch_size).cache().prefetch(AUTOTUNE)

    def split_dataset(self):
        """
        Splits the dataset into training, validation, and test sets.

        Splits the dataset into training, validation, and test sets using train_test_split.

        """
        # Split the data into training, validation, and test sets using train_test_split
        train_images, test_images, train_labels, test_labels = train_test_split(
            self.images_path, self.labels, test_size=0.2, random_state=42
        )

        # Further split the test set into validation and final test sets
        val_images, test_images, val_labels, test_labels = train_test_split(
            test_images, test_labels, test_size=0.5, random_state=42
        )

        self.train_set = self.prepare_dataset(train_images, train_labels)
        self.val_set = self.prepare_dataset(val_images, val_labels)
        self.test_set = self.prepare_dataset(test_images, test_labels)

    def build_model(self):
        """
        Build and compile the Handwritten Text Recognition (HTR) model.

        The model consists of convolutional and recurrent layers with a CTC loss layer.

        """
        # Define the input layers
        input_img = tf.keras.Input(
            shape=(self.image_size[0], self.image_size[1], 1), name="image")
        labels = tf.keras.layers.Input(name="label", shape=(None,))

        # Model architecture
        x = tf.keras.layers.Conv2D(
            32,
            (3, 3),
            activation="relu",
            kernel_initializer="he_normal",
            padding="same",
        )(input_img)
        x = tf.keras.layers.MaxPooling2D((2, 2))(x)
        x = tf.keras.layers.Conv2D(
            64,
            (3, 3),
            activation="relu",
            kernel_initializer="he_normal",
            padding="same",
        )(x)
        x = tf.keras.layers.MaxPooling2D((2, 2))(x)
        new_shape = ((self.image_size[0] // 4), (self.image_size[1] // 4) * 64)
        x = tf.keras.layers.Reshape(target_shape=new_shape)(x)
        x = tf.keras.layers.Dense(64, activation="relu")(x)
        x = tf.keras.layers.Dropout(0.2)(x)
        x = tf.keras.layers.Bidirectional(
            tf.keras.layers.LSTM(128, return_sequences=True, dropout=0.25)
        )(x)
        x = tf.keras.layers.Bidirectional(
            tf.keras.layers.LSTM(64, return_sequences=True, dropout=0.25)
        )(x)
        x = tf.keras.layers.Dense(
            len(self.char_to_num.get_vocabulary()) + 2, activation="softmax", name="dense2"
        )(x)

        # Define the CTC loss layer
        output = CTCLayer(name="ctc_loss")(labels, x)

        # Create the model
        self.model = tf.keras.models.Model(
            inputs=[input_img, labels], outputs=output
        )

        # Compile the model
        self.model.compile(
            optimizer=tf.keras.optimizers.Adam(learning_rate=0.001)
        )

        # Display model summary
        self.model.summary()

    def train_model(self):
        """
        Train the Handwritten Text Recognition (HTR) model.

        Uses the training dataset and performs model training with early stopping.

        """
        validation_images = []
        validation_labels = []

        # Collect validation images and labels for edit distance calculation
        for batch in self.val_set:
            validation_images.append(batch["image"])
            validation_labels.append(batch["label"])

        # Create a separate prediction model for edit distance calculation
        self.prediction_model = tf.keras.models.Model(
            self.model.get_layer(name="image").input, self.model.get_layer(
                name="dense2").output
        )

        # Create an edit distance callback
        edit_distance_callback = EditDistanceCallback(
            self.prediction_model, self.max_len, validation_images, validation_labels)

        # Create early stopping callback
        early_stopping = tf.keras.callbacks.EarlyStopping(
            monitor="val_loss", patience=10, restore_best_weights=True
        )

        # Train the model
        self.hist = self.model.fit(
            self.train_set,
            validation_data=self.val_set,
            epochs=self.epochs,
            callbacks=[edit_distance_callback, early_stopping],
        )

    def evaluate_model(self):
        """
        Evaluate the Handwritten Text Recognition (HTR) model on the test dataset.

        Prints the test accuracy of the model.

        """
        accuracy = self.model.evaluate(self.test_set)
        print("Test Accuracy:", accuracy)

    def save_model(self):
        """
        Save the Handwritten Text Recognition (HTR) model to the specified output path.

        Saves the prediction model in the TensorFlow SavedModel format.

        """
        os.makedirs(self.MODEL_OUTPUT_PATH, exist_ok=True)
        self.prediction_model.save(os.path.join(
            self.MODEL_OUTPUT_PATH, f'{self.MODEL_NAME}.keras'))

    def visualize_train_history(self, history):
        """
        Visualize the training history of the Handwritten Text Recognition (HTR) model.

        Plots training and validation accuracy, as well as training and validation loss.

        Args:
            history (tf.keras.callbacks.History): Training history.

        """
        plt.figure(figsize=(12, 4))

        # Plot training & validation accuracy values
        plt.subplot(1, 2, 1)
        plt.plot(history.history['accuracy'])
        plt.plot(history.history['val_accuracy'])
        plt.title('Model accuracy')
        plt.xlabel('Epoch')
        plt.ylabel('Accuracy')
        plt.legend(['Train', 'Validation'], loc='upper left')

        # Plot training & validation loss values
        plt.subplot(1, 2, 2)
        plt.plot(history.history['loss'])
        plt.plot(history.history['val_loss'])
        plt.title('Model loss')
        plt.xlabel('Epoch')
        plt.ylabel('Loss')
        plt.legend(['Train', 'Validation'], loc='upper left')

        plt.tight_layout()
        plt.show()

    def main_process(self):
        """
        Main process for Handwritten Text Recognition (HTR).

        This method orchestrates the entire HTR workflow, including dataset preprocessing,
        model building, training, evaluation, saving, and visualization of the training history.

        """
        # Preprocess the dataset
        self.preprocess_dataset()

        # Split the dataset into training, validation, and test sets
        self.split_dataset()

        # Build the Handwritten Text Recognition (HTR) model
        self.build_model()

        # Train the HTR model
        self.train_model()

        # Evaluate the HTR model on the test dataset
        self.evaluate_model()

        # Save the trained model
        self.save_model()

        # Visualize the training history
        self.visualize_train_history(self.hist)

# Instantiate the HTRService class and run the main process
imgRec = HTRService()
imgRec.main_process()