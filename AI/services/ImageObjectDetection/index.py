import numpy as np
import cv2
import keras_cv
import matplotlib.pyplot as plt

CLASS_IDS = [
    "Aeroplane",
    "Bicycle",
    "Bird",
    "Boat",
    "Bottle",
    "Bus",
    "Car",
    "Cat",
    "Chair",
    "Cow",
    "Dining Table",
    "Dog",
    "Horse",
    "Motorbike",
    "Person",
    "Potted Plant",
    "Sheep",
    "Sofa",
    "Train",
    "Tvmonitor",
    "Total",
]

inference_resizing = keras_cv.layers.Resizing(
    640, 640, pad_to_aspect_ratio=True, bounding_box_format="xywh"
)

prediction_decoder = keras_cv.layers.NonMaxSuppression(
    bounding_box_format="xywh",
    from_logits=True,
    # Decrease the required threshold to make predictions get pruned out
    iou_threshold=0.2,
    # Tune confidence threshold for predictions to pass NMS
    confidence_threshold=0.6
)


class ImageObjectDetectionService:
    def __init__(self, model, share_folder):

        self.class_mapping = dict(zip(range(len(CLASS_IDS)), CLASS_IDS))

        self.model = model
        self.share_folder = share_folder
        self.model.prediction_decoder = prediction_decoder

    def predict(self, path):
        image = cv2.imread(self.share_folder + path)
        image = np.array(image)
        image_batch = inference_resizing([image])
        y_pred = self.model.predict(image_batch)
        # fig, ax = plt.subplots()
        keras_cv.visualization.plot_bounding_box_gallery(
            image_batch,
            value_range=(0, 255),
            rows=1,
            cols=1,
            y_pred=y_pred,
            scale=5,
            font_scale=0.7,
            bounding_box_format="xywh",
            class_mapping=self.class_mapping
        )
        fig = plt.gcf()

        fig.savefig(f"{self.share_folder}pred-{path}")

        return ['OK']
