import numpy as np 
import pandas as pd 
import os
import tensorflow as tf
import tensorflow_datasets as tfds
from collections import Counter
from transformers import BertTokenizer, TFBertModel

dataset_name = "universal_dependencies/en_ewt:1.0.1"
train_val_ds, info = tfds.load(dataset_name, split=["train", "dev"], with_info=True)

def extract_sentence_label(element):
  """Extracts sentence and label from a dataset element.

  Args:
      element: A dictionary element from the TFDS dataset.

  Returns:
      A tuple of tensors:
          - sentence: A TensorFlow string tensor containing the sentence text.
          - label: A TensorFlow tensor containing the corresponding label (integer or string).
  """
  return element["text"], element["upos"]

def preprocess_sentence(item):
    """Preprocesses a sentence for POS tagging.

    Args:
        sentence: A TensorFlow string tensor containing the sentence text.
        label: A TensorFlow tensor containing the corresponding label (integer or string).

    Returns:
        A tuple of tensors:
            - tokens: A TensorFlow string tensor containing the tokenized sentence (individual words).
            - labels: A TensorFlow int32 tensor containing the integer-encoded POS tags for each word.
    """
    sentence, label = extract_sentence_label(item)
    sentence = tf.strings.lower(sentence)  # Lowercase conversion
    sentence = tf.strings.regex_replace(sentence, r"[^\w\s]", "")  # Punctuation removal
    tokens = tf.strings.split(sentence)  # Tokenization

    # Convert labels to integers if needed (assuming string labels)
    if label.dtype == tf.string:
        label = tf.strings.to_number(label, out_type=tf.int32)

    return tokens, label

# Separate preprocessing for training and validation sets
train_ds = train_val_ds[0].map(preprocess_sentence)
val_ds = train_val_ds[1].map(preprocess_sentence)

def build_vocab():

    all_tokens = []
    all_labels = []

    for tokens, labels in train_ds:
        all_tokens.extend(tokens.numpy())
        all_labels.extend(labels.numpy())

    token_counts = Counter(all_tokens)
    label_counts = Counter(all_labels)
    
    
    # Get unique tokens and calculate vocabulary size
    unique_tokens = set(all_tokens)
    vocab_size = len(unique_tokens)

    unique_labels = set(all_labels)
    num_labels = len(unique_labels)

    # Filter low-frequency tokens (optional)
    min_token_count = 5  # Adjust threshold as needed
    filtered_tokens = [token for token, count in token_counts.items() if count >= min_token_count]

    # Create vocabulary mappings (token -> integer ID, label -> integer ID)
    token_to_id = {token: i for i, token in enumerate(filtered_tokens)}
    label_to_id = {label: i for i, label in enumerate(set(all_labels))}
    
    return token_to_id, label_to_id, vocab_size, num_labels

def prepare_data(data, token_to_id, label_to_id, max_len=100):
    X, y = [], []
    for tokens, labels in data:
        # Convert tokens and labels to integer IDs
        X.append([token_to_id.get(token, 0) for token in tokens.numpy()])
        y.append([label_to_id[label] for label in labels.numpy()])

        # Pad sequences (optional)
        X[-1] = tf.keras.utils.pad_sequences([X[-1]], maxlen=max_len, padding="post", truncating="post")[0]
        y[-1] = tf.keras.utils.pad_sequences([y[-1]], maxlen=max_len, padding="post", truncating="post")[0]

    return np.array(X), np.array(y)

token_to_id, label_to_id, vocab_size, num_labels = build_vocab() 
train_X, train_y = prepare_data(train_ds, token_to_id, label_to_id)
val_X, val_y = prepare_data(val_ds, token_to_id, label_to_id)


# Load tokenizer and model from pre-trained BERT model
# bert_preprocessor = BertTokenizer.from_pretrained("bert-base-uncased")
# bert_model = TFBertModel.from_pretrained("bert-base-uncased")

def create_model(vocab_size, num_labels, max_len=100):
    inputs = tf.keras.layers.Input(shape=(max_len,), dtype=tf.int32)
    
    # Reshape the input to meet LSTM layer requirements
    reshaped_inputs = tf.keras.layers.Reshape((1, max_len))(inputs)
    
    # LSTM layer
    lstm_layer = tf.keras.layers.LSTM(128, return_sequences=True)(reshaped_inputs)
    
    # Output layer
    outputs = tf.keras.layers.Dense(num_labels, activation='softmax')(lstm_layer)
    
    # Create and compile the model
    model = tf.keras.Model(inputs=inputs, outputs=outputs)
    model.compile(loss="sparse_categorical_crossentropy", optimizer="adam", metrics=["accuracy"])
    
    return model

model = create_model(vocab_size, num_labels)  # Replace with actual values
model.compile(loss="sparse_categorical_crossentropy", optimizer="adam", metrics=["accuracy"])

model.fit(
    train_X, 
    train_y, 
    epochs=10, 
    batch_size=32, 
    validation_data=[val_X, val_y]
)