import numpy as np
import matplotlib.pyplot as plt
import random


class CGanService:
    def __init__(self, model, share_folder):
        self.model = model
        self.share_folder = share_folder

    def predict(self, index):
        response = []
        for _ in range(0, 3):
            samples_test = np.random.normal(0, 1, (1, 100))
            labels_test = np.array([int(index)]).reshape((-1, 1))
            results = self.model.predict([samples_test, labels_test])
            fileRand = random.randint(1000000, 10000000)
            filePath = f"{self.share_folder}image_{fileRand}.png"
            plt.imsave(filePath, results[0].reshape(28, 28))
            response.append({
                "path": f"image_{fileRand}.png",
                "index": index
            })
        return response
