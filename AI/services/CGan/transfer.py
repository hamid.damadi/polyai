"""
CGANMnist Class

This class implements a Conditional Generative Adversarial Network (CGAN) on the MNIST dataset using TensorFlow and Keras.

Key functionalities include:
1. Defining a generator model for generating images.
2. Defining a discriminator model for distinguishing real from fake images.
3. Building the CGAN model by combining the generator and discriminator.
4. Training the CGAN model on the MNIST dataset.
5. Saving the trained CGAN, discriminator, and generator models for future use.

Author: Hamid Damadi
Date: 2023
"""

import numpy as np
import os
import matplotlib.pyplot as plt
import tensorflow as tf


class CGANMnist:
    def __init__(self):
        np.random.seed(42)
        self.MODEL_OUTPUT_PATH = "###YOUR_MODEL_OUTPUT_PATH###"
        self.CGAN_MODEL_NAME = "###YOUR_CGAN_MODEL_NAME###"
        self.D_MODEL_NAME = "###YOUR_D_MODEL_NAME###"
        self.G_MODEL_NAME = "###YOUR_G_MODEL_NAME###"
        self.random_dim = 100
        self.latent_dim = 100
        self.epochs = 15005
        self.batch_size = 128
        self.save_interval = 1000
        self.cgan = None
        self.d_model = None
        self.g_model = None

    def define_generator(self, latent_dim):
        """
        Defines the generator model for generating images.

        Parameters:
        - latent_dim (int): Dimension of the latent space.

        Returns:
        - tf.keras.models.Model: Generator model.
        """
        model = tf.keras.Sequential([
            tf.keras.layers.Dense(
                128*7*7, activation='relu', input_dim=latent_dim),
            tf.keras.layers.Reshape((7, 7, 128)),
            tf.keras.layers.UpSampling2D(),
            tf.keras.layers.Conv2D(
                128, 3, padding='same', activation='relu'),
            tf.keras.layers.BatchNormalization(momentum=0.8),
            tf.keras.layers.UpSampling2D(),
            tf.keras.layers.Conv2D(64, 3, padding='same', activation='relu'),
            tf.keras.layers.BatchNormalization(momentum=0.8),
            tf.keras.layers.Conv2D(1, 3, padding='same', activation='tanh')
        ])

        z = tf.keras.layers.Input(shape=(latent_dim,))
        label = tf.keras.layers.Input(shape=(1,), dtype='uint8')
        label_embed = tf.keras.layers.Embedding(
            10, latent_dim, input_length=1)(label)
        label_embed = tf.keras.layers.Flatten()(label_embed)
        combined = tf.keras.layers.multiply([z, label_embed])

        img = model(combined)

        return tf.keras.models.Model([z, label], img)

    def get_g_model(self, latent_dim):
        """
        Retrieves the generator model.

        Parameters:
        - latent_dim (int): Dimension of the latent space.

        Returns:
        - tf.keras.models.Model: Generator model.
        """
        g_model = self.define_generator(latent_dim)
        g_model.summary()
        tf.keras.utils.plot_model(
            g_model, to_file=os.path.join(self.MODEL_OUTPUT_PATH, 'diagrams/generator.png'), show_shapes=True, show_layer_names=True)
        return g_model

    def define_discriminator(self):
        """
        Defines the discriminator model for distinguishing real from fake images.

        Returns:
        - tf.keras.models.Model: Discriminator model.
        """
        model = tf.keras.Sequential([
            tf.keras.layers.Conv2D(32, (3, 3), 2,
                                   input_shape=(28, 28, 2), padding='same'),
            tf.keras.layers.LeakyReLU(0.2),
            tf.keras.layers.Dropout(0.25),
            tf.keras.layers.Conv2D(64, (3, 3), 2, padding='same'),
            tf.keras.layers.LeakyReLU(0.2),
            tf.keras.layers.Dropout(0.25),
            tf.keras.layers.Flatten(),
            tf.keras.layers.Dense(512, activation='relu'),
            tf.keras.layers.Dropout(0.25),
            tf.keras.layers.Dense(1, activation='sigmoid')
        ])

        img = tf.keras.layers.Input(shape=(28, 28, 1))
        label = tf.keras.layers.Input(shape=(1,))
        label_embed = tf.keras.layers.Embedding(
            input_dim=10, output_dim=np.prod((28, 28)), input_length=1)(label)
        label_embed = tf.keras.layers.Flatten()(label_embed)
        label_embed = tf.keras.layers.Reshape((28, 28, 1))(label_embed)
        concatenated = tf.keras.layers.Concatenate(axis=-1)([label_embed, img])
        prediction = model(concatenated)
        return tf.keras.models.Model([img, label], prediction)

    def get_d_model(self):
        """
        Retrieves the discriminator model.

        Returns:
        - tf.keras.models.Model: Discriminator model.
        """
        d_model = self.define_discriminator()
        d_model.summary()
        tf.keras.utils.plot_model(
            d_model, to_file=os.path.join(self.MODEL_OUTPUT_PATH, 'diagrams/discriminator.png'), show_shapes=True, show_layer_names=True)
        return d_model

    def get_cgan_model(self):
        """
        Retrieves the CGAN model, discriminator, and generator models.

        Returns:
        - Tuple[tf.keras.models.Model, tf.keras.models.Model, tf.keras.models.Model]: CGAN, discriminator, and generator models.
        """
        d_model = self.get_d_model()
        g_model = self.get_g_model(self.latent_dim)
        d_model.compile(loss='binary_crossentropy', metrics=[
            'accuracy'], optimizer='adam')

        z = tf.keras.layers.Input(shape=(100,))
        label = tf.keras.layers.Input(shape=(1,))
        img = g_model([z, label])

        d_model.trainable = False
        prediction = d_model([img, label])
        cgan = tf.keras.models.Model([z, label], prediction)
        cgan.compile(loss='binary_crossentropy',
                     optimizer='adam', metrics=['accuracy'])
        cgan.summary()
        tf.keras.utils.plot_model(
            cgan, to_file=os.path.join(self.MODEL_OUTPUT_PATH, 'diagrams/cgan.png'), show_shapes=True, show_layer_names=True)
        return cgan, d_model, g_model

    def load_dataset(self):
        """
        Loads and preprocesses the MNIST dataset.

        Returns:
        - Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]: Training and test data and labels.
        """
        (x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data()
        x_train, x_test = (x_train-127.5)/127.5, (x_test-127.5)/127.5
        x_train = x_train.reshape((-1, 28, 28, 1))
        x_test = x_test.reshape((-1, 28, 28, 1))
        return (x_train, y_train, x_test, y_test)

    def plot_image(self, images, labels, rows, cols):
        """
        Plots images in a grid.

        Parameters:
        - images (np.ndarray): Images to be plotted.
        - labels (np.ndarray): Corresponding labels for each image.
        - rows (int): Number of rows in the grid.
        - cols (int): Number of columns in the grid.
        """
        fig = plt.figure(figsize=(8, 8))
        for i in range(1, cols*rows + 1):
            img = images[i-1]
            ax = fig.add_subplot(rows, cols, i)
            ax.title.set_text(labels[i-1])
            plt.imshow(img.reshape((28, 28)))
        fig.tight_layout()
        plt.show()

    def train_model(self):
        """
        Trains the CGAN model on the MNIST dataset.
        """
        cgan, d_model, g_model = self.get_cgan_model()
        x_train, y_train, _, _ = self.load_dataset()
        valid = np.ones((self.batch_size, 1))
        fake = np.zeros((self.batch_size, 1))

        losses = {"G": [], "D": []}

        samples_test = np.random.normal(0, 1, (16, 100))
        labels_test = np.random.randint(0, 10, 16).reshape((-1, 1))

        for epoch in range(self.epochs):
            idx = np.random.randint(0, x_train.shape[0], self.batch_size)
            real_imgs, labels = x_train[idx], y_train[idx]

            noise = np.random.normal(0, 1, (self.batch_size, 100))
            fake_imgs = g_model.predict([noise, labels])

            # train discriminator
            d_model.trainable = True
            d_loss_real = d_model.train_on_batch([real_imgs, labels], valid)
            d_loss_fake = d_model.train_on_batch([fake_imgs, labels], fake)

            d_loss = 0.5 * np.add(d_loss_real, d_loss_fake)

            d_model.trainable = False

            z = np.random.normal(0, 1, size=(self.batch_size, 100))
            labels = np.random.randint(0, 10, self.batch_size).reshape((-1, 1))

            # train generator
            g_loss = cgan.train_on_batch([z, labels], valid)

            losses["G"].append(g_loss)
            losses["D"].append(d_loss)

            if epoch % self.save_interval == 0:
                print("Losses(d,g):", d_loss[0], g_loss)
                results = g_model.predict([samples_test, labels_test])
                self.plot_image(results, labels_test, 4, 4)
        self.cgan = cgan
        self.d_model = d_model
        self.g_model = g_model

    def save_model(self):
        """
        Saves the trained CGAN, discriminator, and generator models.
        """
        os.makedirs(self.MODEL_OUTPUT_PATH, exist_ok=True)
        self.cgan.save(os.path.join(
            self.MODEL_OUTPUT_PATH, f'{self.CGAN_MODEL_NAME}.keras'))
        self.d_model.save(os.path.join(
            self.MODEL_OUTPUT_PATH, f'{self.D_MODEL_NAME}.keras'))
        self.g_model.save(os.path.join(
            self.MODEL_OUTPUT_PATH, f'{self.G_MODEL_NAME}.keras'))

    def main_process(self):
        """
        Executes the main processes of training and saving the models.
        """
        self.train_model()
        self.save_model()


# Create an instance of the CGANMnist class and execute the main process
cGANMnist = CGANMnist()
cGANMnist.main_process()
