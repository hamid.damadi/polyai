import tensorflow as tf
import pandas as pd
import numpy as np


class NERService:
    def __init__(self, model):
        self.model = model
        self.data = pd.read_csv(
            "./datasets/ner/ner_dataset.csv", encoding="latin1")
        self.data = self.data.fillna(method="ffill")
        self.max_len = 128

        def agg_func(s): return [(w, t) for w, t in zip(s["Word"].values.tolist(),
                                                        s["Tag"].values.tolist())]

        agg_data = self.data.groupby(['Sentence #']).apply(
            agg_func).reset_index().rename(columns={0: 'Sentence_POS_Tag_Pair'})

        agg_data['Sentence'] = agg_data['Sentence_POS_Tag_Pair'].apply(
            lambda sentence: " ".join([s[0] for s in sentence]))
        agg_data['Tag'] = agg_data['Sentence_POS_Tag_Pair'].apply(
            lambda sentence: " ".join([s[1] for s in sentence]))

        agg_data['tag_list'] = agg_data['Tag'].apply(lambda x: x.split())

        self.tokenised_sentences = agg_data['Sentence'].apply(
            lambda x: x.split())
        self.sentences_list = agg_data['Sentence'].tolist()
        self.tags_list = agg_data['tag_list'].tolist()

    def getSamples(self):

        i = np.random.randint(0, len(self.sentences_list))
        return self.tokenised_sentences[i], i

    def predict(self, index):
        tokeniser = tf.keras.preprocessing.text.Tokenizer(
            lower=False, filters='')

        tokeniser.fit_on_texts(self.sentences_list)

        encoded_sentence = tokeniser.texts_to_sequences(self.sentences_list)

        tags = ['I-org', 'B-gpe', 'B-org', 'I-gpe', 'I-eve', 'I-nat', 'B-nat', 'I-per',
                'O', 'I-tim', 'B-eve', 'I-geo', 'I-art', 'B-tim', 'B-per', 'B-geo', 'B-art']

        tags_map = {tag: i for i, tag in enumerate(tags)}

        reverse_tag_map = {v: k for k, v in tags_map.items()}

        padded_encoded_sentences = tf.keras.preprocessing.sequence.pad_sequences(
            maxlen=self.max_len, sequences=encoded_sentence, padding="post", value=0)

        p = self.model.predict(
            np.array([padded_encoded_sentences[int(index)]]))

        df = pd.DataFrame()
        df['pred_target_index'] = np.argmax(p, axis=2)[0]
        df['pred_target_tag'] = df['pred_target_index'].apply(
            lambda x: reverse_tag_map[x])

        return df['pred_target_tag'].tolist()
