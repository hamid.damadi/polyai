"""
Named Entity Recognition (NER) Service

This script defines a Named Entity Recognition (NER) service using a Long Short-Term Memory (LSTM) network
with pre-trained GloVe word embeddings. It includes data loading, preprocessing, model training, and evaluation.

Requirements:
- TensorFlow (tf)
- pandas (pd)
- numpy (np)
- matplotlib.pyplot as plt
- scikit-learn's classification_report
- GloVe pre-trained word embeddings file (downloadable from https://nlp.stanford.edu/projects/glove/)

Usage:
1. Update the paths and parameters in the __init__ method.
2. Ensure the correct file path for the GloVe embeddings file in the EMBEDDING_PATH attribute.
3. Run the script to train the NER model and evaluate its performance.

Author: Hamid Damadi
Date: 2023
"""

from sklearn.model_selection import train_test_split
import tensorflow as tf
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import classification_report

# Set random seed for reproducibility
np.random.seed(42)
plt.style.use("ggplot")


class NERService:
    def __init__(self):
        """
        Named Entity Recognition (NER) service for processing text data.

        Attributes:
            DATA_INPUT_PATH (str): Path to the input dataset.
            MODEL_OUTPUT_PATH (str): Path to save the trained model.
            MODEL_NAME (str): Name of the trained model.
            EMBEDDING_PATH (str): Path to the pre-trained GloVe word embeddings file.
            lstm_units (int): Number of LSTM units in the model.
            dense_units (int): Number of units in the dense layer.
            dropout_rate (float): Dropout rate for regularization.
            max_len (int): Maximum length of input sequences.
            embedding_dim (int): Dimension of word embeddings.
            batch_size (int): Number of samples per batch during training.
            epochs (int): Number of training epochs.
            model (tf.keras.Model): NER model.
            hist (tf.keras.callbacks.History): Training history.
        """
        # Paths and filenames
        self.DATA_INPUT_PATH = "###YOUR_DATASET_INPUT_PATH###"
        self.MODEL_OUTPUT_PATH = "###YOUR_MODEL_OUTPUT_PATH###"
        self.MODEL_NAME = "###YOUR_MODEL_NAME###"
        # Update with your GloVe file path
        self.EMBEDDING_PATH = "path/to/glove.6B.100d.txt"

        # Model parameters
        self.lstm_units = 128
        self.dense_units = 256
        self.dropout_rate = 0.2

        # Sequence and embedding dimensions
        self.max_len = 128
        self.embedding_dim = 100

        # Training parameters
        self.batch_size = 32
        self.epochs = 3

    def load_data(self):
        """
        Load data from the specified input path.

        Returns:
            pd.DataFrame: Loaded dataset.
        """
        data = pd.read_csv(self.DATA_INPUT_PATH, encoding="latin1")
        data = data.fillna(method="ffill")
        return data

    def preprocess_data(self, data):
        """
        Preprocess data to create tokenized sentences and corresponding tags.

        Args:
            data (pd.DataFrame): Raw dataset.

        Returns:
            tuple: Tuple containing lists of tokenized sentences and corresponding tags.
        """
        agg_data = data.groupby(['Sentence #']).apply(
            lambda s: [(w, p, t) for w, p, t in zip(s["Word"].values.tolist(),
                                                    s['POS'].values.tolist(),
                                                    s["Tag"].values.tolist())]
        ).reset_index().rename(columns={0: 'Sentence_POS_Tag_Pair'})

        agg_data['Sentence'] = agg_data['Sentence_POS_Tag_Pair'].apply(
            lambda sentence: " ".join([s[0] for s in sentence]))
        agg_data['Tag'] = agg_data['Sentence_POS_Tag_Pair'].apply(
            lambda sentence: " ".join([s[2] for s in sentence]))

        agg_data['tokenised_sentences'] = agg_data['Sentence'].apply(
            lambda x: x.split())
        agg_data['tag_list'] = agg_data['Tag'].apply(lambda x: x.split())

        agg_data['len_sentence'] = agg_data['tokenised_sentences'].apply(
            lambda x: len(x))
        agg_data['len_tag'] = agg_data['tag_list'].apply(lambda x: len(x))
        agg_data['is_equal'] = agg_data.apply(
            lambda row: 1 if row['len_sentence'] == row['len_tag'] else 0, axis=1)

        agg_data = agg_data[agg_data['is_equal'] != 0]

        sentences_list = agg_data['Sentence'].tolist()
        tags_list = agg_data['tag_list'].tolist()

        return sentences_list, tags_list

    def create_tags_map(self, tags):
        """
        Create a mapping from tags to integer indices.

        Args:
            tags (list): List of unique tags.

        Returns:
            dict: Mapping from tags to integer indices.
        """
        tags_map = {tag: i for i, tag in enumerate(tags)}
        return tags_map

    def tokenize_data(self, sentences_list):
        """
        Tokenize sentences using the Keras Tokenizer.

        Args:
            sentences_list (list): List of tokenized sentences.

        Returns:
            tuple: Tuple containing the encoded sentences and the Keras Tokenizer.
        """
        tokeniser = tf.keras.preprocessing.text.Tokenizer(
            lower=False, filters='')
        tokeniser.fit_on_texts(sentences_list)
        return tokeniser.texts_to_sequences(sentences_list), tokeniser

    def prepare_padded_data(self, encoded_sentence, encoded_tags, tags_map):
        """
        Prepare padded input data for model training.

        Args:
            encoded_sentence (list): List of encoded sentences.
            encoded_tags (list): List of encoded tags.
            tags_map (dict): Mapping from tags to integer indices.

        Returns:
            tuple: Tuple containing padded encoded sentences and tags.
        """
        padded_encoded_sentences = tf.keras.preprocessing.sequence.pad_sequences(
            maxlen=self.max_len, sequences=encoded_sentence, padding="post", value=0)
        padded_encoded_tags = tf.keras.preprocessing.sequence.pad_sequences(
            maxlen=self.max_len, sequences=encoded_tags, padding="post", value=tags_map['O'])

        return padded_encoded_sentences, padded_encoded_tags

    def load_glove_embeddings(self):
        """
        Load pre-trained GloVe word embeddings.

        Returns:
            dict: Dictionary mapping words to their GloVe embeddings.
        """
        embeddings_index = {}
        with open(self.EMBEDDING_PATH, encoding="utf-8") as f:
            for line in f:
                values = line.split()
                word = values[0]
                coefs = np.asarray(values[1:], dtype="float32")
                embeddings_index[word] = coefs

        return embeddings_index

    def create_embedding_matrix(self, tokeniser, embeddings_index):
        """
        Create an embedding matrix using pre-trained GloVe embeddings.

        Args:
            tokeniser (tf.keras.preprocessing.text.Tokenizer): Keras Tokenizer.
            embeddings_index (dict): Dictionary mapping words to GloVe embeddings.

        Returns:
            np.ndarray: Embedding matrix.
        """
        vocab_size = len(tokeniser.word_index) + 1
        embedding_matrix = np.zeros((vocab_size, self.embedding_dim))

        for word, i in tokeniser.word_index.items():
            embedding_vector = embeddings_index.get(word)
            if embedding_vector is not None:
                embedding_matrix[i] = embedding_vector

        return embedding_matrix

    def build_model(self, input_dim, output_dim, embedding_matrix):
        """
        Build the NER model architecture.

        Args:
            input_dim (int): Vocabulary size.
            output_dim (int): Number of output classes (tags).
            embedding_matrix (np.ndarray): Embedding matrix.
        """
        input_word = tf.keras.layers.Input(shape=(self.max_len,))
        model = tf.keras.layers.Embedding(
            input_dim=input_dim,
            output_dim=self.embedding_dim,
#           input_length=MAX_LEN,
            embeddings_initializer=tf.keras.initializers.Constant(embedding_matrix),
            trainable=False  # Set to True if you want to fine-tune embeddings
        )(input_word)

        model = tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(
            units=self.lstm_units, return_sequences=True))(model)
        model = tf.keras.layers.Dropout(self.dropout_rate)(model)

        model = tf.keras.layers.TimeDistributed(
            tf.keras.layers.Dense(self.dense_units, activation='relu'))(model)
        model = tf.keras.layers.Dropout(self.dropout_rate)(model)

        out = tf.keras.layers.TimeDistributed(
            tf.keras.layers.Dense(output_dim, activation='softmax'))(model)

        model = tf.keras.Model(input_word, out)
        model.compile(optimizer="adam",
                      loss="categorical_crossentropy", metrics=["accuracy"])

        self.model = model

    def train_model(self, X_train, y_train, X_val, y_val):
        """
        Train the NER model.

        Args:
            X_train (np.ndarray): Padded encoded training sentences.
            y_train (list): One-hot encoded training tags.
            X_val (np.ndarray): Padded encoded validation sentences.
            y_val (list): One-hot encoded validation tags.
        """
        self.hist = self.model.fit(
            X_train,
            np.array(y_train),
            validation_data=(X_val, np.array(y_val)),
            batch_size=self.batch_size,
            epochs=self.epochs
        )

    def evaluate_model(self, X_test, y_test):
        """
        Evaluate the NER model on the test data.

        Args:
            X_test (np.ndarray): Padded encoded test sentences.
            y_test (list): One-hot encoded test tags.
        """
        test_loss, test_accuracy = self.model.evaluate(
            X_test, np.array(y_test))
        print(f"Test Loss: {test_loss}, Test Accuracy: {test_accuracy}")

    def report(self, X_test, y_test):
        """
        Generate and display a classification report for the NER model.

        Args:
            X_test (np.ndarray): Padded encoded test sentences.
            y_test (list): One-hot encoded test tags.
        """
        # Predictions
        predictions = self.model.predict(X_test)
        predicted_labels = np.argmax(predictions, axis=-1)

        # Flatten true and predicted labels
        true_labels_flat = np.argmax(y_test, axis=-1).flatten()
        predicted_labels_flat = predicted_labels.flatten()

        # Display classification report
        print(classification_report(true_labels_flat, predicted_labels_flat))

    def main_process(self):
        """
        Main process for loading, preprocessing, training, and evaluating the NER model.
        """
        # Load and preprocess data
        data = self.load_data()
        sentences_list, tags_list = self.preprocess_data(data)

        # Create tags_map
        tags = list(set(data['Tag'].values))
        tags_map = self.create_tags_map(tags)

        # Tokenize data
        encoded_sentence, tokeniser = self.tokenize_data(sentences_list)
        encoded_tags = [[tags_map[w] for w in tag] for tag in tags_list]

        # Prepare padded data
        padded_encoded_sentences, padded_encoded_tags = self.prepare_padded_data(
            encoded_sentence, encoded_tags, tags_map)

        # Create target
        num_tags = len(set(data['Tag'].values))
        target = [tf.keras.utils.to_categorical(
            i, num_classes=num_tags) for i in padded_encoded_tags]

        # Load GloVe embeddings
        embeddings_index = self.load_glove_embeddings()

        # Create embedding matrix
        embedding_matrix = self.create_embedding_matrix(
            tokeniser, embeddings_index)

        # Build and compile the enhanced model
        self.build_model(input_dim=len(tokeniser.word_index) + 1, output_dim=num_tags,
                         embedding_matrix=embedding_matrix)

        # Split the data
        X_train, X_val_test, y_train, y_val_test = train_test_split(
            padded_encoded_sentences, target, test_size=0.3, random_state=42)
        X_val, X_test, y_val, y_test = train_test_split(
            X_val_test, y_val_test, test_size=0.2, random_state=42)

        # Train the model
        self.train_model(X_train, y_train, X_val, y_val)

        # Evaluate the model
        self.evaluate_model(X_test, y_test)

        # Display classification report
        self.report(X_test, y_test)


# Instantiate and run the NERService
service = NERService()
service.main_process()
