import { WEBSITE_TITLE, BASE_IMAGE } from "../configs/constants-config";

function generate_title(pagetitle) {
  return `${pagetitle} | ${WEBSITE_TITLE}`;
}

function copyToClipboard(value) {
  const dummy = document.createElement("input");
  document.body.appendChild(dummy);
  dummy.setAttribute("id", "dummy_id");
  document.getElementById("dummy_id").value = value;
  dummy.select();
  document.execCommand("copy");
  document.body.removeChild(dummy);
}

function toImageUrl(path) {
  if (!path) return "";

  let output = BASE_IMAGE;
  if (path.startsWith("/")) {
    output += path;
  } else {
    output += `/${path}`;
  }
  return output;
}


export const commaSeparator = (num) => {
  return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

// A helper for converting non-persian numbers to persian ones.
let arabicNumbers = ["١", "٢", "٣", "٤", "٥", "٦", "٧", "٨", "٩", "٠"],
  persianNumbers = ["۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹", "۰"],
  englishNumbers = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"];

function searchAndReplaceInNumbers(value, source, target) {
  for (let i = 0, len = target.length; i < len; i++) {
    value = value.replace(new RegExp(source[i], "g"), target[i]);
  }
  return value;
}

function numbersFormatter(value, to = "fa") {
  value = typeof value === "number" ? String(value) : value;
  if (!value) return value;
  let output = value;
  if (to === "fa") {
    output = searchAndReplaceInNumbers(output, englishNumbers, persianNumbers);
    output = searchAndReplaceInNumbers(output, arabicNumbers, persianNumbers);
  } else if (to === "en") {
    output = searchAndReplaceInNumbers(output, persianNumbers, englishNumbers);
    output = searchAndReplaceInNumbers(output, arabicNumbers, englishNumbers);
  }
  return output;
}


function isFloat(n) {
  return Number(n) === n && Number(n) % 1 !== 0;
}

export const fixIt = (n) => {
  if (n || n === 0) {
    n = n.toString().replace(/,/g, '')
    n = numbersFormatter(n, 'en')
    n = Number(n)
    if (isFloat(n)) {
      if (Math.abs(n - Math.floor(n)) >= 0.99) {
        return Math.floor(n) + 1
      } else if (Math.abs(n - Math.floor(n)) <= 0.001) {
        return Math.floor(n)
      } else {
        return Math.trunc(Number(n))
      }
    } else {
      return Number(n)
    }
  } else {
    return ''
  }
}
function priceFormatter(val, to = 'fa') {
  return numbersFormatter(commaSeparator(fixIt(val)), to);
}

function queryStringMaker({ query }) {
  let queryString = ''
  Object.keys(query).forEach((v) => {
    if (query[v]) {
      if (queryString === '') {
        queryString += v + '=' + query[v].toString()
      } else {
        queryString += '&' + v + '=' + query[v].toString()
      }
    }
  })
  return queryString
}



export {
  generate_title,
  copyToClipboard,
  toImageUrl,
  numbersFormatter,
  priceFormatter,
  queryStringMaker
};
