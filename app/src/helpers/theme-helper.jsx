export { breakpoint, space, color, fz, height };

function breakpoint(name) {
  return function getBreakpoint(props) {
    return props.theme.breakpoints[name];
  };
}

function space(num) {
  return function getSpace(props) {
    return props.theme.space[num];
  };
}

function color(name) {
  return function getColor(props) {
    let paths = name.split(".");
    // eslint-disable-next-line
    let val = paths.reduce((prev, current) => {
      if (prev) {
        return prev[current];
      }
    }, props.theme.colors);
    return val;
  };
}

function fz(name) {
  return function getFontSize(props) {
    return props.theme.fontSizes[name];
  };
}

function height(name) {
  return function getHeight(props) {
    return props.theme.height[name];
  };
}
