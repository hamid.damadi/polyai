import Modal from "../Modal";
import LoadingModalStyle from "./LoadingModal-st";

function LoadingModal(props) {

  let { isOpen, toggleModal } = props;

  return (
    isOpen && (
      <Modal open={isOpen} handleToggle={toggleModal} isLoading={true}>
        <LoadingModalStyle>
          <div className="Rec1" />
          <div className="Rec2" />
          <div className="Rec3" />
          <div className="Rec4" />
          <div className="Rec5" />
          <div className="Rec6" />

          {/* <img src='./gifs/loading.gif'></img> */}
        </LoadingModalStyle>
      </Modal>
    )
  );
}

export default LoadingModal;
