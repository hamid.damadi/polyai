import styled from "styled-components";

let LoadingModalStyle = styled("div")`

width: 100px;
height: 100px;
display: flex;
align-items: center;
justify-content: center;
gap: 8px;

.Rec1{
  width: 8px;
  border-radius: 10px;
  animation: Rec1animation 3s infinite;
}
.Rec2{
  width: 8px;
  border-radius: 10px;
  animation: Rec2animation 3s infinite;
}
.Rec3{
  width: 8px;
  border-radius: 10px;
  animation: Rec3animation 3s infinite;

}
.Rec4{
  width: 8px;
  border-radius: 10px;
  animation: Rec4animation 3s infinite;

}
.Rec5{
  width: 8px;
  border-radius: 10px;
  animation: Rec5animation 3s infinite;

}
.Rec6{
  width: 8px;
  border-radius: 10px;
  animation: Rec6animation 3s infinite;
}

@keyframes Rec1animation {
  0%   {  height: 51px; width: 8px; background: #3B8681;}
  20%  {   height: 30px; width: 8px; background: #3B868182;}
  40%  {   height: 30px; width: 8px; background: #3B868182;}
  60%  {  height: 30px; width: 8px; background: #3B868182;}
  80% {   height: 30px; width: 8px; background: #3B868182;;}
  100% {   height: 30px; width: 8px; background: #3B868182;;}
}
@keyframes Rec2animation {
  0%   {  height: 30px; width: 8px; background: #3B868182;;}
  20%  {  height: 51px; width: 8px; background: #3B8681;}
  40%  {   height: 30px; width: 8px; background: #3B868182;}
  60%  {  height: 30px; width: 8px; background: #3B868182;}
  80% {   height: 30px; width: 8px; background: #3B868182;;}
  100% {   height: 30px; width: 8px; background: #3B868182;;}
}
@keyframes Rec3animation {
  0%   {  height: 30px; width: 8px; background: #3B868182;;}
  20%  {   height: 30px; width: 8px; background: #3B868182;}
  40%  {  height: 51px; width: 8px; background: #3B8681;}
  60%  {  height: 30px; width: 8px; background: #3B868182;}
  80% {   height: 30px; width: 8px; background: #3B868182;;}
  100% {   height: 30px; width: 8px; background: #3B868182;;}
}
@keyframes Rec4animation {
  0%   {  height: 30px; width: 8px; background: #3B868182;;}
  20%  {   height: 30px; width: 8px; background: #3B868182;}
  40%  {   height: 30px; width: 8px; background: #3B868182;}
  60%  {  height: 51px; width: 8px; background: #3B8681;}
  80% {   height: 30px; width: 8px; background: #3B868182;;}
  100% {   height: 30px; width: 8px; background: #3B868182;;}
}
@keyframes Rec5animation {
  0%   {  height: 30px; width: 8px; background: #3B868182;;}
  20%  {   height: 30px; width: 8px; background: #3B868182;}
  40%  {   height: 30px; width: 8px; background: #3B868182;}
  60%  {  height: 30px; width: 8px; background: #3B868182;}
  80%  { height: 51px; width: 8px; background: #3B8681;}
  100% {   height: 30px; width: 8px; background: #3B868182;;}
}
@keyframes Rec6animation {
  0%   {  height: 30px; width: 8px; background: #3B868182;;}
  20%  {   height: 30px; width: 8px; background: #3B868182;}
  40%  {   height: 30px; width: 8px; background: #3B868182;}
  60%  {  height: 30px; width: 8px; background: #3B868182;}
  80% {   height: 30px; width: 8px; background: #3B868182;;}
  100% {  height: 51px; width: 8px; background: #3B8681;}
}
  @media only screen and (max-width: 1000px) {
  }
`;
export default LoadingModalStyle;
