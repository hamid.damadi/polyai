
import { BaseModal } from ".";
import {
  ModalOverlay,
  ModalGlobalStyles,
  ModalContainer,
  ModalLoading,
} from "./Modal-styles";


function GeneralModal(props) {


  let {
    open,
    isMenu,
    children,
    width,
    isLoading
  } = props



  return (
    <BaseModal open={open}>
      <ModalGlobalStyles
        isMenu={isMenu}
        isLoading={isLoading}
      />
      <ModalOverlay
        isMenu={isMenu}
        isLoading={isLoading}
      >
        {

          isLoading ? (
            <ModalLoading>{children}</ModalLoading>
          ) : (
            <ModalContainer width={width}>{children}</ModalContainer>
          )

        }
      </ModalOverlay>
    </BaseModal >
  );
}
GeneralModal.defaultProps = {
  open: false,
};

export default GeneralModal;
