
import { DLTitleContainer } from "./Modal-styles";
import { ModalBody } from ".";

function DefaultLayoutModal(props) {
  return (
    <>
      <ModalBody>
        <DLTitleContainer>
          <div className="title">
            {props.onPrevious && (
              <button
                onClick={props.onPrevious}
                className="Button"
              >
                <img src="./svgs/arrow-right.svg" alt="back" width={16} />
              </button>
            )}

            {/* <Typography color="red" fontSize="xm" fontWeight="bold">
              {props.title}
            </Typography> */}
          </div>
          <div className="actions">
            <button onClick={props.onClose}>
              <img src="./svgs/x.svg" alt="close" width={16} />
            </button>
          </div>
        </DLTitleContainer>
        {props.children}
      </ModalBody>
      {!props.hasBtn && (
        <button
          disabled={props.btnDisabled}
          onClick={props.onAction}
        >
          {/* <Typography
            fontSize="xm"
            color="white"
            fontWeight="bold"
            textAlign="center"
          >
            {props.btnText}
          </Typography> */}
        </button>
      )}
    </>
  );
}

DefaultLayoutModal.defaultProps = {
  btnDisabled: false,
  btnText: "مرحله بعدی",
  btnColor: "primaryGradient",
};

export default DefaultLayoutModal;
