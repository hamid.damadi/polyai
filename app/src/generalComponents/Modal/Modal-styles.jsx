import styled from "styled-components";
import { createGlobalStyle } from "styled-components";
import { breakpoint } from "../../helpers/theme-helper";

let ModalOverlay = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: ${props => (props.isMenu || props.isLoading ? 'transparent' : 'rgba(0, 0, 0, 0.5)')};

  `;

let ModalGlobalStyles = createGlobalStyle`
  body {
    overflow-y: scroll !important;
    scroll-behavior: smooth;
    scrollbar-color: #B6B6B6 transparent;
    scrollbar-width: 4px;
  }
`;

let ModalContainer = styled.div`
  position: fixed;
  box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.06); 
  width: ${props => (props.width ? props.width : 95)}%;
  width: calc(100%-40px);
  top: 55%;
  left: 47%;
  transform: translate(-50%, -50%);
  max-height: 90vh;
  overflow-y: scroll;
  scroll-behavior: smooth;
  scrollbar-color: #B6B6B6 transparent;
  scrollbar-width: 4px;
  z-index: 2020;
  overflow-x: hidden;

  @media screen and (max-width: ${breakpoint("tablet_landskape")}) {
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
    max-height: 95vh;
    overflow-y: scroll;
  scroll-behavior: smooth;
  scrollbar-color: #B6B6B6 transparent;
  scrollbar-width: 4px;
    width: 60%;
  }

  @media screen and (max-width: ${breakpoint("tablet")}) {
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
    max-height: 95vh;
    overflow-y: scroll;
  scroll-behavior: smooth;
  scrollbar-color: #B6B6B6 transparent;
  scrollbar-width: 4px;
    width: 95%;
  }
`;

let ModalLoading = styled.div`
  position: fixed;
  width: calc(100%-40px);
  top: 55%;
  left: 47%;
  transform: translate(-50%, -50%);
  z-index: 2023;
`;



let ModalBody = styled.div`

  @media screen and (max-width: ${breakpoint("tablet")}) {
  }
`;

let ModalTitle = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

let DLTitleContainer = styled(ModalTitle)`

  .title {
    display: flex;
    align-items: center;

    & img {
      display: inline-block;
    }

    & .Button {
    }
  }

  .actions {
    display: flex;
    align-items: center;

    & > * {
      &:last-child {
        margin-left: 0;
      }
    }
  }
`;

export {
  ModalOverlay,
  ModalGlobalStyles,
  ModalContainer,
  ModalBody,
  ModalTitle,
  DLTitleContainer,
  ModalLoading
};

