export { default } from "./GeneralModal-component";
export { default as BaseModal } from "./BaseModal-component";
export { ModalBody, ModalTitle } from "./Modal-styles";

// layouts
export { default as DefaultLayout } from "./DefaultLayoutModal-component";
