import styled from "styled-components";

let EmptyContainer = styled.div`

    min-height: 100vh;
    display: flex;
    justify-content: flex-end;
    align-items: flex-start;
    background: #0D1F34;
`

export default EmptyContainer