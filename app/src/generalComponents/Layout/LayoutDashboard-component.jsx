
import Footer from '../Footer';
import Header from '../Header';
import LayoutDashboardStyles from './Dashboard-styles'

function LayoutDashboard(props) {

  let {
    children,
  } = props


  return (
    <LayoutDashboardStyles>
      <div className='content'>
        <Header />
        {children}
        <Footer />
      </div>
    </LayoutDashboardStyles>
  );
}

export default LayoutDashboard;
