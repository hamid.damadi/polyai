import styled from "styled-components";
import { breakpoint } from "../../helpers/theme-helper";

let LayoutDashboardStyles = styled.div`

  display: flex;
  flex-direction: column;
  height: 100vh;
  width: 100%;
  
  
  .content {
    width: 100%;
    background: transparent;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    padding-bottom: 150px !important;
    height: 100vh;
  }

  .tinder {
    position: absolute;
    z-index: 1000;
    bottom: 64px;
    right: 0px;
  }


  .page-divider {
    min-height: 80px;
    width: 100%;
  }
  
  @media screen and (max-width: ${breakpoint("compact_desktop")}) {
    /* min-height: 1000px; */
      margin-bottom: 40px;

      .content {
        margin: 0px 0px 50px 0px;
      }

  }

`;

export default LayoutDashboardStyles;