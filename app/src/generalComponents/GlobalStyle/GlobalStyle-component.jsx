import { createGlobalStyle } from "styled-components";
import { space, color } from "../../helpers/theme-helper";

export default createGlobalStyle`

  background: ${color('white.first')};

  html, body{
    padding: 0;
    margin: 0;
  }
  *, *:after, *:before{

    box-sizing: inherit;
  }
  body{
    background-color: ${color('white.first')};
    box-sizing: border-box;
    /* direction: rtl; */
    line-height: 1.6;
    /* background-color: #EFEFF7; */
  }

  button {
    cursor: pointer !important;
  }

  button:disabled {
    cursor: url('./pngs/disabled.png'), auto !important;
  }

  body::scroll-behavior {
    scrollbar-color: rebeccapurple green;
    scrollbar-width: thin;
  }

    /* width */
  ::-webkit-scrollbar {
    width: 4px;
  }

  /* Track */
  ::-webkit-scrollbar-track {
    background: transparent;
  }

  /* Handle */
  ::-webkit-scrollbar-thumb {
    background: #B6B6B6;
  }

  /* Handle on hover */
  ::-webkit-scrollbar-thumb:hover {
    background: #555;
  } 

  a{
    text-decoration: none;
    color: #9DDFEE ;
    &:hover{
      color: #666;
    }
  }

  .container{
    max-width: 100%;
    margin: auto;
    /* min-height: 100vh; */
  }

  .errorNotif {
    display: flex;
    align-items: center;
    width: 340px;
    height: 50px;
    box-sizing: border-box;
    background: #FFF5F6;
    border: 1px solid rgba(218, 100, 107, 0.9);
    box-shadow: 0px 2px 4px 1px rgba(162, 162, 162, 0.18);

    font-style: normal;
    font-weight: 400;
    font-size: 12px;
    line-height: 19px;
    text-align: right;
    color: rgba(174, 6, 6, 0.9);
  }

  .errorNotif img {
    margin-left: 30px;
    margin-right: 30px;
  }

  .successNotif {
    display: flex;
    align-items: center;
    width: 630px;
    height: 50px;
    background: #EEFAF7;
    border: 1px solid #0AB28A;
    box-shadow: 0px 2px 4px 1px rgba(162, 162, 162, 0.18);
    font-style: normal;
    font-weight: 400;
    font-size: 12px;
    line-height: 19px;
    text-align: right;
    color: #01654D;
  }

  .successNotif img{
    margin-left: 30px;
    margin-right: 30px;
  }


  @media screen and (max-width: "1100px"){
    .trackingLinks {
      margin-right: 26px; 
      margin-top: 15px;
      margin-bottom: 10px;
    }
  }

  @media screen and (max-width: ${({ theme }) => theme.containerWidth}px){

    .container{

      padding-right: ${space(4)}px;
      padding-left: ${space(4)}px;
    }
  }

`;
