import { useState, useCallback, useEffect } from "react";
import { useLocation } from "react-router-dom";

function ProtectedRoute(props) {
  const location = useLocation();

  const [, updateState] = useState();
  const forceUpdate = useCallback(() => updateState({}), []);

  useEffect(() => {
    window.addEventListener("resize", forceUpdate, { passive: true });
    // eslint-disable-next-line
  }, [])

  useEffect(() => {
    window.scrollTo(0, 0)
    // eslint-disable-next-line
  }, [location]);


  return <props.render {...props} />;
}

export default ProtectedRoute;
