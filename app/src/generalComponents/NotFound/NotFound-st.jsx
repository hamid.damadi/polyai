import styled from "styled-components";
import { breakpoint } from "../../helpers/theme-helper";

let HomeStyle = styled.div`

  width: 100%;
  height: auto;
  padding: 0 64px;
  display: flex;
  align-items: flex-start;
  justify-content: flex-end;

@media screen and (max-width: ${breakpoint("tablet")}){
  padding: 64px 16px 0 16px;
}

`;
export default HomeStyle
