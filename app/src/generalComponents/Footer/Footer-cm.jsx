import { Link } from 'react-router-dom';
import {
  LINK_HOME_PAGE,
  LINK_BOOKS_INTRODUCTION_PAGE,
  LINK_SERVICES_INFO_PAGE
} from '../../configs/constants-config';
import FooterStyle from './Footer-st'

function Footer() {

  return (
    <FooterStyle>
      <div className='column1'>
        Hamid Damadi
      </div>
      <div className='column2'>
        <a
          className='links'
          href='https://hamiddamadi.ir'
          target='_blank'
          rel="noreferrer"
        >
          Personal Website
        </a>
        <a
          className='links'
          href='https://hamiddamadi.ir/CV.pdf'
          target="_blank"
          rel="noreferrer"
        >
          CV
        </a>
        <a
          className='links'
          href="https://gitlab.com/hamid.damadi"
          target="_blank"
          rel="noreferrer"
        >
          GitLab
        </a>
        <a
          className='links'
          href="https://www.kaggle.com/hamiddamadi"
          target="_blank"
          rel="noreferrer"
        >
          Kaggle
        </a>
        <a
          className='links'
          href="https://linkedin.com/in/hamid-damadi-bb622354"
          target="_blank"
          rel="noreferrer"
        >
          LinkedIn
        </a>
      </div>
      <div className='column3'>
        <Link
          className='links'
          to={LINK_HOME_PAGE}
        >
          Home
        </Link>
        <Link
          className='links'
          to={LINK_BOOKS_INTRODUCTION_PAGE}
        >
          Books
        </Link>
      </div>
      <div className='column4'>
        <Link
          className='links'
          to={`${LINK_SERVICES_INFO_PAGE}?service=Image-Recognition`}
          state={{ doable: true }}
        >
          Image Recognition
        </Link>
        <Link
          className='links'
          to={`${LINK_SERVICES_INFO_PAGE}?service=Named-Entity-Recognition`}
          state={{ doable: true }}
        >
          Named Entity Recognition
        </Link>
        <Link
          className='links'
          to={`${LINK_SERVICES_INFO_PAGE}?service=Conditional-Generative-Adversarial-Network`}
          state={{ doable: true }}
        >
          Conditional GAN
        </Link>
        <Link
          className='links'
          to={`${LINK_SERVICES_INFO_PAGE}?service=Sentiment-Analysis`}
          state={{ doable: true }}
        >
          Sentiment Analysis
        </Link>
        <Link
          className='links'
          to={`${LINK_SERVICES_INFO_PAGE}?service=Emotion-Recognition`}
          state={{ doable: true }}
        >
          Emotion Recognition
        </Link>
        <Link
          className='links'
          to={`${LINK_SERVICES_INFO_PAGE}?service=Handwritten-Text-Recognition`}
          state={{ doable: true }}
        >
          Handwritten Text Recognition
        </Link>
        <Link
          className='links'
          to={`${LINK_SERVICES_INFO_PAGE}?service=Image-Captioning`}
          state={{ doable: true }}
        >
          Image Captioning
        </Link>
        <Link
          className='links'
          to={`${LINK_SERVICES_INFO_PAGE}?service=Image-Object-Detection`}
          state={{ doable: true }}
        >
          Image Object Detection
        </Link>
      </div>
    </FooterStyle >
  );
}

export default Footer;
