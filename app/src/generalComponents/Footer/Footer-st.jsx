import styled from "styled-components";
import { breakpoint } from "../../helpers/theme-helper";

let FooterStyle = styled.div`


width: 100%;
background: #0F1014; 
padding: 64px;
display: flex;
flex-wrap: wrap;
column-gap: 256px;
margin-top: 100px;


.column1 {
    color: #9DDFEE;
}

.column2 {
    display: flex;
    flex-direction: column;
    row-gap: 24px;
}

.column3 {
    display: flex;
    flex-direction: column;
    row-gap: 24px;
}

.column4 {
    display: flex;
    flex-direction: column;
    row-gap: 24px;
}

.links {
    color: var(--White, #FFF) !important;
    font-size: 14px;
    font-style: normal;
    font-weight: 400;
    line-height: normal; 
}



@media screen and (max-width: ${breakpoint("compact_desktop")}) {
    flex-direction: column;
    /* justify-content: center;
    align-items: center; */
    row-gap: 24px;

    /* .column2 {
        row-gap: 0;
    }

    .column3 {
        row-gap: 0;
    }

    .column4 {
        row-gap: 0;
    } */

}  
   

`;
export default FooterStyle
