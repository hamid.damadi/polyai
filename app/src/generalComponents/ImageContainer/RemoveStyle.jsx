import styled from "styled-components";
import { breakpoint } from "../../helpers/theme-helper";

let RemoveStyle = styled.div`

.removeBtn-image{
  width: 24px !important;
  height: 24px !important;
  background-image:  url(${props => props.disp ? "trash.png" : ""});
  background-repeat: no-repeat !important;
  background-position: center !important;
  border: none !important;
  background-color:  ${props => props.disp ? '#EEC7CF' : 'transparent'} ;
  border-radius: 50% !important;
  display: ${props => props.disp};
  transition: .3s;
}


@media screen and (max-width: ${breakpoint("tablet")}){

  }


`;
export default RemoveStyle
