import styled from "styled-components";
import { breakpoint } from "../../helpers/theme-helper";
import { FADE_IN_DURATION } from "../../configs/constants-config";

let ImageContainerStyle = styled.div`

display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
width: 100%;
max-width: 646px;
margin-top: 60px;

.title {
  font-style: normal;
  font-weight: 500;
  font-size: 13px;
  line-height: 25px;
  text-align: left;
  color: #56628C;
  margin-bottom: 4px;
}

.mainDiv {
  max-width: 720px;
  width: 100%;
  background: #9DDFEE;
  border-radius: 8px;
  display: flex;
}

.rightContainer {
  width: 100%;
  flex: 95%;
  padding: 58px 32px 58px 32px;
  max-height: 640px;
  overflow-y: scroll;
  scroll-behavior: smooth;
  scrollbar-color: #B6B6B6 transparent;
  scrollbar-width: 4px;
}

.upload {
  max-width: 224px;
  width: 100%;
  height: 40px;
  background: #0F1014;
  border-radius: 64px;
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 22px;
  text-align: center;
  color: #FFFFFF;
  margin-bottom: 32px;
  border: none;
  outline: none;
}


.filesContainer{
  width: 100%;
  height: auto;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  row-gap: 32px;

  animation-name: 'fadein';
  animation-duration: ${FADE_IN_DURATION};
  animation-fill-mode: linear;

  @keyframes fadein {
      from { opacity: 0; }
      to   { opacity: 1; }
  }
}

.filesRow {
  display: flex;
  justify-content: space-between;
  width: 100%;
  /* flex-wrap: wrap; */
  /* column-gap: 16px; */
}

.fileContainer{
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin-left: 8px;
}

.theFile{
  width: 256px;
  /* width: 100%; */
  height: 256px;
  background: #FFFFFF;
  border: 1px solid rgba(56, 58, 96, 0.21);
  border-radius: 10px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin-top: -20px;
}

.theFile img{
  max-width: 248px;
  max-height: 248px;
  object-fit: contain;
}

.filesName {
  display: flex;
  align-items: center;
  justify-content: space-between;
  max-width: 224px;
  width: 100%;
  height: 64px;
  background: #FFFFFF;
  border: 1px solid rgba(56, 58, 96, 0.21);
  border-radius: 10px;
  padding-right: 24px;
  padding-left: 24px;
}

.filesName span {
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 22px;
  text-align: left;
  color: #56628C;

  display:inline-block;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  max-width: 26ch;
}

.filesName button {
  width: 16px;
  background: transparent;
  border: none;
  cursor: pointer;
}

.removeBtnContainer {
  width: 100%;
  display: flex;
  justify-content: flex-end;
  z-index: 5 !important;
  margin-top: -9px !important;
}

.removeBtn{
  width: 24px !important;
  height: 24px !important;
  background: transparent;
  border: none !important;
}

@media screen and (max-width: ${breakpoint('compact_desktop')}){
  .files-dropzone {
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }

  .filesRow {
    flex-direction: column;
    justify-content: center;
    align-items: center;
    row-gap: 24px;
  }

  .filesName {
    display: none;
  }

}


`;
export default ImageContainerStyle
