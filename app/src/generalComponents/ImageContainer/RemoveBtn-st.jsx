import styled from "styled-components";

let RemoveBtn = styled.div`

.removeBtn-image{
  width: 24px !important;
  height: 24px !important;
  background-image:  url(${props => props.disp ? "trash.png" : ""});
  background-repeat: no-repeat !important;
  background-position: center !important;
  border: none !important;
  background-color:  ${props => props.disp ? '#EEC7CF' : 'transparent'} ;
  border-radius: 50% !important;
  display: ${props => props.disp};
  transition: .3s;
}

`;
export default RemoveBtn
