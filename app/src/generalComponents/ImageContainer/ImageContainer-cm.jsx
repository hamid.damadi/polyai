import { useState, Children } from 'react'
import PropTypes from 'prop-types';
import Files from 'react-files'
import { useToasts } from 'react-toast-notifications'
import ImageContainerStyle from './ImageContainer-st'
import RemoveStyle from './RemoveStyle'

function ImageContainer(props) {
  let { addToast } = useToasts()

  let {
    files,
    setFiles,
    requestBody,
    setRequestBody,
    validTypes
  } = props

  const onFilesError = (error,) => {
    addToast(error.message, { appearance: "error" });
  }

  let onFilesChange = (files) => {
    // files = files.concat(newFiles)
    setFiles(files)
    let imageForm = new FormData()
    let fullName = ''
    files.map((v) => {
      const extension = v.extension
      const name = Math.floor(Math.random() * 100000000)
      fullName = name.toString() + '.' + extension
      const file = new Blob([v])
      imageForm.append('images[]', file, fullName)
      return {
        fileName: fullName
      }
    })
    requestBody['files'] = files
    requestBody['imageForm'] = imageForm
    requestBody['path'] = fullName
    setRequestBody({ ...requestBody })
  }

  let filesRemoveFile = (files) => {
    // files = files.filter((prevFile) => prevFile.id !== file.id)
    setFiles([])
    // let imageForm = new FormData()
    // files.map((v) => {
    //   const file = new Blob([v])
    //   imageForm.append('images[]', file, v.name)
    //   return {
    //     fileName: v.name
    //   }
    // })

    requestBody['files'] = files
    requestBody['imageForm'] = undefined
    requestBody['path'] = ''
    setRequestBody({ ...requestBody })
  }

  const extensionMap = new Map()
  extensionMap.set('pdf', './pngs/PDF.png')
  extensionMap.set('docx', './pngs/WORD.png')
  extensionMap.set('xlsx', './pngs/EXC.png')


  let [disp, setDisp] = useState()
  let [hoveredIndex, setHoveredIndex] = useState()
  let [mapType, setMapType] = useState()

  return (
    <ImageContainerStyle
      disp={disp}
    >
      <div className='mainDiv'>
        <div className='rightContainer'>
          <Files
            className='files-dropzone'
            onChange={onFilesChange}
            onError={onFilesError}
            accepts={validTypes}
            multiple={false}
            // maxFiles={3}
            maxFileSize={10000000}
            minFileSize={0}
            clickable
            maxFiles={8}
          >

            <button
              className='upload'
            >
              Upload
            </button>
          </Files>
          <div className='filesContainer'>
            {
              files && files.length > 0 &&
              Children.toArray(
                files.map((e, index) => (
                  <div className='filesRow'
                    onPointerEnter={() => {
                      setDisp(true)
                      setHoveredIndex(index)
                      setMapType('files')
                    }}
                    onPointerLeave={() => {
                      setDisp(false)
                      setHoveredIndex()
                      setMapType()
                    }}
                  >
                    <div className='filesName'>
                      <span>
                        {e.name}
                      </span>
                    </div>
                    <div className='fileContainer'>
                      <div className="removeBtnContainer" >
                        <RemoveStyle
                          disp={hoveredIndex === index && mapType === 'files' && disp}
                        >
                          <button
                            className='removeBtn-image'
                            onClick={() => filesRemoveFile(e)} // eslint-disable-line
                          />
                        </RemoveStyle>
                      </div>
                      <div className='theFile'>
                        <img
                          alt=''
                          src={['jpg', 'png', 'jpeg', 'webp'].includes(e.extension) && e.preview ? e.preview.url : ['pdf', 'docx', 'xlsx'].includes(e.extension) ? extensionMap.get(e.extension) : ''}
                        />
                      </div>
                    </div>
                  </div>
                ))
              )
            }
          </div>
        </div>
      </div>
    </ImageContainerStyle >
  );
}

ImageContainer.propTypes = {
  files: PropTypes.array,
  setFiles: PropTypes.func.isRequired,
  requestBody: PropTypes.object.isRequired,
  setRequestBody: PropTypes.func.isRequired,
  validTypes: PropTypes.arrayOf(PropTypes.string).isRequired
};

export default ImageContainer;

