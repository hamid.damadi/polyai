const MyCustomToast = ({ appearance, children }) => (
  <div className={appearance === 'error' ? 'errorNotif' : 'successNotif'}>
    <img alt='' src={appearance === 'error' ? "./pngs/errorNotif.png" : './pngs/successNotif.png'} />
    {children}
  </div>
);

export default MyCustomToast;