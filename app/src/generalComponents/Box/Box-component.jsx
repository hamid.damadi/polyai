import styled from "styled-components";
import {
  space,
  flexbox,
  layout,
  color,
  borderRadius,
  position,
  border,
  background,
} from "styled-system";

const Box = styled.div(
  {},
  space,
  flexbox,
  layout,
  color,
  borderRadius,
  position,
  border,
  background
);

export default Box;
