import styled from "styled-components";
import { breakpoint } from "../../helpers/theme-helper";

let HeaderStyle = styled.div`


width: 100%;
display: flex;
justify-content: space-between;
align-items: center;
padding: 24px 64px;
background: #0F1014;

.items {
  display: flex;
  column-gap: 24px;
}

.item {
  color: var(--White, #FFF); 
}

.logo {
  color: #9DDFEE;
}



@media screen and (max-width: ${breakpoint("tablet_landskape")}) {
  padding: 24px;

}  

`;
export default HeaderStyle
