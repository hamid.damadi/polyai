import { Link } from 'react-router-dom';
import HeaderStyle from './Header-st'
import { LINK_BOOKS_INTRODUCTION_PAGE, LINK_HOME_PAGE } from '../../configs/constants-config';

function Header() {

  return (
    <HeaderStyle>
      <div className='logo'>
        <a
          href='https://hamiddamadi.ir'
          target='_blank'
          rel="noreferrer"
        >
          Hamid Damadi
        </a>
      </div>
      <div className='items'>
        <div className='item'>
          <Link
            className='item'
            to={LINK_BOOKS_INTRODUCTION_PAGE}
          >
            Books
          </Link>
        </div>
        <a
          className='item'
          href="https://gitlab.com/hamid.damadi/polyai"
          target='_blank'
          rel="noreferrer"
        >
          Source Code
        </a>
        <div className='item'>
          <Link
            className='item'
            to={LINK_HOME_PAGE}
          >
            Home
          </Link>
        </div>
      </div>

    </HeaderStyle>
  );
}

export default Header;
