import { Suspense, lazy } from "react"
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";

import { ThemeProvider } from "styled-components"
import { ToastProvider } from "react-toast-notifications"

import "./resources/styles/normalize.css"

import themeConfig from "./configs/theme-config"
import ProtectedRoute from "./generalComponents/ProtectedRoute"
import GlobalStyle from "./generalComponents/GlobalStyle"
import withRedux from "./generalComponents/withRedux"

import {
  LINK_NOT_FOUND,
  LINK_HOME_PAGE,
  LINK_SERVICE_IMAGE_RECOGNITION_PAGE,
  LINK_SERVICE_NER_PAGE,
  LINK_SERVICE_CGAN_PAGE,
  LINK_SERVICE_SENT_PAGE,
  LINK_SERVICE_EMOTION_RECOGNITION_PAGE,
  LINK_SERVICE_TEXT_RECOGNITION_PAGE,
  LINK_BOOKS_INTRODUCTION_PAGE,
  LINK_SERVICE_IMAGE_CAPTION_PAGE,
  LINK_SERVICES_INFO_PAGE,
  LINK_SERVICE_IMAGE_OBJECT_DETECTION_PAGE
} from './configs/constants-config'

import LoadingModal from "./generalComponents/LoadingModal/LoadingModal-cm";
import MyCustomToast from './generalComponents/MyCustomToast'
import ReactGA from "react-ga4";

ReactGA.initialize("G-34TKPWVPF8");

const HomePage = lazy(() => import("./sections/Home/pages/HomePage"))
const ServicesInfoPage = lazy(() => import("./sections/Services/pages/ServicesInfoPage"))
const ImageRecognitionPage = lazy(() => import("./sections/Services/pages/ImageRecognitionPage"))
const NERPage = lazy(() => import("./sections/Services/pages/NERPage"))
const CGanPage = lazy(() => import("./sections/Services/pages/CGanPage"))
const SentimentPage = lazy(() => import("./sections/Services/pages/SentimentPage"))
const EmotionRecognitionPage = lazy(() => import("./sections/Services/pages/EmotionRecognitionPage"))
const TextRecognitionPage = lazy(() => import("./sections/Services/pages/TextRecognitionPage"))
const ImageCaptionPage = lazy(() => import("./sections/Services/pages/ImageCaptionPage"))
const ImageObjectDetectionPage = lazy(() => import("./sections/Services/pages/ImageObjectDetectionPage"))
const IntroductionBooksPage = lazy(() => import("./sections/Books/pages/IntroductionBooksPage"))


function App() {

  return (
    <BrowserRouter>
      <ThemeProvider theme={themeConfig}>
        <GlobalStyle />
        <ToastProvider
          components={{ Toast: MyCustomToast }}
          placement='bottom-center'
          autoDismiss
        >
          <Suspense fallback={<LoadingModal isOpen={true} />}>
            <Routes>
              <Route
                path="*"
                element={<Navigate to={LINK_NOT_FOUND} replace />}
              />
              <Route path='/app' element={<Navigate to={LINK_HOME_PAGE} replace />} />
              <Route path={LINK_HOME_PAGE} element={<ProtectedRoute render={HomePage} path={LINK_HOME_PAGE} />} />
              <Route path={LINK_SERVICES_INFO_PAGE} element={<ProtectedRoute render={ServicesInfoPage} path={LINK_SERVICE_IMAGE_RECOGNITION_PAGE} />} />
              <Route path={LINK_SERVICE_IMAGE_RECOGNITION_PAGE} element={<ProtectedRoute render={ImageRecognitionPage} path={LINK_SERVICE_IMAGE_RECOGNITION_PAGE} />} />
              <Route path={LINK_SERVICE_NER_PAGE} element={<ProtectedRoute render={NERPage} path={LINK_SERVICE_NER_PAGE} />} />
              <Route path={LINK_SERVICE_CGAN_PAGE} element={<ProtectedRoute render={CGanPage} path={LINK_SERVICE_CGAN_PAGE} />} />
              <Route path={LINK_SERVICE_SENT_PAGE} element={<ProtectedRoute render={SentimentPage} path={LINK_SERVICE_SENT_PAGE} />} />
              <Route path={LINK_SERVICE_EMOTION_RECOGNITION_PAGE} element={<ProtectedRoute render={EmotionRecognitionPage} path={LINK_SERVICE_EMOTION_RECOGNITION_PAGE} />} />
              <Route path={LINK_SERVICE_TEXT_RECOGNITION_PAGE} element={<ProtectedRoute render={TextRecognitionPage} path={LINK_SERVICE_TEXT_RECOGNITION_PAGE} />} />
              <Route path={LINK_SERVICE_IMAGE_CAPTION_PAGE} element={<ProtectedRoute render={ImageCaptionPage} path={LINK_SERVICE_IMAGE_CAPTION_PAGE} />} />
              <Route path={LINK_SERVICE_IMAGE_OBJECT_DETECTION_PAGE} element={<ProtectedRoute render={ImageObjectDetectionPage} path={LINK_SERVICE_IMAGE_OBJECT_DETECTION_PAGE} />} />
              <Route path={LINK_BOOKS_INTRODUCTION_PAGE} element={<ProtectedRoute render={IntroductionBooksPage} path={LINK_BOOKS_INTRODUCTION_PAGE} />} />
            </Routes>
          </Suspense>
        </ToastProvider>
      </ThemeProvider>
    </BrowserRouter>
  );
}

export default withRedux(App);
