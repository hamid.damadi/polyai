import ServiceCardStyle from './ServiceCard-st'
import { Link } from 'react-router-dom';


function ServiceCard(props) {
  const { icon, title, description, path } = props

  return (
    <ServiceCardStyle
      color={props.color}
    >
      <Link
        to={path ? path : undefined}
      >
        <div className='icon-div'>
          <img
            src={icon}
            alt=''
            decoding="async"
            loading="lazy"
          ></img>
        </div>
        <div className='card-title'>
          {title}
        </div>
        <div className='card-description'>
          {description}
        </div>
      </Link>

    </ServiceCardStyle >

  );
}

export default ServiceCard;
