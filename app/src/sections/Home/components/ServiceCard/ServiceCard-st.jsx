import styled from "styled-components";
import { breakpoint } from "../../../../helpers/theme-helper";

let ServiceCardStyle = styled.div`

  width: 100%;
  height: auto;
  background-color: ${(props) => props.color};
  max-width: 646px;
  min-height: 302px;
  display: flex;
  flex-direction: column;
  padding: 24px;
  border-radius: 16px; 

  img {
    width: 48px;
    height: 48px;
  }


  .icon-div {
    display: flex;
    width: 78px;
    height: 78px;
    padding: 22px 22px 22px 23px;
    justify-content: center;
    align-items: center;
    margin-bottom: 24px;

    border-radius: 8px;
    border: 1px solid var(--Primary, #0F1014);
    background: transparent;
    box-shadow: 0px 4px 12px 0px rgba(0, 0, 0, 0.12);
  }

  .card-title {
    width: 100%;
    margin-bottom: 8px;
    color: var(--Primary, #0F1014);
    text-align: left;
    font-size: 24px;
    font-style: normal;
    font-weight: 700;
    line-height: 150%; /* 36px */
  }

  .card-description {
    color: var(--Primary, #0F1014);
    font-size: 18px;
    font-style: normal;
    font-weight: 400;
    line-height: 150%; /* 27px */
    text-align: justify;
    text-justify: inter-word;
  }


@media screen and (max-width: ${breakpoint('compact_desktop')}){
}

`;
export default ServiceCardStyle
