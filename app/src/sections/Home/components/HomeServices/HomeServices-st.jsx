import styled from "styled-components";
import { breakpoint } from "../../../../helpers/theme-helper";

let HomeServicesStyle = styled.div`

  width: 100%;
  height: auto;
  background-color: #FFF;
  padding: 64px 64px 146px 64px;


  .titles {
    display: flex;
    flex-direction: column;
    max-width: 646px;
  }

  h2 {
    width: 100%;
    color: #0F1014;
    text-align: left;
    /* H2 */
    font-family: Red Hat Display;
    font-size: 48px;
    font-style: normal;
    font-weight: 700;
    line-height: normal;
    text-transform: uppercase; 
  }

  h3 {
    color: #0F1014;
    font-size: 24px;
    font-style: normal;
    font-weight: 400;
    line-height: 150%; 
    margin-bottom: 100px;
  }

  .cards-section {
    width: 100%;
    display: flex;
    flex-wrap: wrap;
    column-gap: 20px;
    row-gap: 80px;
  }

@media screen and (max-width: ${breakpoint('compact_desktop')}){
  padding: 64px 24px 146px 24px;
}

`;
export default HomeServicesStyle
