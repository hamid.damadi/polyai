import { Children } from 'react';
import ServiceCard from '../ServiceCard';
import HomeServicesStyle from './HomeServices-st'
import {
  LINK_SERVICES_INFO_PAGE
} from '../../../../configs/constants-config';


function HomeServices() {

  const infoArr = [
    {
      color: "#D2BFE6",
      icon: "./icons/image-detection.png",
      title: "Image Recognition",
      path: `${LINK_SERVICES_INFO_PAGE}?service=Image-Recognition`,
      description: "In this section, we are faced with a relatively simple image recognition. Using the transfer learning technique by the MobileNetV2 trained model and training on the MNIST dataset using the fine-tuning method, we have reached 98.9% accuracy."
    },
    {
      color: "#F4B9CE",
      icon: "./icons/ner.png",
      title: "Named Entity Recognition",
      path: `${LINK_SERVICES_INFO_PAGE}?service=Named-Entity-Recognition`,
      description: "In this section, we are faced with a relatively simple named entity recognition. Using the trained embedding technique by the GloVe and training on the NER dataset using the recursive neural network, we have reached 99.3% accuracy."
    },
    {
      color: "#F9F89E",
      icon: "./icons/cGan.png",
      title: "Conditional Generative Adversarial Network",
      path: `${LINK_SERVICES_INFO_PAGE}?service=Conditional-Generative-Adversarial-Network`,
      description: "In this section, we are faced with a relatively simple conditional GAN. Using the special training methods on the MNIST dataset, it is possible to generate handwritten digits similar to MNIST samples."
    },
    {
      color: "#CDF8A5",
      icon: "./icons/sent.png",
      title: "Sentiment Analysis",
      path: `${LINK_SERVICES_INFO_PAGE}?service=Sentiment-Analysis`,
      description: "        In this section, we are faced with a relatively simple Sentiment Analysis. Using the trained embedding technique by the GloVe and training on the aclImdb dataset using the recursive neural network, we have reached almost 90% accuracy."
    },
    {
      color: "#54A0FF",
      icon: "./icons/emotion-recognition.png",
      title: "Emotion Recognition",
      path: `${LINK_SERVICES_INFO_PAGE}?service=Emotion-Recognition`,
      description: "In this section, we are faced with a relatively simple emotion recognition. Using CNN on the FER2013 dataset, it is possible to recognize people's facial emotions. By removing 2 classes, we have reached almost 73% accuracy."
    },
    {
      color: "#EFD9D5",
      icon: "./icons/htr.png",
      title: "HandWritten Text Recognition",
      path: `${LINK_SERVICES_INFO_PAGE}?service=Handwritten-Text-Recognition`,
      description: "In this section, we are faced with a relatively simple Handwritten text recognition. By combining CNN and RNN and using the endpoint layer for implementing CTC loss, the model trained model on the IAM dataset."
    },
    {
      color: "#ffd476",
      icon: "./icons/image-caption.png",
      title: "Image Captioning",
      path: `${LINK_SERVICES_INFO_PAGE}?service=Image-Captioning`,
      description: "In this section, we are faced with a relatively simple image captioning. By combining CNN and Transformer, the model trained on the COCO-2017 dataset."
    },
    {
      color: "#CACACA",
      icon: "./icons/image-object-detection.png",
      title: "Image Object Detection",
      path: `${LINK_SERVICES_INFO_PAGE}?service=Image-Object-Detection`,
      description: "In this section, we are faced with relatively simple image object detection. By leveraging the Keras_CV library, the model was trained on the COCO-2017 dataset."
    },
    {
      color: "gray",
      icon: "./icons/coming-soon.png",
      title: "Coming Soon ...",
      description: ""
    }
  ]

  return (
    <HomeServicesStyle>
      <div className='titles'>
        <h2>
          Services
        </h2>
        <h3>
          The current services! Other amazing services will be added very soon.
        </h3>
      </div>
      <div className='cards-section'>
        {
          Children.toArray(
            infoArr.map((e) => (
              <ServiceCard
                color={e.color}
                icon={e.icon}
                title={e.title}
                description={e.description}
                path={e.path}
              />
            ))
          )
        }
      </div>
    </HomeServicesStyle >

  );
}

export default HomeServices;
