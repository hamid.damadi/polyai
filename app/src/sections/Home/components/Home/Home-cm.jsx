import HomeStyle from './Home-st'
import HomeHeader from '../HomeHeader'
import HomeServices from '../HomeServices'
import HomeFeatures from '../HomeFeatures';

function Home() {

  return (
    <HomeStyle>
      <HomeHeader />
      <HomeFeatures />
      <HomeServices />
    </HomeStyle >

  );
}

export default Home;
