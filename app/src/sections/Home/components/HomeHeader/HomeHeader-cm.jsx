import HomeHeaderStyle from './HomeHeader-st'


function HomeHeader() {

  return (
    <HomeHeaderStyle>
      <div className='header'>
        <div className='header-imageDiv'>
          <picture>
            <source srcSet="./webp/Hero.webp" type="image/webp" />
            <source srcSet="./avif/Hero.avif" type="image/avif" />
            <img
              alt=''
              decoding="async"
              loading="lazy"
              width='638'
              height='906'
            >
            </img>
          </picture>
        </div>
        <div className='titles'>
          <h1>
            Fun With AI!
          </h1>
          <h2>
            A simple microservices project that help you to see some AI applications in a simple form!
          </h2>
          <div className='header-titles-buttons'>
            <a
              className='header-titles-buttons-1'
              href="https://gitlab.com/hamid.damadi/polyai"
              target='_blank'
              rel="noreferrer"
            >
              Source Code
            </a>
            <a
              className='header-titles-buttons-2'
              href="https://www.kaggle.com/hamiddamadi"
              target='_blank'
              rel="noreferrer"
            >
              Kaggle
            </a>
          </div>
        </div>
      </div>
    </HomeHeaderStyle >

  );
}

export default HomeHeader;
