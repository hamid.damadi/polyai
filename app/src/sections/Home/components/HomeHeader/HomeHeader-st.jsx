import styled from "styled-components";
import { breakpoint } from "../../../../helpers/theme-helper";

let HomeHeaderStyle = styled.div`

  width: 100%;
  height: auto;
  /* padding: 0 64px; */

  .header {
    width: 100%;
    background: #0F1014;
    padding: 48px 64px 0 64px;

    display: flex;
  }

  .titles {
    display: flex;
    flex-direction: column;
    max-width: 646px;
    margin-top: 160px;
    margin-left: 80px;
  }

  h1 {
    color: var(--Secondary, #9DDFEE);
    /* H1 */
    font-size: 64px;
    font-style: normal;
    font-weight: 700;
    line-height: normal;
    text-transform: uppercase; 
    margin-bottom: 24px;
  }

  h2 {
    color: var(--White, #FFF);
    font-family: Red Hat Display;
    font-size: 24px;
    font-style: normal;
    font-weight: 400;
    line-height: 150%; 
  }

  .header-imageDiv {
    width: 816px;
    min-height: 906px;
  }

  .header-titles-buttons {
    width: 100%;
    display: flex;
    column-gap: 40px;
    margin-top: 56px;
  }

  .header-titles-buttons-1 {
    background: var(--Secondary, #9DDFEE);
    box-shadow: 0px 12px 16px 0px rgba(0, 0, 0, 0.15);
    border: none;

    display: flex;
    width: 202px;
    padding: 16px 10px;
    justify-content: center;
    align-items: center;
    gap: 10px;
    flex-shrink: 0; 
    color: var(--Primary, #0F1014);
    font-family: Red Hat Display;
    font-size: 18px;
    font-style: normal;
    font-weight: 700;
    line-height: normal; 
  }
  
  .header-titles-buttons-2 {
    border: 1px solid var(--Secondary, #9DDFEE); 
    background: transparent;

    display: flex;
    width: 202px;
    padding: 16px 10px;
    justify-content: center;
    align-items: center;
    gap: 10px;
    flex-shrink: 0;

    color: var(--White, #FFF);
    font-family: Red Hat Display;
    font-size: 18px;
    font-style: normal;
    font-weight: 700;
    line-height: normal; 
  }
@media screen and (max-width: ${breakpoint('compact_desktop')}){
  .header {
    flex-direction: column;
    padding: 0 24px 0 24px;
  }
  .header-imageDiv {
    display: none;
  }
  .titles {
    margin-left: 0;
    margin-top: 40px;
  }
  .header-titles-buttons {
    flex-direction: column;
    justify-content: center;
    align-items: center;
    row-gap: 40px;
    margin-bottom: 56px;
  }
}

`;
export default HomeHeaderStyle
