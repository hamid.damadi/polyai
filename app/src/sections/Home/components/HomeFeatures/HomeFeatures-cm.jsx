import { Children, useState, useRef } from 'react';
import ServiceCard from '../ServiceCard';
import HomeFeaturesStyle from './HomeFeatures-st'
import Carousel from '@itseasy21/react-elastic-carousel'


function HomeFeatures() {

  const infoArr = [
    {
      color: "#D2BFE6",
      icon: "./icons/technology.png",
      title: "Technologies",
      description: "Various technologies have been used in this project so that you can easily add desired parts to it in the future. The React library has been used on the front end, Node.js as a server, and Python as artificial intelligence services back end. Docker is also used for deployment."
    },
    {
      color: "#F4B9CE",
      icon: "./icons/architecture.png",
      title: "Architecture",
      description: "The architecture of this project is microservices. This architecture has been chosen to be able to run different services on it easily and independently. It is enough to develop the desired service and establish a connection between it and the server."
    },
    {
      color: "#F9F89E",
      icon: "./icons/communication.png",
      title: "Communication",
      description: "In this project, we tried to use both the HTTP protocol and gRPC for communication between services. The reason for this is that services of different complexity can be easily implemented in this project."
    },
    {
      color: "#CDF8A5",
      icon: "./icons/simplicity.png",
      title: "Simplicity",
      description: "All parts of this project have been designed and implemented so that they can be easily understood and reproduced. This project is not a production, but a tool to mess with the concepts of artificial intelligence."
    },
  ]

  let [currentColumn, setCurrentColumn] = useState(0)
  let carouselRef = useRef(null)

  return (
    <HomeFeaturesStyle>
      <div className='titles'>
        <h2>
          Features
        </h2>
        <h3>
          A simple microservices project that help you to see some AI applications in a simple form!
        </h3>
      </div>
      <div className='cards-section'>
        <Carousel
          ref={carouselRef}
          defaultValue={currentColumn}
          itemsToShow={1}
          showArrows={false}
          enableMouseSwipe={false}
          renderPagination={({ pages, activePage, onClick }) => {
            return (
              <div className='dots-div'>
                {
                  Children.toArray(
                    pages.map(page => {
                      const isActivePage = activePage === page
                      return (
                        <div
                          className={isActivePage ? 'active-dot' : 'dot'}
                          key={page}
                          onClick={() => {
                            onClick(page)
                            setCurrentColumn(page)
                          }}
                        />
                      )
                    })
                  )
                }
              </div>
            )
          }}
        >
          {
            Children.toArray(
              infoArr.map((e) => (
                <ServiceCard
                  color={e.color}
                  icon={e.icon}
                  title={e.title}
                  description={e.description}
                  path={e.path}
                />
              ))
            )
          }
        </Carousel>
        <div className="btnSection">
          <button className="paginationBtn"
            disabled={currentColumn <= 0}
            onClick={() => {
              currentColumn--
              setCurrentColumn(currentColumn)
              carouselRef.current.goTo(currentColumn)
            }}>
            Previous
          </button>
          <button className="paginationBtn"
            disabled={currentColumn === infoArr.length - 1}
            onClick={() => {
              currentColumn++
              setCurrentColumn(currentColumn)
              carouselRef.current.goTo(currentColumn)
            }}>
            Next
          </button>
        </div>
      </div>
    </HomeFeaturesStyle >

  );
}

export default HomeFeatures;
