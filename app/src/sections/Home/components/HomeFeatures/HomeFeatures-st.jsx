import styled from "styled-components";
import { breakpoint } from "../../../../helpers/theme-helper";

let HomeFeaturesStyle = styled.div`

  width: 100%;
  height: auto;
  background-color: #9DDFEE;
  padding: 64px 64px 146px 64px;

  
  .titles {
    display: flex;
    flex-direction: column;
    max-width: 646px;
  }

  h2 {
    width: 100%;
    color: #0F1014;
    text-align: left;
    /* H2 */
    font-size: 48px;
    font-style: normal;
    font-weight: 700;
    line-height: normal;
    text-transform: uppercase; 
  }

  h3 {
    color: #0F1014;
    font-size: 24px;
    font-style: normal;
    font-weight: 400;
    line-height: 150%; 
    margin-bottom: 100px;
  }

  .cards-section {
    width: 100%;
    display: flex;
    flex-wrap: wrap;
    column-gap: 20px;
    row-gap: 80px;
  }

  .btnSection {
    width: 100%;
    display: flex;
    justify-content: space-between;
  }

  .paginationBtn {
    border: none;
    outline: none;
    background: transparent;

    color: var(--Primary, #0F1014);
    text-align: center;
    font-family: Inter;
    font-size: 16px;
    font-style: normal;
    font-weight: 700;
    line-height: normal; 
  }

  .dots-div {
    width: 100%;
    display: flex;
    justify-content: center;
    column-gap: 8px;
    margin-top: 80px;
  }

  .dot {
    width: 18px;
    height: 18px;
    border-radius: 90px;
    background: #F4B9CE;
    cursor: pointer;
  }

  .active-dot {
    width: 18px;
    height: 18px;
    border-radius: 90px;
    background: #CDF8A5;
  }


@media screen and (max-width: ${breakpoint('compact_desktop')}){
  padding: 64px 24px 146px 24px;

}

`;
export default HomeFeaturesStyle
