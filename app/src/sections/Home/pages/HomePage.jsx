import { Helmet } from "react-helmet";
import { LayoutDashboard } from '../../../generalComponents/Layout'
import Home from '../components/Home'
import { useLocation } from "react-router-dom";

function HomePage() {
  let location = useLocation();

  return (
    <>
      <LayoutDashboard
        activeMenu={'home'}
        menuTitle={'داشبورد'}
        location={location.pathname}
      >
        <Helmet>
          <title>{"Home | Fun With AI | Hamid Damadi"}</title>
          <meta
            name="description"
            content="A simple microservices project that help you to see some AI applications in a simple form!"
          />
        </Helmet>
        <Home />
      </LayoutDashboard>
    </>
  );
}

export default HomePage;
