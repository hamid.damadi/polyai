import { Helmet } from "react-helmet";
import { useLocation } from "react-router-dom";
import { LayoutDashboard } from '../../../generalComponents/Layout'
import HomeIntroductionBooks from "../components/HomeIntroductionBooks";

function IntroductionBooksPage() {
    let location = useLocation();

    return (
        <>
            <LayoutDashboard
                location={location.pathname}
            >
                <Helmet>
                    <title>{"Introduction to Books | Fun With AI | Hamid Damadi"}</title>
                    <meta
                        name="description"
                        content="In this page, we are introducing excellent books in AI."
                    />
                </Helmet>
                <HomeIntroductionBooks />
            </LayoutDashboard>
        </>
    );
}

export default IntroductionBooksPage;
