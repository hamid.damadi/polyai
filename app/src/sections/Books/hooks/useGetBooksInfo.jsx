import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useToasts } from 'react-toast-notifications'

import { getBooksInfo } from "../ducks/books/api";
import { setBooksInfoAction } from "../ducks/books/actions";
import { selectorGetBooksInfo } from "../ducks/books/selectors";

function useGetBooksInfo() {
  let { addToast } = useToasts()
  let [loading, setLoading] = useState(true);
  let dispatch = useDispatch();
  let data = useSelector(selectorGetBooksInfo);

  function fetch() {
    setLoading(true);
    return getBooksInfo()
      .then((response) => {
        dispatch(setBooksInfoAction(response.data.data));
      })
      .catch(({ response }) => {
        let errorMessage =
          response?.data?.metaData?.messageEnglish || "There are some problems!";
        addToast(errorMessage, { appearance: "error" });
      })
      .finally(() => {
        setLoading(false);
      });
  }
  useEffect(() => {
    fetch();
    // eslint-disable-next-line
  }, []);

  return {
    data,
    loading,
    refetch: fetch
  };
}

export default useGetBooksInfo;
