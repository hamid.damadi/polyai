import ActionTypes from "../actionTypes-config";

export function setBooksInfoAction(imageDetectionSamples) {
  return {
    type: ActionTypes.SET_BOOKS_INFO_ACTIONS,
    data: imageDetectionSamples
  };
}
