import ActionTypes from "../actionTypes-config";

// State
const books_init_state = {
  booksInfo: [],
};

//

const books_reducers = {
  [ActionTypes.SET_BOOKS_INFO_ACTIONS](state, action) {
    return {
      ...state,
      booksInfo: action.data
    };
  }
};

//

function booksReducers(state = books_init_state, action) {
  const reducer = books_reducers[action.type];
  return reducer ? reducer(state, action) : state;
}

export { books_init_state };
export default booksReducers;
