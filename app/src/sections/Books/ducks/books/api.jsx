import axios from "axios";
import {
  GET_BOOKS_INFO_API,
} from "../../../../configs/constants-config";

export {
  getBooksInfo,
}


function getBooksInfo() {
  return axios.get(GET_BOOKS_INFO_API, {
    withCredentials: true
  });
}



