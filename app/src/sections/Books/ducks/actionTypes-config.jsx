const ActionTypes = {
  // BOOKS
  SET_BOOKS_INFO_ACTIONS: "BOOKS/SET_BOOKS_INFO_ACTIONS"
};

export default ActionTypes;
