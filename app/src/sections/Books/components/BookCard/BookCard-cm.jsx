import parse from 'html-react-parser';

import BookCardStyle from './BookCard-st'
import { toImageUrl } from '../../../../helpers/general-helper'

function BookCard(props) {

  const { item, index } = props

  const {
    title,
    subTitle,
    image,
    description
  } = item

  return (
    <BookCardStyle>
      <h1>
        {index}. {title}
      </h1>
      <h2>
        {subTitle}
      </h2>
      <div className='img-div'>
        <img
          src={toImageUrl(image)}
          alt=''
          decoding="async"
          loading="lazy"
        >
        </img>
      </div>
      <div>
        {parse(description)}
      </div>
    </BookCardStyle >

  );
}

export default BookCard;
