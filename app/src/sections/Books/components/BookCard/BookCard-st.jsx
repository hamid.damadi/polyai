import styled from "styled-components";
import { breakpoint } from "../../../../helpers/theme-helper";

let BookCardStyle = styled.div`

  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;

  h1 {
    color: #0F1014;
    font-size: 32px !important;
    margin-bottom: 0 !important;
    font-style: normal;
    font-weight: 500 !important;
    line-height: normal;
    text-transform: none !important; 
    text-align: left;
  }

  h2 {
    margin-top: 0 !important;
    padding-left: 36px;
    color: #0F1014;
    font-size: 18px;
    font-style: normal;
    font-weight: 400;
    line-height: 150%; /* 36px */ 
    text-align: left;
    text-align: justify;
    text-justify: inter-word;
  }

  h3 {

  }

  p {
    font-size: 16px;
    font-style: normal;
    font-weight: 400;
    line-height: 150%; /* 36px */ 
    text-align: left;
    text-align: justify;
    text-justify: inter-word;
  }

  .img-div {
    width: 100%;
    height: 400px;
    display: flex;
    justify-content: center;
    align-items: center;
    margin-bottom: 60px;
  }

  .img-div img {
    width: 300px;
    height: 400px;
    object-fit: contain;
  }


@media screen and (max-width: ${breakpoint('compact_desktop')}){

  h1 {
    font-size: 24px !important;
  }

  h2 {
    padding: 0;
  }



}

`;
export default BookCardStyle
