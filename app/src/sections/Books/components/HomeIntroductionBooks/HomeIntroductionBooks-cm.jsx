import { Children } from 'react';

import HomeIntroductionBooksStyle from './HomeIntroductionBooks-st'
import LoadingModal from '../../../../generalComponents/LoadingModal';
import BookCard from '../BookCard'
import useGetBooksInfo from '../../hooks/useGetBooksInfo';

function HomeIntroductionBooks() {

  let { data: books, loading } = useGetBooksInfo()

  return (
    <HomeIntroductionBooksStyle>
      <h1>
        Best Books for Artificial Intelligence
      </h1>
      <p>
        On this page, I will talk about the good books I read about AI. Of course,
        this list is only about the books I&apos;ve read; otherwise, there are other great books out there
        that I haven&apos;t had the pleasure of reading.
        I try to cover different aspects of this field.
        The introduced books have dealt with the issue of artificial intelligence both
        from the academic and scientific aspects as well as from the practical and working aspects.
      </p>
      <p>
        I will try to complete this list over time.
      </p>
      <div className='books-div'>
        {
          Children.toArray(
            books.map((item, index) => (
              <BookCard
                item={item}
                index={index + 1}
              >
              </BookCard>
            ))
          )
        }

      </div>
      {
        (loading) &&

        <LoadingModal isOpen={loading} />
      }
    </HomeIntroductionBooksStyle >

  );
}

export default HomeIntroductionBooks;
