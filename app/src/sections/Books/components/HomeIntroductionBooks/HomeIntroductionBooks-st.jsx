import styled from "styled-components";
import { breakpoint } from "../../../../helpers/theme-helper";

let HomeIntroductionBooksStyle = styled.div`

  width: 100%;
  height: auto;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 0 64px;

  h1 {
    color: #0F1014;
    font-size: 64px;
    font-style: normal;
    font-weight: 700;
    line-height: normal;
    text-transform: none !important; 
    text-align: center;
  }

  h2 {
    color: #0F1014;
    font-size: 24px;
    font-style: normal;
    font-weight: 400;
    line-height: 150%; /* 36px */ 
    text-align: left;
    text-align: justify;
    text-justify: inter-word;
  }

  h3 {

  }

  p {
    padding: 0 !important;
    margin: 8px !important;
    width: 100%;
    font-size: 18px;
    font-style: normal;
    font-weight: 400;
    line-height: 150%; /* 36px */ 
    text-align: left;
    text-align: justify;
    text-justify: inter-word;
  }

  .books-div {
    width: 100%;
    min-height: 100vh;
  }

@media screen and (max-width: ${breakpoint('compact_desktop')}){
  padding: 24px;

  h1 {
    font-size: 48px;
  }

  h2 {
    padding-left: 0;
    padding-right: 0;
    font-size: 18px;
  }



}

`;
export default HomeIntroductionBooksStyle
