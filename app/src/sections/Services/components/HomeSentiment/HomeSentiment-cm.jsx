import { useState } from 'react';
import HomeSentimentStyle from './HomeSentiment-st'
import useGetSentimentPrediction from '../../hooks/useGetSentimentPrediction';
import LoadingModal from '../../../../generalComponents/LoadingModal';

function HomeSentiment() {

  let [sentence, setSentence] = useState("")
  let { refetch, loading, data } = useGetSentimentPrediction({ sentence, doable: false })
  let [reset, setReset] = useState(false)

  return (
    <HomeSentimentStyle>
      <h1 className='title'>
        Sentiment Analysis
      </h1>
      <h2>
        In this section, we are faced with a relatively simple Sentiment Analysis.
        Using the trained embedding technique by the GloVe and training on the aclImdb dataset
        using the recursive neural network, we have reached almost 90% accuracy.
        First, you should write a movie review (max 1000 words), the longer the better,
        and by clicking on prediction, the prediction will be displayed.
      </h2>
      <div className='samples-div'>
        <textarea
          placeholder='Enter your review ...! Max: 1000 words!'
          value={sentence}
          onChange={(e) => {
            setSentence(e.target.value)
          }}
        >

        </textarea>
      </div>
      <div className='response-div'>
        {
          !loading && data && reset && data.length > 0 &&
          <div className='prediction'>
            <p>
              The probability of a positive sense of the review is: <span>{data[0]}</span>
            </p>
            <p>
              The rate of the review is <span>{data[1]}</span> of 10.
            </p>
          </div>
        }
        <div className='buttons-div'>
          <button
            className='predict-btn'
            disabled={loading && !sentence}
            onClick={() => {
              refetch({ sentence, doable: true })
              setReset(true)
            }}
          >
            Predict!
          </button>
        </div>
      </div>
      {
        (loading) &&

        <LoadingModal isOpen={loading} />
      }
    </HomeSentimentStyle >

  );
}

export default HomeSentiment;
