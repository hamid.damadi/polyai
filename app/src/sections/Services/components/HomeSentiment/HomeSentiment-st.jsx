import styled from "styled-components";
import { breakpoint } from "../../../../helpers/theme-helper";

let HomeSentimentStyle = styled.div`

  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  h1 {
    color: #0F1014;
    font-size: 64px;
    font-style: normal;
    font-weight: 700;
    line-height: normal;
    text-align: center;
  }

  h2 {
    color: #0F1014;
    font-size: 24px;
    font-style: normal;
    font-weight: 400;
    line-height: 150%; /* 36px */ 
    text-align: left;
    padding-left: 64px;
    padding-right: 64px;
    text-align: justify;
    text-justify: inter-word;
  }

  .buttons-div {
    width: 100%;
    display: flex;
    justify-content: center;
    margin: 120px 0;
  }

  .predict-btn {
    background: #0F1014; 
    border: none;
    color: #FFFFFF;
    display: flex;
    width: 202px;
    padding: 16px 10px;
    justify-content: center;
    align-items: center;
    gap: 10px;
    flex-shrink: 0;
  }

  .response-div {
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }

  .samples-div {
    display: flex;
    flex-wrap: wrap;
    column-gap: 8px;
    justify-content: center;
    align-items: center;
    width: 100%;
    max-width: 646px;

    background: #9DDFEE;
    height: auto;
    background-color: ${(props) => props.color};
    padding: 64px;
    border-radius: 16px; 
    margin-top: 60px;
  }

  textarea {
    width: 90%;
    height: 180px;
    resize: none;
    padding: 16px;
    text-align: justify;
    text-justify: inter-word;
  }

  span {
    font-size: 18px;
    font-style: normal;
    font-weight: 700;
    line-height: 150%; /* 36px */ 
  }

  .entity {
    font-weight: 700;
  }

  .prediction {
    width: 100%;
    max-width: 646px;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: center;
    margin-top: 64px;

    background: #CDF8A5;
    height: auto;
    background-color: ${(props) => props.color};
    padding: 64px;
    border-radius: 16px; 

    font-size: 18px;
    font-style: normal;
    font-weight: 400;
    line-height: 150%; /* 36px */ 
  }

  table {
    width: 100%;
  }

  thead {
    background: #9DDFEE;
  }

  thead td {
    text-align: center;
  }

  tr:nth-child(even) {
    background-color: #f2f2f2;
  }

  td {
    padding: 8px;
  }


  

@media screen and (max-width: ${breakpoint('compact_desktop')}){
  padding: 24px;

  h1 {
    font-size: 48px;
  }

  h2 {
    padding-left: 0;
    padding-right: 0;
    font-size: 18px;
  }

  textarea {
    width: 100%;
  }

  .samples-div {
    padding: 24px;
  }

  .prediction {
    padding: 24px;
  }
}

`;
export default HomeSentimentStyle
