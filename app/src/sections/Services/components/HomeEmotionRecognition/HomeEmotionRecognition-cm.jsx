import { useState, Children, useEffect } from 'react';
import HomeEmotionRecognitionStyle from './HomeEmotionRecognition-st'
import { toImageUrl } from '../../../../helpers/general-helper'
import LoadingModal from '../../../../generalComponents/LoadingModal';
import useGetEmotionRecognitionPrediction from '../../hooks/useGetEmotionRecognitionPrediction';
import ImageContainer from '../../../../generalComponents/ImageContainer/ImageContainer-cm';

function HomeEmotionRecognition() {

  let { refetch, loading, data } = useGetEmotionRecognitionPrediction({ doable: false })
  let [reset, setReset] = useState(false)

  let [files, setFiles] = useState([])
  let [requestBody, setRequestBody] = useState({})

  useEffect(() => {
    setReset(false)
  }, [files])

  return (
    <HomeEmotionRecognitionStyle>
      <h1 className='title'>
        Emotion Recognition
      </h1>
      <h2>
        In this section, we are faced with a relatively simple emotion recognition. Using CNN on
        the FER2013 dataset, it is possible to recognize people&apos;s facial emotions.
        First, upload your picture and click on prediction,
        the picture with the prediction label will be shown.
      </h2>
      <div className='memory'>
        <h2>In memory of Matthew Perry</h2>
        <h3>For all the sweet moments he created for us!</h3>
        <div className='memory-images'>
          {
            Children.toArray(
              Array.from(Array(6).keys()).map((e) => (
                <img
                  alt=''
                  src={`./webp/chandler-happy-${e + 1}.webp`}
                  decoding="async"
                  loading="lazy"
                >
                </img>
              )
              )
            )
          }
        </div>
      </div>
      <ImageContainer
        files={files}
        setFiles={setFiles}
        requestBody={requestBody}
        setRequestBody={setRequestBody}
        validTypes={['image/jpeg', 'image/jpg', 'image/Jpeg', 'image/png', 'image/GIF', 'image/gif', 'image/webp']}
      >
      </ImageContainer>

      <div className='response-div'>
        {
          !loading && data && data.length > 0 && reset &&
          <div className='prediction'>
            {
              Children.toArray(
                data.map((e) => (
                  <img
                    src={toImageUrl(e.path)}
                    alt=''
                    decoding="async"
                    loading="lazy"
                  >
                  </img>
                ))
              )
            }
          </div>
        }
        <div className='buttons-div'>
          <button
            className='predict-btn'
            disabled={loading || files.length === 0}
            onClick={() => {
              requestBody['doable'] = true
              setRequestBody({ ...requestBody })
              refetch(requestBody)
              setReset(true)
            }}
          >
            Predict!
          </button>
        </div>
      </div>
      {
        (loading) &&

        <LoadingModal isOpen={loading} />
      }
    </HomeEmotionRecognitionStyle >

  );
}

export default HomeEmotionRecognition;
