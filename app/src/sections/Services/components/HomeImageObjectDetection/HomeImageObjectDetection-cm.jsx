import { useState, Children, useEffect } from 'react';
import HomeImageObjectDetectionStyle from './HomeImageObjectDetection-st'
import LoadingModal from '../../../../generalComponents/LoadingModal';
import useGetImageObjectDetectionPrediction from '../../hooks/useGetImageObjectDetectionPrediction';
import ImageContainer from '../../../../generalComponents/ImageContainer/ImageContainer-cm';
import { toImageUrl } from '../../../../helpers/general-helper'

function HomeImageObjectDetection() {

  let { refetch, loading, data } = useGetImageObjectDetectionPrediction({ doable: false })
  let [reset, setReset] = useState(false)

  let [files, setFiles] = useState([])
  let [requestBody, setRequestBody] = useState({})

  useEffect(() => {
    setReset(false)
  }, [files])

  return (
    <HomeImageObjectDetectionStyle>
      <h1 className='title'>
        Image Object Detection
      </h1>
      <h2>
      In this section, we are faced with relatively simple image object detection. By leveraging the Keras_CV library, the model was trained on the COCO-2017 dataset.
        First, upload your picture and click on prediction,
        the image with detected objects will be shown.
      </h2>
      <ImageContainer
        files={files}
        setFiles={setFiles}
        requestBody={requestBody}
        setRequestBody={setRequestBody}
        validTypes={['image/jpeg', 'image/jpg', 'image/Jpeg', 'image/png', 'image/GIF', 'image/gif', 'image/webp']}
      >
      </ImageContainer>

      <div className='response-div'>
        {
          !loading && data && data.length > 0 && reset &&
          <div className='prediction'>
            {
              Children.toArray(
                data.map((e) => (
                  <img
                    src={toImageUrl(e.path)}
                    alt=''
                    decoding="async"
                    loading="lazy"
                  >
                  </img>
                ))
              )
            }
          </div>
        }
        <div className='buttons-div'>
          <button
            className='predict-btn'
            disabled={loading || files.length === 0}
            onClick={() => {
              requestBody['doable'] = true
              setRequestBody({ ...requestBody })
              refetch(requestBody)
              setReset(true)
            }}
          >
            Predict!
          </button>
        </div>
      </div>
      {
        (loading) &&

        <LoadingModal isOpen={loading} />
      }
    </HomeImageObjectDetectionStyle >

  );
}

export default HomeImageObjectDetection;
