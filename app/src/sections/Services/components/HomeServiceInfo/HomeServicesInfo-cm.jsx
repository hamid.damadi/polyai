import { useEffect } from 'react';
import parse from 'html-react-parser';
import HomeServicesInfoStyle from './HomeServicesInfo-st'
import LoadingModal from '../../../../generalComponents/LoadingModal';
import useGetServicesInfo from '../../hooks/useGetServicesInfo';
import { Link } from 'react-router-dom';
import {
  LINK_SERVICE_IMAGE_CAPTION_PAGE,
  LINK_SERVICE_TEXT_RECOGNITION_PAGE,
  LINK_SERVICE_EMOTION_RECOGNITION_PAGE,
  LINK_SERVICE_SENT_PAGE,
  LINK_SERVICE_CGAN_PAGE,
  LINK_SERVICE_NER_PAGE,
  LINK_SERVICE_IMAGE_RECOGNITION_PAGE,
  LINK_SERVICE_IMAGE_OBJECT_DETECTION_PAGE
} from '../../../../configs/constants-config';

function HomeServicesInfo(props) {

  const { query, state } = props
  const { doable } = state ? state : {}

  const linksMap = new Map()
  linksMap.set('Handwritten-Text-Recognition', {
    demo: LINK_SERVICE_TEXT_RECOGNITION_PAGE,
    kaggle: 'https://www.kaggle.com/code/hamiddamadi/handwritten-text-recognition-iam'
  })
  linksMap.set('Image-Captioning', {
    demo: LINK_SERVICE_IMAGE_CAPTION_PAGE,
    kaggle: "https://www.kaggle.com/code/hamiddamadi/image-captioning"
  })
  linksMap.set('Emotion-Recognition', {
    demo: LINK_SERVICE_EMOTION_RECOGNITION_PAGE,
    kaggle: 'https://www.kaggle.com/code/hamiddamadi/emotion-recognition-on-fer2013'
  })
  linksMap.set('Sentiment-Analysis', {
    demo: LINK_SERVICE_SENT_PAGE,
    kaggle: 'https://www.kaggle.com/code/hamiddamadi/sentiment-analysis-of-movie-reviews'
  })
  linksMap.set('Conditional-Generative-Adversarial-Network', {
    demo: LINK_SERVICE_CGAN_PAGE,
    kaggle: 'https://www.kaggle.com/code/hamiddamadi/conditional-gan-on-mnist'
  })
  linksMap.set('Named-Entity-Recognition', {
    demo: LINK_SERVICE_NER_PAGE,
    kaggle: 'https://www.kaggle.com/code/hamiddamadi/named-entity-recognition'
  })
  linksMap.set('Image-Recognition', {
    demo: LINK_SERVICE_IMAGE_RECOGNITION_PAGE,
    kaggle: 'https://www.kaggle.com/code/hamiddamadi/transfer-learning-with-mnist'
  })
  linksMap.set('Image-Object-Detection', {
    demo: LINK_SERVICE_IMAGE_OBJECT_DETECTION_PAGE,
    kaggle: 'https://www.kaggle.com/code/hamiddamadi/keras-cv-object-detection'
  })


  let { loading, data, refetch } = useGetServicesInfo({ service: query?.service, doable: !doable })


  useEffect(() => {
    if (doable) {
      refetch({ service: query?.service, doable: true })
    }
    // eslint-disable-next-line
  }, [query])

  return (
    <HomeServicesInfoStyle>
      <div className='info-header'>
        <h1 className='title'>
          {data && data.title ? data.title : " "}
        </h1>
        <div className='header-titles-buttons'>
          <Link
            className='links'
            to={linksMap.get(data.name)?.demo}
          >
            Live Demo
          </Link>
          <button className='links-2' disabled={!linksMap.get(data.name)?.kaggle}>
            <a
              // className='header-titles-buttons-2'
              href={linksMap.get(data.name)?.kaggle}
              target='_blank'
              rel="noreferrer"
            >
              View in Kaggle
            </a>
          </button>
        </div>
      </div>
      <div className='info-content'>
        {
          !loading && data && data.content &&
          <>
            <div>
              {parse(data.content)}
            </div>
            <div className='header-titles-buttons'>
              <Link
                className='links'
                to={linksMap.get(data.name)?.demo}
                >
                Live Demo
              </Link>
              <button className='links-3' disabled={!linksMap.get(data.name)?.kaggle}>
                <a
                  // className='header-titles-buttons-2'
                  href={linksMap.get(data.name)?.kaggle}
                  target='_blank'
                  rel="noreferrer"
                >
                  View in Kaggle
                </a>
              </button>
            </div>
          </>
        }
      </div>

      {
        (loading) &&
        <LoadingModal isOpen={loading} />
      }
    </HomeServicesInfoStyle >

  );
}

export default HomeServicesInfo;
