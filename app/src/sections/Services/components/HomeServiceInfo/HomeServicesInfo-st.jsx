import styled from "styled-components";
import { breakpoint } from "../../../../helpers/theme-helper";

let HomeServicesInfoStyle = styled.div`

  width: 100%;
  height: auto;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  .info-header {
    width: 100%;
    background: #0F1014
  }

  .header-titles-buttons {
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    column-gap: 40px;
    margin-top: 56px;
    margin-bottom: 64px;
  }

  .links {
    background: var(--Secondary, #9DDFEE);
    box-shadow: 0px 12px 16px 0px rgba(0, 0, 0, 0.15);
    border: none;

    display: flex;
    width: 202px;
    padding: 16px 10px;
    justify-content: center;
    align-items: center;
    gap: 10px;
    flex-shrink: 0; 
    color: var(--Primary, #0F1014) !important;
    text-decoration: none !important;
    font-family: Red Hat Display;
    font-size: 18px;
    font-style: normal;
    font-weight: 700;
    line-height: normal; 
  }

  .links-2 {
    border: 1px solid var(--Secondary, #9DDFEE); 
    background: transparent;

    display: flex;
    width: 202px;
    padding: 16px 10px;
    justify-content: center;
    align-items: center;
    gap: 10px;
    flex-shrink: 0;
  }

  .links-2 a {
    color: var(--White, #FFF) !important;
    font-family: Red Hat Display;
    font-size: 18px;
    font-style: normal;
    font-weight: 700;
    line-height: normal; 
    text-decoration: none !important;
  }

  .links-3 {
    border: 1px solid var(--Secondary, #9DDFEE); 
    background: transparent;

    display: flex;
    width: 202px;
    padding: 16px 10px;
    justify-content: center;
    align-items: center;
    gap: 10px;
    flex-shrink: 0;
  }

  .links-3 a {
    color: var(--Primary, #0F1014) !important;
    font-family: Red Hat Display;
    font-size: 18px;
    font-style: normal;
    font-weight: 700;
    line-height: normal; 
    text-decoration: none !important;
  }

  .title {
    min-height: 100px;
  }

  h1 {
    color: #9DDFEE;
    font-size: 64px;
    font-style: normal;
    font-weight: 700;
    line-height: normal;
    text-align: center;
  }

  h2 {
    color: #0F1014;
    font-size: 24px;
    font-style: normal;
    font-weight: 700;
    line-height: 150%; /* 36px */ 
    text-align: left;
    text-align: justify;
    text-justify: inter-word;
  }

  h3 {
    color: #0F1014;
    font-size: 20px;
    font-style: normal;
    font-weight: 700;
    line-height: 150%; /* 36px */ 
    text-align: center;
    text-align: justify;
    text-justify: inter-word;
  }

  h4 {
    color: #0F1014;
    font-size: 18px;
    font-style: normal;
    font-weight: 700;
    line-height: 150%; /* 36px */ 
    text-align: center;
    text-align: justify;
    text-justify: inter-word;
  }

  .info-content{
    width: 100%;
    min-height: 100vh;
    padding: 64px 256px;
  }

  p {
    font-size: 18px;
    text-align: justify;
    text-justify: inter-word;
  }

  a {
    color: #3600EE !important;
    text-decoration: underline !important;
  }


@media screen and (max-width: ${breakpoint('compact_desktop')}){

  .title {
    min-height: 154px;
  }

  h1 {
    font-size: 48px;
  }

  h2 {
    padding-left: 0;
    padding-right: 0;
    font-size: 18px;
  }

  .header-titles-buttons {
    flex-direction: column;
    row-gap: 40px;
  }

  .info-content{
    padding: 24px;
  }

}

`;
export default HomeServicesInfoStyle
