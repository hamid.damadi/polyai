import { useState, Children } from 'react';
import HomeCGanStyle from './HomeCGan-st'
import { toImageUrl } from '../../../../helpers/general-helper'
import LoadingModal from '../../../../generalComponents/LoadingModal';
import useGetCGanPrediction from '../../hooks/useGetCGanPrediction';

function HomeCGan() {

  let [index, setIndex] = useState()
  let { refetch, loading, data } = useGetCGanPrediction({ index, doable: false })
  let [reset, setReset] = useState(false)

  return (
    <HomeCGanStyle>
      <h1 className='title'>
        Conditional Generative Adversarial Network
      </h1>
      <h2>
        In this section, we are faced with a relatively simple conditional GAN. Using the special training methods on
        the MNIST dataset, it is possible to generate handwritten digits similar to MNIST samples.
        First, you will be shown ten categories, and by selecting a category and clicking on prediction,
        three generated images will be displayed.
      </h2>
      <div className='samples-div'>
        {
          Children.toArray(
            Array.from(Array(10).keys()).map((e) => (
              <label>
                {e}
                <input
                  type='radio'
                  name='indexSelection'
                  value={e}
                  disabled={loading}
                  onChange={() => {
                    setIndex(e)
                    setReset(false)
                  }}
                >
                </input>
              </label>
            ))
          )
        }
      </div>
      <div className='response-div'>
        {
          !loading && data && data.length > 0 && reset &&
          <div className='prediction'>
            {
              Children.toArray(
                data.map((e) => (
                  <img
                    src={toImageUrl(e.path)}
                    alt=''
                    decoding="async"
                    loading="lazy"
                  >
                  </img>
                ))
              )
            }
          </div>
        }
        <div className='buttons-div'>
          <button
            className='predict-btn'
            disabled={loading && !index}
            onClick={() => {
              refetch({ index, doable: true })
              setReset(true)
            }}
          >
            Predict!
          </button>
        </div>
      </div>
      {
        (loading) &&

        <LoadingModal isOpen={loading} />
      }
    </HomeCGanStyle >

  );
}

export default HomeCGan;
