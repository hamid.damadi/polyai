import { useState, Children } from 'react';
import HomeImageRecognitionStyle from './HomeImageRecognition-st'
import useGetImageDetectionPrediction from '../../hooks/useGetImageDetectionPrediction';
import useGetImageDetectionSamples from '../../hooks/useGetImageDetectionSamples';
import { toImageUrl } from '../../../../helpers/general-helper'
import LoadingModal from '../../../../generalComponents/LoadingModal';

function HomeImageRecognition() {

  let [index, setIndex] = useState(0)
  let { refetch: getImagePredictionRefetch, loading: getImagePredictionLoading, data: imagePrediction } = useGetImageDetectionPrediction({ index, doable: false })
  let { data, loading, refetch } = useGetImageDetectionSamples({ doable: false })
  let [reset, setReset] = useState(false)

  return (
    <HomeImageRecognitionStyle>
      <h1 className='title'>
        Image Recognition
      </h1>
      <h2>
        In this section, we are faced with a relatively simple image recognition.
        Using the transfer learning technique by the MobileNetV2 trained model and training on the MNIST dataset
        using fine-tuning method, we have reached 98.9% accuracy.
        First, you will be shown 4 random images from the test dataset that were not used in model training,
        and by selecting an image and clicking on prediction, the result will be displayed.
      </h2>
      <div className='buttons-div'>
        < button
          className='get-samples-btn'
          disabled={loading || getImagePredictionLoading}
          onClick={() => {
            refetch({ doable: true })
            setReset(true)
          }}
        >
          Get Samples!
        </button>
      </div>
      <div className='response-div'>
        {
          !loading && data && data.length > 0 &&
          <div className='samples-div'>
            {
              Children.toArray(
                data.map((e) => (
                  <label>
                    <img
                      src={toImageUrl(e.path)}
                      alt=''
                      decoding="async"
                      loading="lazy"
                      >
                    </img>
                    <input
                      type='radio'
                      name='imageRandomSelection'
                      value={e}
                      disabled={loading || getImagePredictionLoading}
                      onChange={() => {
                        setIndex(e.index)
                      }}
                    >
                    </input>
                  </label>
                ))
              )
            }
          </div>
        }
        {
          !loading && data && data.length > 0 &&
          <div className='buttons-div'>

            <button
              className='predict-btn'
              disabled={getImagePredictionLoading}
              onClick={() => {
                getImagePredictionRefetch({ index, doable: true })
                setReset(false)
              }}
            >
              Predict!
            </button>
          </div>
        }
        {
          imagePrediction && !reset && !loading && !getImagePredictionLoading && imagePrediction?.prediction && imagePrediction?.prediction[imagePrediction?.argMax] === 1 &&
          <div className='prediction'>
            The chosen image is {imagePrediction?.argMax}.
          </div>
        }
        {
          imagePrediction && !reset && !loading && !getImagePredictionLoading && imagePrediction?.prediction && imagePrediction?.prediction[imagePrediction?.argMax] !== 1 &&
          <div className='prediction'>
            The chosen image is {imagePrediction?.argMax} with probablity of {imagePrediction?.prediction[imagePrediction?.argMax]}.
          </div>

        }
      </div>
      {
        (loading || getImagePredictionLoading) &&

        <LoadingModal isOpen={loading || getImagePredictionLoading} />
      }
    </HomeImageRecognitionStyle >

  );
}

export default HomeImageRecognition;
