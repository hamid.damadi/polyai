import { useState, Children } from 'react';
import HomeNERStyle from './HomeNER-st'
import useGetNERPrediction from '../../hooks/useGetNERPrediction';
import useGetNERSamples from '../../hooks/useGetNERSamples';
import LoadingModal from '../../../../generalComponents/LoadingModal';

function HomeNER() {

  let [index,] = useState(0)
  let { refetch: getNERPredictionRefetch, loading: getNERPredictionLoading, data: NERPrediction } = useGetNERPrediction({ index, doable: false })
  let { data, loading, refetch } = useGetNERSamples({ doable: false })
  let [reset, setReset] = useState(false)

  return (
    <HomeNERStyle>
      <h1 className='title'>
        Named Entity Recognition
      </h1>
      <h2>
        In this section, we are faced with a relatively simple named entity recognition.
        Using the trained embedding technique by the GloVe and training on the NER dataset
        using the recursive neural network, we have reached 99.3% accuracy.
        First, you will be shown 1 random sentence from the test dataset that were not used in model training,
        and by clicking on prediction, the result will be displayed.
      </h2>
      <div className='buttons-div'>
        < button
          className='get-samples-btn'
          disabled={loading || getNERPredictionLoading}
          onClick={() => {
            refetch({ doable: true })
            setReset(true)
          }}
        >
          Get Samples!
        </button>
      </div>
      <div className='response-div'>
        {
          !loading && data && data.sentence &&
          <div className='samples-div'>
            {
              Children.toArray(
                data.sentence.map((e, index) => (
                  <div className={NERPrediction?.prediction && !reset && !getNERPredictionLoading && NERPrediction?.prediction[index] !== 'O' ? 'entity' : 'non-entity'}>
                    {e}
                  </div>
                ))
              )
            }
          </div>
        }
        {
          NERPrediction && NERPrediction?.prediction && !loading && !getNERPredictionLoading && !reset &&
          <div className='prediction'>
            <table>
              <thead>
                <tr>
                  <td > Word </td>
                  <td > Entity Prediction </td>
                </tr>
              </thead>
              <tbody>
                {
                  Children.toArray(
                    NERPrediction?.prediction.map((e, index) => (
                      e !== 'O' &&
                      <tr>
                        <td>
                          {data.sentence[index]}
                        </td>
                        <td>
                          {e}
                        </td>
                      </tr>
                    ))
                  )
                }
              </tbody>
            </table>

          </div>
        }
        {
          !loading && data && data.sentence &&
          <div className='buttons-div'>
            <button
              className='predict-btn'
              disabled={getNERPredictionLoading}
              onClick={() => {
                getNERPredictionRefetch({ index: data?.index, doable: true })
                setReset(false)
              }}
            >
              Predict!
            </button>
          </div>
        }
      </div>
      {
        (loading || getNERPredictionLoading) &&

        <LoadingModal isOpen={loading || getNERPredictionLoading} />
      }
    </HomeNERStyle >

  );
}

export default HomeNER;
