import { useState, Children, useEffect } from 'react';
import HomeImageCaptionStyle from './HomeImageCaption-st'
import LoadingModal from '../../../../generalComponents/LoadingModal';
import useGetImageCaptioningPrediction from '../../hooks/useGetImageCaptioningPrediction';
import ImageContainer from '../../../../generalComponents/ImageContainer/ImageContainer-cm';

function HomeImageCaption() {

  let { refetch, loading, data } = useGetImageCaptioningPrediction({ doable: false })
  let [reset, setReset] = useState(false)

  let [files, setFiles] = useState([])
  let [requestBody, setRequestBody] = useState({})

  useEffect(() => {
    setReset(false)
  }, [files])

  return (
    <HomeImageCaptionStyle>
      <h1 className='title'>
        Image Captioning
      </h1>
      <h2>
        In this section, we are faced with a relatively simple image captioning.
        By combining CNN and Transformer,
        the model trained on the COCO-2017 dataset.
        First, upload your picture and click on prediction,
        the caption will be shown.
      </h2>
      <ImageContainer
        files={files}
        setFiles={setFiles}
        requestBody={requestBody}
        setRequestBody={setRequestBody}
        validTypes={['image/jpeg', 'image/jpg', 'image/Jpeg', 'image/png', 'image/GIF', 'image/gif']}
      >
      </ImageContainer>

      <div className='response-div'>
        {
          !loading && data && data.length > 0 && reset &&
          <div className='prediction'>
            {
              Children.toArray(
                data.map((e) => (
                  e
                ))
              )
            }
          </div>
        }
        <div className='buttons-div'>
          <button
            className='predict-btn'
            disabled={loading || files.length === 0}
            onClick={() => {
              requestBody['doable'] = true
              setRequestBody({ ...requestBody })
              refetch(requestBody)
              setReset(true)
            }}
          >
            Predict!
          </button>
        </div>
      </div>
      {
        (loading) &&
        <LoadingModal isOpen={loading} />
      }
    </HomeImageCaptionStyle >

  );
}

export default HomeImageCaption;
