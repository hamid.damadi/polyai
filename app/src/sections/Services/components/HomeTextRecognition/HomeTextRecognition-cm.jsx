import { useState, Children } from 'react';
import HomeTextRecognitionStyle from './HomeTextRecognition-st'
import useGetTextDetectionPrediction from '../../hooks/useGetTextRecognitionPrediction';
import useGetTextRecognitionSamples from '../../hooks/useGetTextRecognitionSamples';
import { toImageUrl } from '../../../../helpers/general-helper'
import LoadingModal from '../../../../generalComponents/LoadingModal';

function HomeTextRecognition() {

  let [path, setPath] = useState('')
  let { refetch: getTextPredictionRefetch, loading: getTextPredictionLoading, data: textPrediction } = useGetTextDetectionPrediction({ path, doable: false })
  let { data, loading, refetch } = useGetTextRecognitionSamples({ doable: false })
  let [reset, setReset] = useState(false)

  return (
    <HomeTextRecognitionStyle>
      <h1 className='title'>
        Handwritten Text Recognition
      </h1>
      <h2>
        In this section, we are faced with a relatively simple Handwritten text recognition.
        By combining CNN and RNN and using the endpoint layer for implementing CTC loss, the model trained model
        on the IAM dataset.
        First, you will be shown 4 random texts from the test dataset that were not used in model training,
        and by selecting a text and clicking on prediction, the result will be displayed.
      </h2>
      <div className='buttons-div'>
        < button
          className='get-samples-btn'
          disabled={loading || getTextPredictionLoading}
          onClick={() => {
            refetch({ doable: true })
            setReset(true)
          }}
        >
          Get Samples!
        </button>
      </div>
      <div className='response-div'>
        {
          !loading && data && data.length > 0 &&
          <div className='samples-div'>
            {
              Children.toArray(
                data.map((e) => (
                  <label>
                    <img
                      src={toImageUrl(e)}
                      alt=''
                      decoding="async"
                      loading="lazy"
                    >
                    </img>
                    <input
                      type='radio'
                      name='TextRandomSelection'
                      value={e}
                      disabled={loading || getTextPredictionLoading}
                      onChange={() => {
                        setPath(e)
                      }}
                    >
                    </input>
                  </label>
                ))
              )
            }
          </div>
        }
        {
          !loading && data && data.length > 0 &&
          <div className='buttons-div'>
            <button
              className='predict-btn'
              disabled={getTextPredictionLoading}
              onClick={() => {
                getTextPredictionRefetch({ path, doable: true })
                setReset(false)
              }}
            >
              Predict!
            </button>
          </div>
        }
        {
          textPrediction && !reset && !loading && !getTextPredictionLoading && textPrediction?.prediction &&
          <div className='prediction'>
            {textPrediction.prediction}
          </div>
        }
      </div>
      {
        (loading || getTextPredictionLoading) &&
        <LoadingModal isOpen={loading || getTextPredictionLoading} />
      }
    </HomeTextRecognitionStyle >

  );
}

export default HomeTextRecognition;
