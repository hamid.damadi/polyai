import styled from "styled-components";
import { breakpoint } from "../../../../helpers/theme-helper";

let HomeImageRecognition = styled.div`

  width: 100%;
  height: auto;

  h1 {
    color: #0F1014;
    font-size: 64px;
    font-style: normal;
    font-weight: 700;
    line-height: normal;
    text-align: center;
  }

  h2 {
    color: #0F1014;
    font-size: 24px;
    font-style: normal;
    font-weight: 400;
    line-height: 150%; /* 36px */ 
    text-align: left;
    padding-left: 64px;
    padding-right: 64px;
    text-align: justify;
    text-justify: inter-word;
  }

  .buttons-div {
    width: 100%;
    display: flex;
    justify-content: center;
    margin: 120px 0;
  }

  .get-samples-btn {
    display: flex;
    width: 202px;
    padding: 16px 10px;
    justify-content: center;
    align-items: center;
    gap: 10px;
    flex-shrink: 0;

    border: 1px solid var(--Secondary, #9DDFEE);
  }

  .predict-btn {
    background: #0F1014; 
    border: none;
    color: #FFFFFF;
    display: flex;
    width: 202px;
    padding: 16px 10px;
    justify-content: center;
    align-items: center;
    gap: 10px;
    flex-shrink: 0;
  }

  .response-div {
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }

  .samples-div {
    display: flex;
    justify-content: center;
    align-items: center;
    column-gap: 24px;
    width: 100%;
    max-width: 646px;

    background: #9DDFEE;
    height: auto;
    background-color: ${(props) => props.color};
    padding: 64px;
    border-radius: 16px; 
  }

  img {
    width: 128px;
    height: 128px;
  }

  label {
    display: flex;
    row-gap: 24px;
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }

  .prediction {
    display: flex;
    justify-content: center;
    align-items: center;
    column-gap: 24px;
    width: 100%;
    max-width: 646px;

    background: #CDF8A5;
    height: auto;
    background-color: ${(props) => props.color};
    padding: 64px;
    border-radius: 16px; 

    font-size: 24px;
    font-style: normal;
    font-weight: 400;
    line-height: 150%; /* 36px */ 
  }

@media screen and (max-width: ${breakpoint('compact_desktop')}){
  padding: 24px;

  h1 {
    font-size: 48px;
  }

  h2 {
    padding-left: 0;
    padding-right: 0;
    font-size: 18px;
  }

  .samples-div {
    flex-direction: column;
    row-gap: 48px;
  }

  .prediction {
    padding: 24px;
  }
}

`;
export default HomeImageRecognition
