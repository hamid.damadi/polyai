import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useToasts } from 'react-toast-notifications'

import { getdTextRecognitionPrediction } from "../ducks/services/api";
import { setGetdTextRecognitionPredictionAction } from "../ducks/services/actions";
import { selectorGetdTextRecognitionPrediction } from "../ducks/services/selectors";

function useGetdTextRecognitionPrediction({ path, doable }) {
  let { addToast } = useToasts()
  let [loading, setLoading] = useState(true);
  let dispatch = useDispatch();
  let data = useSelector(selectorGetdTextRecognitionPrediction);

  function fetch({ path, doable }) {
    if (doable) {
      setLoading(true);
      return getdTextRecognitionPrediction({ path })
        .then((response) => {
          dispatch(setGetdTextRecognitionPredictionAction(response.data.data));
        })
        .catch(({ response }) => {
          let errorMessage =
            response?.data?.metaData?.messageEnglish || "There are some problems!";  
          addToast(errorMessage, { appearance: "error" });
        })
        .finally(() => {
          setLoading(false);
        });
    } else {
      setLoading(false)
    }
  }
  useEffect(() => {
    fetch({ path, doable });
    // eslint-disable-next-line
  }, []);

  return {
    data,
    loading,
    refetch: fetch
  };
}

export default useGetdTextRecognitionPrediction;
