import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useToasts } from 'react-toast-notifications'

import { getServicesInfo } from "../ducks/services/api";
import { setServicesInfoAction } from "../ducks/services/actions";
import { selectorGetServicesInfo } from "../ducks/services/selectors";

function useGetServicesInfo({ service, doable }) {
  let { addToast } = useToasts()
  let [loading, setLoading] = useState(true);
  let dispatch = useDispatch();
  let data = useSelector(selectorGetServicesInfo);

  function fetch({ service, doable }) {
    if (doable) {
      setLoading(true);
      return getServicesInfo({ service })
        .then((response) => {
          dispatch(setServicesInfoAction(response.data.data));
        })
        .catch(({ response }) => {
          let errorMessage =
            response?.data?.metaData?.messageEnglish || "There are some problems!";
          addToast(errorMessage, { appearance: "error" });
        })
        .finally(() => {
          setLoading(false);
        });
    }
  }
  useEffect(() => {
    fetch({ service, doable });
    // eslint-disable-next-line
  }, []);

  return {
    data,
    loading,
    refetch: fetch
  };
}

export default useGetServicesInfo;
