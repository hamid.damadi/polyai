import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useToasts } from 'react-toast-notifications'

import { getImageDetectionPrediction } from "../ducks/services/api";
import { setGetImageDetectionPredictionAction } from "../ducks/services/actions";
import { selectorGetImageDetectionPrediction } from "../ducks/services/selectors";

function useGetImageDetectionPrediction({ index, doable }) {
  let { addToast } = useToasts()
  let [loading, setLoading] = useState(true);
  let dispatch = useDispatch();
  let data = useSelector(selectorGetImageDetectionPrediction);

  function fetch({ index, doable }) {
    if (doable) {
      setLoading(true);
      return getImageDetectionPrediction({ index })
        .then((response) => {
          dispatch(setGetImageDetectionPredictionAction(response.data.data));
        })
        .catch(({ response }) => {
          let errorMessage =
            response?.data?.metaData?.messageEnglish || "There are some problems!";  
          addToast(errorMessage, { appearance: "error" });
        })
        .finally(() => {
          setLoading(false);
        });
    } else {
      setLoading(false)
    }
  }
  useEffect(() => {
    fetch({ index, doable });
    // eslint-disable-next-line
  }, []);

  return {
    data,
    loading,
    refetch: fetch
  };
}

export default useGetImageDetectionPrediction;
