import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useToasts } from 'react-toast-notifications'

import { getSentimentPrediction } from "../ducks/services/api";
import { setGetSentimentPredictionAction } from "../ducks/services/actions";
import { selectorGetSentimentPrediction } from "../ducks/services/selectors";

function useGetSentimentPrediction({ sentence, doable }) {
  let { addToast } = useToasts()
  let [loading, setLoading] = useState(true);
  let dispatch = useDispatch();
  let data = useSelector(selectorGetSentimentPrediction);

  function fetch({ sentence, doable }) {
    if (doable) {
      setLoading(true);
      return getSentimentPrediction({ sentence })
        .then((response) => {
          dispatch(setGetSentimentPredictionAction(response.data.data));
        })
        .catch(({ response }) => {
          let errorMessage =
            response?.data?.metaData?.messageEnglish || "There are some problems!";  
          addToast(errorMessage, { appearance: "error" });
        })
        .finally(() => {
          setLoading(false);
        });
    } else {
      setLoading(false)
    }
  }
  useEffect(() => {
    fetch({ sentence, doable });
    // eslint-disable-next-line
  }, []);

  return {
    data,
    loading,
    refetch: fetch
  };
}

export default useGetSentimentPrediction;
