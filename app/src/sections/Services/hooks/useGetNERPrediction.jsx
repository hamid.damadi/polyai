import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useToasts } from 'react-toast-notifications'

import { getNERPrediction } from "../ducks/services/api";
import { setGetNERPredictionAction } from "../ducks/services/actions";
import { selectorGetNERPrediction } from "../ducks/services/selectors";

function useGetNERPrediction({ index, doable }) {
  let { addToast } = useToasts()
  let [loading, setLoading] = useState(true);
  let dispatch = useDispatch();
  let data = useSelector(selectorGetNERPrediction);

  function fetch({ index, doable }) {
    if (doable) {
      setLoading(true);
      return getNERPrediction({ index })
        .then((response) => {
          dispatch(setGetNERPredictionAction(response.data.data));
        })
        .catch(({ response }) => {
          let errorMessage =
            response?.data?.metaData?.messageEnglish || "There are some problems!";  
          addToast(errorMessage, { appearance: "error" });
        })
        .finally(() => {
          setLoading(false);
        });
    } else {
      setLoading(false)
    }
  }
  useEffect(() => {
    fetch({ index, doable });
    // eslint-disable-next-line
  }, []);

  return {
    data,
    loading,
    refetch: fetch
  };
}

export default useGetNERPrediction;
