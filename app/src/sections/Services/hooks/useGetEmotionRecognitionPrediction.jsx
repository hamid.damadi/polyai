import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useToasts } from 'react-toast-notifications'

import { getEmotionRecognitionPrediction } from "../ducks/services/api";
import { setGetEmotionRecognitionPredictionAction } from "../ducks/services/actions";
import { selectorGetEmotionRecognitionPrediction } from "../ducks/services/selectors";
import { upload } from "../../../ducks/general/api";

function useGetEmotionRecognitionPrediction(body) {
  let { addToast } = useToasts()
  let [loading, setLoading] = useState(true);
  let dispatch = useDispatch();
  let data = useSelector(selectorGetEmotionRecognitionPrediction);

  function fetch(body) {
    if (body.doable) {
      setLoading(true);
      let valsArr = []
      if (body && body.imageForm) {
        for (var value of body.imageForm.values()) {
          valsArr.push(value)
        }
      }
      if (valsArr.length > 0) {
        return upload(body.imageForm)
          .then(() => {
            return getEmotionRecognitionPrediction({ path: body.path })
              .then((response) => {
                dispatch(setGetEmotionRecognitionPredictionAction(response.data.data));
              })
              .catch(({ response }) => {
                let errorMessage =
                  response?.data?.metaData?.messageEnglish || "There are some problems!";
                addToast(errorMessage, { appearance: "error" });
                dispatch(setGetEmotionRecognitionPredictionAction([]))
              })
          })
          .catch(({ response }) => {
            let errorMessage =
              response?.data?.metaData?.messageEnglish || "There are some problems!";
            addToast(errorMessage, { appearance: "error" });
            dispatch(setGetEmotionRecognitionPredictionAction([]))
          })
          .finally(() => {
            setLoading(false);
          });
      }
    } else {
      setLoading(false)
    }
  }
  useEffect(() => {
    fetch(body);
    // eslint-disable-next-line
  }, []);

  return {
    data,
    loading,
    refetch: fetch
  };
}

export default useGetEmotionRecognitionPrediction;
