import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useToasts } from 'react-toast-notifications'

import { getNERSamples } from "../ducks/services/api";
import { setGetNERSamplesAction } from "../ducks/services/actions";
import { selectorGetNERSamples } from "../ducks/services/selectors";

function useGetNERSamples({ doable }) {
  let { addToast } = useToasts()
  let [loading, setLoading] = useState(true);
  let dispatch = useDispatch();
  let data = useSelector(selectorGetNERSamples);

  function fetch({ doable }) {
    if (doable) {
      setLoading(true);
      return getNERSamples()
        .then((response) => {
          dispatch(setGetNERSamplesAction(response.data.data));
        })
        .catch(({ response }) => {
          let errorMessage =
            response?.data?.metaData?.messageEnglish || "There are some problems!";
          addToast(errorMessage, { appearance: "error" });
        })
        .finally(() => {
          setLoading(false);
        });
    } else {
      setLoading(false)
    }
  }
  useEffect(() => {
    fetch({ doable });
    // eslint-disable-next-line
  }, []);

  return {
    data,
    loading,
    refetch: fetch
  };
}

export default useGetNERSamples;
