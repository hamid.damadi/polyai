import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useToasts } from 'react-toast-notifications'

import { getImageObjectDetectionPrediction } from "../ducks/services/api";
import { setGetImageObjectDetectionPredictionAction } from "../ducks/services/actions";
import { selectorGetImageObjectDetectionPrediction } from "../ducks/services/selectors";
import { upload } from "../../../ducks/general/api";

function useGetImageObjectDetectionPrediction(body) {
  let { addToast } = useToasts()
  let [loading, setLoading] = useState(true);
  let dispatch = useDispatch();
  let data = useSelector(selectorGetImageObjectDetectionPrediction);

  function fetch(body) {
    if (body.doable) {
      setLoading(true);
      let valsArr = []
      if (body && body.imageForm) {
        for (var value of body.imageForm.values()) {
          valsArr.push(value)
        }
      }
      if (valsArr.length > 0) {
        return upload(body.imageForm)
          .then(() => {
            return getImageObjectDetectionPrediction({ path: body.path })
              .then((response) => {
                dispatch(setGetImageObjectDetectionPredictionAction(response.data.data));
              })
              .catch(({ response }) => {
                let errorMessage =
                  response?.data?.metaData?.messageEnglish || "There are some problems!";
                addToast(errorMessage, { appearance: "error" });
                dispatch(setGetImageObjectDetectionPredictionAction([]))
              })
          })
          .catch(({ response }) => {
            let errorMessage =
              response?.data?.metaData?.messageEnglish || "There are some problems!";
            addToast(errorMessage, { appearance: "error" });
            dispatch(setGetImageObjectDetectionPredictionAction([]))
          })
          .finally(() => {
            setLoading(false);
          });
      }
    } else {
      setLoading(false)
    }
  }
  useEffect(() => {
    fetch(body);
    // eslint-disable-next-line
  }, []);

  return {
    data,
    loading,
    refetch: fetch
  };
}

export default useGetImageObjectDetectionPrediction;
