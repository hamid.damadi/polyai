import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useToasts } from 'react-toast-notifications'

import { getCGanPrediction } from "../ducks/services/api";
import { setGetCGanPredictionAction } from "../ducks/services/actions";
import { selectorGetCGanPrediction } from "../ducks/services/selectors";

function useGetCGanPrediction({ index, doable }) {
  let { addToast } = useToasts()
  let [loading, setLoading] = useState(true);
  let dispatch = useDispatch();
  let data = useSelector(selectorGetCGanPrediction);

  function fetch({ index, doable }) {
    if (doable) {
      setLoading(true);
      return getCGanPrediction({ index })
        .then((response) => {
          dispatch(setGetCGanPredictionAction(response.data.data));
        })
        .catch(({ response }) => {
          let errorMessage =
            response?.data?.metaData?.messageEnglish || "There are some problems!";  
          addToast(errorMessage, { appearance: "error" });
        })
        .finally(() => {
          setLoading(false);
        });
    } else {
      setLoading(false)
    }
  }
  useEffect(() => {
    fetch({ index, doable });
    // eslint-disable-next-line
  }, []);

  return {
    data,
    loading,
    refetch: fetch
  };
}

export default useGetCGanPrediction;
