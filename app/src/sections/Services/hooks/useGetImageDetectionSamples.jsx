import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useToasts } from 'react-toast-notifications'

import { getImageDetectionSamples } from "../ducks/services/api";
import { setGetImageDetectionSamplesAction } from "../ducks/services/actions";
import { selectorGetImageDetectionSamples } from "../ducks/services/selectors";

function useGetImageDetectionSamples({ doable }) {
  let { addToast } = useToasts()
  let [loading, setLoading] = useState(true);
  let dispatch = useDispatch();
  let data = useSelector(selectorGetImageDetectionSamples);

  function fetch({ doable }) {
    if (doable) {
      setLoading(true);
      return getImageDetectionSamples()
        .then((response) => {
          dispatch(setGetImageDetectionSamplesAction(response.data.data));
        })
        .catch(({ response }) => {
          let errorMessage =
            response?.data?.metaData?.messageEnglish || "There are some problems!";
          addToast(errorMessage, { appearance: "error" });
        })
        .finally(() => {
          setLoading(false);
        });
    } else {
      setLoading(false)
    }
  }
  useEffect(() => {
    fetch({ doable });
    // eslint-disable-next-line
  }, []);

  return {
    data,
    loading,
    refetch: fetch
  };
}

export default useGetImageDetectionSamples;
