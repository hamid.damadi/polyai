import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useToasts } from 'react-toast-notifications'

import { getTextRecognitionSamples } from "../ducks/services/api";
import { setGetTextRecognitionSamplesAction } from "../ducks/services/actions";
import { selectorGetTextRecognitionSamples } from "../ducks/services/selectors";

function useGetTextRecognitionSamples({ doable }) {
  let { addToast } = useToasts()
  let [loading, setLoading] = useState(true);
  let dispatch = useDispatch();
  let data = useSelector(selectorGetTextRecognitionSamples);

  function fetch({ doable }) {
    if (doable) {
      setLoading(true);
      return getTextRecognitionSamples()
        .then((response) => {
          dispatch(setGetTextRecognitionSamplesAction(response.data.data));
        })
        .catch(({ response }) => {
          let errorMessage =
            response?.data?.metaData?.messageEnglish || "There are some problems!";
          addToast(errorMessage, { appearance: "error" });
        })
        .finally(() => {
          setLoading(false);
        });
    } else {
      setLoading(false)
    }
  }
  useEffect(() => {
    fetch({ doable });
    // eslint-disable-next-line
  }, []);

  return {
    data,
    loading,
    refetch: fetch
  };
}

export default useGetTextRecognitionSamples;
