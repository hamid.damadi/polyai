import { Helmet } from "react-helmet";
import { useLocation } from "react-router-dom";
import { LayoutDashboard } from '../../../generalComponents/Layout'
import HomeImageCaption from "../components/HomeImageCaption";

function ImageCaptionPage() {
  let location = useLocation();

  return (
    <>
      <LayoutDashboard
        location={location.pathname}
      >
        <Helmet>
          <title>{"Image Captioning | Fun With AI | Hamid Damadi"}</title>
          <meta
            name="description"
            content="In this page, we are faced with a relatively simple image captioning."
          />
        </Helmet>
        <HomeImageCaption />
      </LayoutDashboard>
    </>
  );
}

export default ImageCaptionPage;
