import { Helmet } from "react-helmet";
import { LayoutDashboard } from '../../../generalComponents/Layout'
import HomeSentiment from '../components/HomeSentiment'
import { useLocation } from "react-router-dom";

function SentimentPage() {
    let location = useLocation();

    return (
        <>
            <LayoutDashboard
                location={location.pathname}
            >
                <Helmet>
                    <title>{"Sentiment Analysis | Fun With AI | Hamid Damadi"}</title>
                    <meta
                        name="description"
                        content="In this page, we are faced with a relatively simple sentiment analysis."
                    />
                </Helmet>
                <HomeSentiment />
            </LayoutDashboard>
        </>
    );
}

export default SentimentPage;
