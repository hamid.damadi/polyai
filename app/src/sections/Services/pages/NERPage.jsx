import { Helmet } from "react-helmet";
import { LayoutDashboard } from '../../../generalComponents/Layout'
import HomeNER from '../components/HomeNER'
import { useLocation } from "react-router-dom";

function NERPage() {
    let location = useLocation();

    return (
        <>
            <LayoutDashboard
                location={location.pathname}
            >
                <Helmet>
                    <title>{"NER | Fun With AI | Hamid Damadi"}</title>
                    <meta
                        name="description"
                        content="In this page, we are faced with a relatively simple named entity recognition."
                    />
                </Helmet>
                <HomeNER />
            </LayoutDashboard>
        </>
    );
}

export default NERPage;
