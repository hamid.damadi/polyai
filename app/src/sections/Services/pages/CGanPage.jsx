import { Helmet } from "react-helmet";
import { useLocation } from "react-router-dom";
import { LayoutDashboard } from '../../../generalComponents/Layout'
import HomeCGan from "../components/HomeCGan";

function CGanPage() {
  let location = useLocation();

  return (
    <>
      <LayoutDashboard
        location={location.pathname}
      >
        <Helmet>
          <title>{"Conditional GAN | Fun With AI | Hamid Damadi"}</title>
          <meta
            name="description"
            content="In this page, we are faced with a relatively simple conditional gan."
          />
        </Helmet>
        <HomeCGan />
      </LayoutDashboard>
    </>
  );
}

export default CGanPage;
