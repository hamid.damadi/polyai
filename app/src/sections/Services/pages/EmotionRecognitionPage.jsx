import { Helmet } from "react-helmet";
import { useLocation } from "react-router-dom";
import { LayoutDashboard } from '../../../generalComponents/Layout'
import HomeEmotionRecognition from "../components/HomeEmotionRecognition";

function EmotionRecognitionPage() {
  let location = useLocation();

  return (
    <>
      <LayoutDashboard
        location={location.pathname}
      >
        <Helmet>
          <title>{"Emotional Recognition | Fun With AI | Hamid Damadi"}</title>
          <meta
            name="description"
            content="In this page, we are faced with a relatively simple emotional recognition."
          />
        </Helmet>
        <HomeEmotionRecognition />
      </LayoutDashboard>
    </>
  );
}

export default EmotionRecognitionPage;
