import { Helmet } from "react-helmet";
import { LayoutDashboard } from '../../../generalComponents/Layout'
import HomeImageRecognition from '../components/HomeImageRecognition'
import { useLocation } from "react-router-dom";

function ImageRecognitionPage() {
  let location = useLocation();

  return (
    <>
      <LayoutDashboard
        location={location.pathname}
      >
        <Helmet>
          <title>{"Image Recognition | Fun With AI | Hamid Damadi"}</title>
          <meta
            name="description"
            content="In this page, we are faced with a relatively simple image recognition."
          />
        </Helmet>
        <HomeImageRecognition />
      </LayoutDashboard>
    </>
  );
}

export default ImageRecognitionPage;
