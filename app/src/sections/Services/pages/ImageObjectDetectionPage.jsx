import { Helmet } from "react-helmet";
import { useLocation } from "react-router-dom";
import { LayoutDashboard } from '../../../generalComponents/Layout'
import HomeImageObjectDetection from "../components/HomeImageObjectDetection";

function ImageObjectDetectionPage() {
  let location = useLocation();

  return (
    <>
      <LayoutDashboard
        location={location.pathname}
      >
        <Helmet>
          <title>{"Image Object Detection | Fun With AI | Hamid Damadi"}</title>
          <meta
            name="description"
            content="In this page, we are faced with a relatively simple image object detection."
          />
        </Helmet>
        <HomeImageObjectDetection />
      </LayoutDashboard>
    </>
  );
}

export default ImageObjectDetectionPage;
