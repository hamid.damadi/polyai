import { Helmet } from "react-helmet";
import qs from 'query-string'
import { useLocation } from "react-router-dom";
import { LayoutDashboard } from '../../../generalComponents/Layout'
import HomeServicesInfo from "../components/HomeServiceInfo";

function ServicesInfoPage() {
  let location = useLocation();
  let query = qs.parse(location.search)


  return (
    <>
      <LayoutDashboard
        location={location.pathname}
      >
        <Helmet>
          <title>{`Introduction | ${query?.service} | Fun With AI | Hamid Damadi`}</title>
          <meta
            name="description"
            content={`In this page, we introduce ${query?.service} service.`}
          />
        </Helmet>
        <HomeServicesInfo
          query={query}
          state={location.state}
        />
      </LayoutDashboard>
    </>
  );
}

export default ServicesInfoPage;
