import { Helmet } from "react-helmet";
import { LayoutDashboard } from '../../../generalComponents/Layout'
import HomeTextRecognition from '../components/HomeTextRecognition'
import { useLocation } from "react-router-dom";

function TextRecognitionPage() {
  let location = useLocation();

  return (
    <>
      <LayoutDashboard
        location={location.pathname}
      >
        <Helmet>
          <title>{"HTR | Fun With AI | Hamid Damadi"}</title>
          <meta
            name="description"
            content="In this page, we are faced with a relatively simple handwritten text recognition."
          />
        </Helmet>
        <HomeTextRecognition />
      </LayoutDashboard>
    </>
  );
}

export default TextRecognitionPage;
