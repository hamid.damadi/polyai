import ActionTypes from "../actionTypes-config";


export function setServicesInfoAction(data) {
  return {
    type: ActionTypes.SET_GET_SERVICES_INFO,
    data
  };
}

export function setGetImageDetectionSamplesAction(imageDetectionSamples) {
  return {
    type: ActionTypes.SET_GET_IMAGE_DETECTION_SAMPLES,
    data: imageDetectionSamples
  };
}

export function setGetImageDetectionPredictionAction(imageDetectionPredection) {
  return {
    type: ActionTypes.SET_GET_IMAGE_DETECTION_PREDEICTIN,
    data: imageDetectionPredection
  };
}

export function setGetNERSamplesAction(data) {
  return {
    type: ActionTypes.SET_GET_NER_SAMPLES,
    data
  };
}

export function setGetNERPredictionAction(data) {
  return {
    type: ActionTypes.SET_GET_NER_PREDEICTIN,
    data
  };
}

export function setGetCGanPredictionAction(data) {
  return {
    type: ActionTypes.SET_GET_CGAN_PREDEICTIN,
    data
  };
}

export function setGetSentimentPredictionAction(data) {
  return {
    type: ActionTypes.SET_GET_SENT_PREDEICTIN,
    data
  };
}

export function setGetEmotionRecognitionPredictionAction(data) {
  return {
    type: ActionTypes.SET_GET_EMOTION_RECOGNITION_PREDEICTIN,
    data
  };
}

export function setGetTextRecognitionSamplesAction(data) {
  return {
    type: ActionTypes.SET_GET_TEXT_RECOGNITION_SAMPLES,
    data
  };
}

export function setGetdTextRecognitionPredictionAction(data) {
  return {
    type: ActionTypes.SET_GET_TEXT_RECOGNITION_PREDEICTIN,
    data
  };
}

export function setGetImageCaptioningPredictionAction(data) {
  return {
    type: ActionTypes.SET_GET_IMAGE_CAPTIONING_PREDEICTIN,
    data
  };
}

export function setGetImageObjectDetectionPredictionAction(data) {
  return {
    type: ActionTypes.SET_GET_IMAGE_OBJECT_DETECTION_PREDEICTIN,
    data
  };
}


