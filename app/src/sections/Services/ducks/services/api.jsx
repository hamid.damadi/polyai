import axios from "axios";
import {
  GET_SERVICE_INFO_API,
  GET_IMAGE_DETECTION_PREDICTION_API,
  GET_IMAGE_DETECTION_SAMPLES_API,
  GET_NER_PREDICTION_API,
  GET_NER_SAMPLES_API,
  GET_CGAN_PREDICTION_API,
  GET_SENT_PREDICTION_API,
  GET_EMOTION_RECOGNITION_PREDICTION_API,
  GET_TEXT_RECOGNITION_SAMPLES_API,
  GET_TEXT_RECOGNITION_PREDICTION_API,
  GET_IMAGE_CAPTIONING_PREDICTION_API,
  GET_IMAGE_OBJECT_DETECTION_PREDICTION_API
} from "../../../../configs/constants-config";

export {
  getServicesInfo,
  getImageDetectionPrediction,
  getImageDetectionSamples,
  getNERPrediction,
  getNERSamples,
  getCGanPrediction,
  getSentimentPrediction,
  getEmotionRecognitionPrediction,
  getdTextRecognitionPrediction,
  getTextRecognitionSamples,
  getImageCaptioningPrediction,
  getImageObjectDetectionPrediction
}

function getServicesInfo(data) {
  return axios.get(GET_SERVICE_INFO_API(data), {
    withCredentials: true
  });
}

function getImageDetectionPrediction(data) {
  return axios.get(GET_IMAGE_DETECTION_PREDICTION_API(data), {
    withCredentials: true
  });
}

function getImageDetectionSamples() {
  return axios.get(GET_IMAGE_DETECTION_SAMPLES_API, {
    withCredentials: true
  });
}

function getNERPrediction(data) {
  return axios.get(GET_NER_PREDICTION_API(data), {
    withCredentials: true
  });
}

function getNERSamples() {
  return axios.get(GET_NER_SAMPLES_API, {
    withCredentials: true
  });
}

function getCGanPrediction(data) {
  return axios.get(GET_CGAN_PREDICTION_API(data), {
    withCredentials: true
  });
}

function getSentimentPrediction(data) {
  return axios.get(GET_SENT_PREDICTION_API(data), {
    withCredentials: true
  });
}

function getEmotionRecognitionPrediction(data) {
  return axios.get(GET_EMOTION_RECOGNITION_PREDICTION_API(data), {
    withCredentials: true
  });
}

function getdTextRecognitionPrediction(data) {
  return axios.get(GET_TEXT_RECOGNITION_PREDICTION_API(data), {
    withCredentials: true
  });
}

function getTextRecognitionSamples() {
  return axios.get(GET_TEXT_RECOGNITION_SAMPLES_API, {
    withCredentials: true
  });
}

function getImageCaptioningPrediction(data) {
  return axios.get(GET_IMAGE_CAPTIONING_PREDICTION_API(data), {
    withCredentials: true
  });
}

function getImageObjectDetectionPrediction(data) {
  return axios.get(GET_IMAGE_OBJECT_DETECTION_PREDICTION_API(data), {
    withCredentials: true
  });
}



