function selectorGetServicesInfo(state) {
    return state.services.serviceInfo
}

function selectorGetImageDetectionSamples(state) {
    return state.services.imageDetectionSamples
}

function selectorGetImageDetectionPrediction(state) {
    return state.services.imageDetectionPrediction
}

function selectorGetNERSamples(state) {
    return state.services.nerSamples
}

function selectorGetNERPrediction(state) {
    return state.services.nerPrediction
}

function selectorGetCGanPrediction(state) {
    return state.services.cGanPrediction
}

function selectorGetSentimentPrediction(state) {
    return state.services.sentPrediction
}

function selectorGetEmotionRecognitionPrediction(state) {
    return state.services.emotionRecognitionPrediction
}

function selectorGetTextRecognitionSamples(state) {
    return state.services.textRecognitionSamples
}

function selectorGetdTextRecognitionPrediction(state) {
    return state.services.textRecognitionPrediction
}

function selectorGetImageCaptioningPrediction(state) {
    return state.services.imageCaptioningPrediction
}

function selectorGetImageObjectDetectionPrediction(state) {
    return state.services.imageObjectDetectionPrediction
}

export {
    selectorGetServicesInfo,
    selectorGetImageDetectionSamples,
    selectorGetImageDetectionPrediction,
    selectorGetNERPrediction,
    selectorGetNERSamples,
    selectorGetCGanPrediction,
    selectorGetSentimentPrediction,
    selectorGetEmotionRecognitionPrediction,
    selectorGetdTextRecognitionPrediction,
    selectorGetTextRecognitionSamples,
    selectorGetImageCaptioningPrediction,
    selectorGetImageObjectDetectionPrediction
};


