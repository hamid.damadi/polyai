import ActionTypes from "../actionTypes-config";

// State
const services_init_state = {
  serviceInfo: {},
  imageDetectionSamples: [],
  imageDetectionPrediction: {},
  nerSamples: {},
  nerPrediction: {},
  cGanPrediction: {},
  sentPrediction: [],
  emotionRecognitionPrediction: [],
  textRecognitionSamples: [],
  textRecognitionPrediction: {},
  imageCaptioningPrediction: [],
  imageObjectDetectionPrediction: []
};

//

const services_reducers = {

  [ActionTypes.SET_GET_SERVICES_INFO](state, action) {
    return {
      ...state,
      serviceInfo: action.data
    };
  },
  [ActionTypes.SET_GET_IMAGE_DETECTION_SAMPLES](state, action) {
    return {
      ...state,
      imageDetectionSamples: action.data
    };
  },
  [ActionTypes.SET_GET_IMAGE_DETECTION_PREDEICTIN](state, action) {
    return {
      ...state,
      imageDetectionPrediction: action.data
    };
  },
  [ActionTypes.SET_GET_NER_SAMPLES](state, action) {
    return {
      ...state,
      nerSamples: action.data
    };
  },
  [ActionTypes.SET_GET_NER_PREDEICTIN](state, action) {
    return {
      ...state,
      nerPrediction: action.data
    };
  },
  [ActionTypes.SET_GET_CGAN_PREDEICTIN](state, action) {
    return {
      ...state,
      cGanPrediction: action.data
    };
  },
  [ActionTypes.SET_GET_SENT_PREDEICTIN](state, action) {
    return {
      ...state,
      sentPrediction: action.data
    };
  },
  [ActionTypes.SET_GET_EMOTION_RECOGNITION_PREDEICTIN](state, action) {
    return {
      ...state,
      emotionRecognitionPrediction: action.data
    };
  },
  [ActionTypes.SET_GET_TEXT_RECOGNITION_SAMPLES](state, action) {
    return {
      ...state,
      textRecognitionSamples: action.data
    };
  },
  [ActionTypes.SET_GET_TEXT_RECOGNITION_PREDEICTIN](state, action) {
    return {
      ...state,
      textRecognitionPrediction: action.data
    };
  },
  [ActionTypes.SET_GET_IMAGE_CAPTIONING_PREDEICTIN](state, action) {
    return {
      ...state,
      imageCaptioningPrediction: action.data
    };
  },
  [ActionTypes.SET_GET_IMAGE_OBJECT_DETECTION_PREDEICTIN](state, action) {
    return {
      ...state,
      imageObjectDetectionPrediction: action.data
    };
  }
};

//

function servicesReducers(state = services_init_state, action) {
  const reducer = services_reducers[action.type];
  return reducer ? reducer(state, action) : state;
}

export { services_init_state };
export default servicesReducers;
