//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// General Info
export const WEBSITE_TITLE = "Fun With AI"
export const SUB_DIR_PATH = 'app'
export const FADE_IN_DURATION = '0.5s'
export const FADE_IN_DURATION_2 = '0.75s'

// export const API = "https://hamiddamadi.ir"
export const API = "http://localhost:3001"

export const BASE_IMAGE = `${API}/aiUser/getImages`


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const UPLOAD_API = `${API}/aiUser/editUpload`



//home-links
export const LINK_HOME_PAGE = `/${SUB_DIR_PATH}/home`
export const LINK_NOT_FOUND = `/${SUB_DIR_PATH}/not-found`



//services-API

export const GET_SERVICE_INFO_API = ({ service }) => `${API}/aiUser/getServicesInfoByName?service=${service}`

export const GET_IMAGE_DETECTION_PREDICTION_API = ({ index }) => `${API}/aiUser/imageRecognitionPredict?index=${index}`
export const GET_IMAGE_DETECTION_SAMPLES_API = `${API}/aiUser/imageRecognitionRandomSelections`

export const GET_NER_PREDICTION_API = ({ index }) => `${API}/aiUser/NERPredict?index=${index}`
export const GET_NER_SAMPLES_API = `${API}/aiUser/NERRandomSelections`

export const GET_CGAN_PREDICTION_API = ({ index }) => `${API}/aiUser/cGanPredict?index=${index}`

export const GET_SENT_PREDICTION_API = ({ sentence }) => `${API}/aiUser/sentPredict?sentence=${sentence}`

export const GET_EMOTION_RECOGNITION_PREDICTION_API = ({ path }) => `${API}/aiUser/emotionRecognitionPredict?path=${path}`

export const GET_TEXT_RECOGNITION_PREDICTION_API = ({ path }) => `${API}/aiUser/textRecognitionPredict?path=${path}`
export const GET_TEXT_RECOGNITION_SAMPLES_API = `${API}/aiUser/textRecognitionRandomSelections`

export const GET_IMAGE_CAPTIONING_PREDICTION_API = ({ path }) => `${API}/aiUser/imageCaptioningPredict?path=${path}`

export const GET_IMAGE_OBJECT_DETECTION_PREDICTION_API = ({ path }) => `${API}/aiUser/imageObjectDetectionPredict?path=${path}`

export const LINK_SERVICES_INFO_PAGE = `/${SUB_DIR_PATH}/serviceInfo`
export const LINK_SERVICE_IMAGE_RECOGNITION_PAGE = `/${SUB_DIR_PATH}/imageDetection`
export const LINK_SERVICE_NER_PAGE = `/${SUB_DIR_PATH}/ner`
export const LINK_SERVICE_CGAN_PAGE = `/${SUB_DIR_PATH}/cgan`
export const LINK_SERVICE_SENT_PAGE = `/${SUB_DIR_PATH}/sentiment`
export const LINK_SERVICE_EMOTION_RECOGNITION_PAGE = `/${SUB_DIR_PATH}/emotionRecognition`
export const LINK_SERVICE_TEXT_RECOGNITION_PAGE = `/${SUB_DIR_PATH}/textRecognition`
export const LINK_SERVICE_IMAGE_CAPTION_PAGE = `/${SUB_DIR_PATH}/imageCaptioning`
export const LINK_SERVICE_IMAGE_OBJECT_DETECTION_PAGE = `/${SUB_DIR_PATH}/imageObjectDetection`

// Books

export const GET_BOOKS_INFO_API = `${API}/books/getBooksInfo`

export const LINK_BOOKS_INTRODUCTION_PAGE = `/${SUB_DIR_PATH}/booksIntroduction`
