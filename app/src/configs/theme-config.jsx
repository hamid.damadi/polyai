let breakpoints = ["489px", "769px", "1024px", "1260px", "1440px"];
breakpoints.mobile = breakpoints[0];
breakpoints.tablet = breakpoints[1];
breakpoints.tablet_landskape = breakpoints[2];
breakpoints.compact_desktop = breakpoints[3];
breakpoints.desktop = breakpoints[4];

const config = {
  colors: {
    blue: {
      first: '#151855',
      second: '#484B7A',
      third: '#131D3F',
      forth: "#56628C"
    },
    white: {
      first: '#FFFFFF',
      second: '#F0F0F0',
      third: '#FAFAFA',
      fourth: '#FBFCFC'
    },
    yellow: {
      first: '#EBB383',
      second: '#F4BB8A'
    },
    gray: {
      first: '#888888',
      second: '#666666',
      third: '#D6D9DB',
      fourth: '#E0E4E8F2',
      fifth: '#E0E4E870',
      sixth: '#DDDDDD',
      seventh: '#B0BDC85C',
      eighth: '#DADADA',
      nineth: 'rgba(70, 71, 107, 0.37)',
      tenth: '#848484',
      eleventh: '#7C7C7C',
      twelvth: '#454545',
      thirteenth: '#A0A0A0'
    },
    pink: {
      first: '#DE6979'
    },
    green: {
      first: '#F9FCFB'
    }
  },
  space: [0],
  fontSizes: {
    xxs: 10,
  },
  height: {
    s: 25,
    m: 36,
    l: 43,
  },
  typography: {
    medium: {

    },
    regular: {

    }
  },

  containerWidth: 1100,
  breakpoints
};

export default config