import { combineReducers } from "redux";

import userReducers, { user_init_state } from './user/reducers'
import servicesReducers, { services_init_state } from '../sections/Services/ducks/services/reducers'
import booksReducers, { books_init_state } from '../sections/Books/ducks/books/reducers'
import localStorage from "redux-persist/es/storage";
import persistReducer from "redux-persist/es/persistReducer";

// State
const init_state = {
  services: services_init_state,
  user: user_init_state,
  books: books_init_state
};

const PERSISTOR_VERSION = 1;


// Persistors
const userPersistConfig = {
  key: "user",
  storage: localStorage,
  version: PERSISTOR_VERSION
};


// Make it ready
const combinedReducer = combineReducers({
  user: persistReducer(userPersistConfig, userReducers),
  services: servicesReducers,
  books: booksReducers
});

export { init_state };
export default combinedReducer;
