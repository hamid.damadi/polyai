
// State
const user_init_state = {

};

//

const user_reducers = {

};

//

function userReducers(state = user_init_state, action) {
  const reducer = user_reducers[action.type];
  return reducer ? reducer(state, action) : state;
}

export { user_init_state };
export default userReducers;
