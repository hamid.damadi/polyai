import axios from "axios";
import {
    UPLOAD_API
} from "../../configs/constants-config";

export {
    upload
};

function upload(data) {
    return axios.post(UPLOAD_API, data, {
        headers: {
            'Content-Type': `multipart/form-data`
        },
        withCredentials: true
    })
}