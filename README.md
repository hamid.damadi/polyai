
# Fun With AI

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

## Table of Contents
- [Description](#description)
- [Requirements](#requirements)
- [Online Demo](#online-demo)
- [Installation and Usage](#installation-and-usage)
- [Datasets](#datasets)
- [Models](#models)
- [Contributing](#contributing)
  - [How to Contribute](#how-to-contribute)
  - [I Have a Question](#i-have-a-question)
- [License](#license)
- [Project Status](#project-status)

## Description

A simple microservices project that help you to see some AI applications in a simple form!

It's obvious that the project is not a commercial production. It is just for practicing AI applications in semi-real-world problems. Just For Practicing!

### Requirements

- node version > 16.20.1

- npm

- docker and docker compose

### Online Demo

You can work with the project online in the following link:

[https://hamiddamadi.ir/app](https://hamiddamadi.ir/app)

## Installation and Usage

You can install and use Fun With AI on your machine very easily! Just follow a few steps:

1. Open .env and complete the environment variables.

2. Run the next command to build and pull necessary docker images and config the whole project:

```
docker compose up
```
  
3. Now go to app folder and run:

```
npm install
```

4. Finally run:

```
npm run dev
```

5. Now you should see the project in localhost:3000 in your browser!


## Datasets

You can download the necessary datasets as follows:

- [MNIST](https://datahub.io/machine-learning/mnist_784)
- [NER](https://www.kaggle.com/datasets/abhinavwalia95/entity-annotated-corpus?select=ner.csv)
- [aclImdb](http://ai.stanford.edu/~amaas/data/sentiment/aclImdb_v1.tar.gz)
- [FER2013](https://www.kaggle.com/datasets/nicolejyt/facialexpressionrecognition)
- [IAM](https://fki.tic.heia-fr.ch/databases/iam-handwriting-database)
- [COCO-2017](https://cocodataset.org/#download)

## Models

Some trained models are used in this project. Each service has a transfer.py file that should be executed to make the required model for the service. After making the model, you can use it without any restrictions. Note that you should run the mentioned files in the ai container.

## Contributing

First off, thanks for taking the time to contribute! ❤️ All types of contributions are encouraged and valued.

1.  Fork the repository.
2.  Create a new branch for your feature or bug fix: `git checkout -b feature/my-feature` or `git checkout -b bugfix/my-bugfix`.
3.  Make your changes and commit them with clear messages.
4.  Push your changes to your fork: `git push origin feature/my-feature`.
5.  Submit a pull request to the `develop` branch.
  
### I Have a Question

If you feel the need to ask a question and need clarification, I recommend the following:

- Open an [Issue](/issues/new).

- Provide as much context as you can about what you're running into.

- Provide context and relevant versions (Node.js, npm, etc.).

I will address the issue as soon as possible.

## License

The MIT License (MIT). © 2023 Fun With AI Contributors.

## Project status

The project has just started! More services will be added to cover a broader range of AI applications and methods. Stay tuned for updates!
