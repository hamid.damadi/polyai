import { Schema, model } from 'mongoose'
import bcrypt from 'bcrypt'


const adminSchema = new Schema({
    name: { type: String },
    lastName: { type: String },
    username: { type: String, required: true, trim: true, unique: 1 },
    isActive: { type: Boolean, required: true, default: true },
    password: { type: String, required: true, minlength: 6, trim: true }
})

// This functions will execute if the password field is modified.
adminSchema.pre('save', function (next) {
    var user: any = this
    if (user.isModified('password')) {
        bcrypt.genSalt(Number(process.env.SALT_I))
            .then((salt) => {
                bcrypt.hash(user.password, salt)
                    .then((hash) => {
                        user.password = hash
                        next()
                    })
                    .catch((err) => {
                        next(err)
                    })
            })
            .catch((err) => {
                next(err)
            })
    } else {
        next()
    }
})

// This method compares the password which is stored in database and
// the password which the user entered. It is used in Login.
adminSchema.methods.comparePassword = function (candidatePassword: string, cb: any) {
    var user: any = this
    bcrypt.compare(candidatePassword, user.password, function (err, isMatch) {
        if (err) return cb(err)
        cb(null, isMatch)
    })
}

adminSchema.methods.comparePasswordPromise = function (candidatePassword: string) {
    return new Promise((resolve, reject) => {
        var user: any = this
        bcrypt.compare(candidatePassword, user.password)
            .then(function (isMatch) {
                resolve(isMatch)
            })
            .catch((err) => {
                reject(err)
            })
    })
}

export const Admin = model('Admin', adminSchema)

