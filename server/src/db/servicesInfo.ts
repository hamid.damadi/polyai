import { Schema, model } from 'mongoose'

const schemaOptions = {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
};

const ServicesInfoSchema = new Schema({
    isShown: { type: Boolean, default: true, require: true },
    name: { type: String, required: true, unique: true },
    title: { type: String, required: true, unique: true },
    content: { type: String, required: true },
    image: { type: String, required: true }
}, schemaOptions)

export const ServicesInfo = model('ServicesInfo', ServicesInfoSchema)