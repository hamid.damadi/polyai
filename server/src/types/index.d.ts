import session from 'express-session';

declare module 'express-session' {
  interface SessionData {
    merchantId?: string | ObjectId;
    adminId?: string | ObjectId;
  }
}

