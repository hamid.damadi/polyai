import { promises as fsp } from 'fs'

export const checkFileExistence = (file: any) => {
    return fsp.access(file)
        .then(() => {
            return true
        })
        .catch((err) => {
            console.log('err: ', err)
            return false
        })
}