import { Request, Response, NextFunction } from 'express'

import _ from 'lodash'
import myError from '../api/myError.js'

export function isAdmin(req: Request, res: Response, next: NextFunction) {
    if (!req.session.adminId) {
        const error = new myError(
            'unauthorized',
            401,
            3,
            'خطا رخ داد!',
            'شما اجازه دسترسی ندارید!'
        )
        next(error)
    } else {
        next()
    }
}