import { Request, Response, NextFunction } from 'express';
import myError from '../api/myError.js';

// Define the type of the callback function
type AsyncHandler = (req: Request, res: Response, next: NextFunction) => void | Promise<void | Response<any, Record<string, any>>>;

export default function tryCatch(cb: AsyncHandler) {
  return async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      await cb(req, res, next);
    } catch (err: any) {
      // Handle MongoServerError for unique field violations
      if (err.name === 'MongoServerError' && err.code === 11000) {
        const perMessage = 'یکی از ورودی‌های منحصر به فرد قبلا توسط کاربر دیگری ثبت شده است.';
        const engMessage = 'Some unique fields are already entered by another user.';
        const error = new myError(
          engMessage,
          400,
          9,
          perMessage,
          'خطا رخ داد'
        );
        next(error);
      } else {
        next(err); // Forward other errors to the next middleware
      }
    }
  };
}