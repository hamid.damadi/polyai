import { Response } from 'express'


export default function successRes (res: Response, message?: string, data?: any, metaData?: any, statusCode = 200)  {
  return res.status(statusCode).json({ success: true, message: message, data: data, metaData: metaData })
}