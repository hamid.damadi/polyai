import {Router} from 'express';
export const booksAdminRoutes = Router()
import tryCatch from '../../../../middlewares/tryCatch.js'
import { isAdmin } from '../../../../middlewares/auth.js'
import { BooksHandler } from '../../handler/BooksAdminHandler.js';



const booksHandler = new BooksHandler()

booksAdminRoutes.get('/getBooksInfo',
    isAdmin,
    tryCatch(async (req, res) => {
        return booksHandler.getBooksInfo(req, res)
    })
)
booksAdminRoutes.post('/createBook',
    isAdmin,
    tryCatch(async (req, res) => {
        return booksHandler.createBook(req, res)
    })
)

booksAdminRoutes.put('/editBook',
    isAdmin,
    tryCatch(async (req, res) => {
        return booksHandler.editBook(req, res)
    })
)