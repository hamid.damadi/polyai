import '../../../envLoader.js';
//@ts-ignore
import supertest from 'supertest-session';
import { expect, use } from 'chai';
import { Response } from 'supertest';
import chaiDatetime from 'chai-datetime';

use(chaiDatetime);

const api = supertest('https://hamiddamadi.ir');

describe('Get/ getBooksInfo', () => {
    it('should return 200!', (done) => {
        api.get(`/books/getBooksInfo`)
            .expect(200)
            .end((err: any, res: Response) => {
                if (err) {
                    done(err);
                }
                done();
            });
    });

});
