import { Request, Response } from 'express';

import _ from 'lodash'
import successRes from "../../../middlewares/response.js"
import { Books } from '../../../db/books.js'


export class BooksHandler {

    getBooksInfo(req: Request, res: Response): Promise<void> {
        return Books.find({ isShown: true }).select({ _id: 0, title: 1, subTitle: 1, image: 1, description: 1 })
            .then((books) => {
                successRes(res, '', books)
            })
            .catch((err: any) => {
                throw err
            })
    }
}