import { Request, Response } from 'express';

import _ from 'lodash'
import successRes from "../../../middlewares/response.js"
import myError from '../../../api/myError.js'
import { Admin } from '../../../db/admins.js'

export class AdminHandler {

    async logOut(req: Request, res: Response): Promise<void> {
        req.session.destroy((err) => console.log(err))
        successRes(res)
    }

    async register(req: Request, res: Response): Promise<void> {
        const username = req.body.username.toLowerCase()
        const body = {
            name: req.body.name,
            lastName: req.body.lastName,
            username: username,
            password: req.body.password,
        }
        return Admin.findOne()
            .then((person) => {
                if (person) {
                    const error = new myError(
                        'The admin already exists!',
                        400,
                        4,
                        'ادمین قبلا ثبت شده است!',
                        'خطا رخ داد'
                    )
                    throw (error)
                } else {
                    const user = new Admin({ ...body })
                    return user.save()
                        .then(() => {
                            successRes(res, 'Registration is done successfully')
                        })
                        .catch((err: any) => {
                            if (err.name === 'MongoError' && err.code === 11000) {
                                const error = new myError(
                                    'The user has registered already!',
                                    400,
                                    9,
                                    'شما قبلا ثبت نام کرده اید!',
                                    'خطا رخ داد'
                                )
                                throw (error)
                            } else {
                                const error = new myError(
                                    'Error happened during the registration!',
                                    500,
                                    9,
                                    'در فرآیند ثبت نام مشکلی پیش آمده است!',
                                    'خطا در سرور'
                                )
                                throw (error)
                            }
                        })
                }
            })
            .catch((err) => {
                throw (err)
            })
    }

    async login(req: Request, res: Response): Promise<void> {
        const username = req.body.username.toLowerCase()
        return Admin.findOne({ username: username })
            .then((person) => {
                if (!person) {
                    const error = new myError(
                        'Email or Password are not valid!',
                        400,
                        8,
                        'ایمیل یا گذرواژه معتبر نیستند!',
                        'خطا رخ داد'
                    )
                    throw (error)
                } else if (person.isActive !== true) {
                    const error = new myError(
                        'The account is not active!',
                        400,
                        18,
                        'حساب کاربری شما غیرفعال شده است!',
                        'خطا رخ داد'
                    )
                    throw (error)
                } else {
                    //@ts-ignore
                    return person.comparePasswordPromise(req.body.password)
                        .then((isMatch: any) => {
                            if (!isMatch) {
                                const error = new myError(
                                    'Email or Password are not valid!',
                                    400,
                                    8,
                                    'ایمیل یا گذرواژه معتبر نیستند!',
                                    'خطا رخ داد'
                                )
                                throw (error)
                            } else {
                                return person.save()
                                    .then(() => {
                                        req.session.adminId = person._id
                                        successRes(res, '')
                                    })
                                    .catch((err: any) => {
                                        throw (err)
                                    })
                            }
                        })
                        .catch((err: any) => {
                            throw (err)
                        })
                }
            })
            .catch((err) => {
                throw (err)
            })
    }

}