import * as express from 'express';
import * as _ from 'lodash'
export const authAdminRoutes = express.Router()
import { isAdmin } from '../../../../middlewares/auth.js'
import { AdminHandler } from '../../handlers/AdminHandler.js'
import tryCatch from '../../../../middlewares/tryCatch.js'


const adminHandlers = new AdminHandler()

authAdminRoutes.post('/register',
    tryCatch((req, res) => {
        return adminHandlers.register(req, res)
    })
)

authAdminRoutes.get('/logout',
    isAdmin,
    tryCatch((req, res) => {
        return adminHandlers.logOut(req, res)
    })
)

authAdminRoutes.post('/login',
    tryCatch((req, res) => {
        return adminHandlers.login(req, res)
    }))











