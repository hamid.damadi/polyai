import { Request, Response } from 'express';

import _ from 'lodash'
import fetch from 'node-fetch';
import fs from 'fs/promises'
import path from 'path'
import successRes from "../../../middlewares/response.js"
import { checkFileExistence } from '../../../api/checkFileExistence.js'
import myError from '../../../api/myError.js'
import { ServicesInfo } from '../../../db/servicesInfo.js'

const rootDir = path.resolve(process.cwd(), process.env.ROOT_DIR as string);

export class GeneralHandler {

    getServicesInfoByName(req: Request, res: Response): Promise<void> {
        const { service } = req.query
        return ServicesInfo.findOne({ name: service }).select({ _id: 0, name: 1, title: 1, content: 1, image: 1 })
            .then((infoPage) => {
                if (infoPage) {
                    successRes(res, '', infoPage)
                } else {
                    const error = new myError(
                        "The topic doesn't exist!",
                        400,
                        11,
                        'چنین عنوانی یافت نشد!',
                        'خطا رخ داد'
                    )
                    throw (error)
                }
            })
            .catch((err: any) => {
                throw err
            })
    }

    imageRecognitionRandomSelections(req: Request, res: Response): Promise<void> {
        let samples: any = []
        return fetch(`${process.env.flaskAPI}/imageRecognitionRandomSelections`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            }
        })
            .catch((err: any) => {
                throw err
            })
            .then((response: any) => response.json())
            .then((response: any) => {
                samples = response?.result
                const resp = response?.result.map((item: any) => {
                    return {
                        path: process.env.GET_SHARE_FOLDER_WITH_FLASK + item?.path,
                        index: item?.index
                    }
                })
                successRes(res, "", resp)
            })
            .catch((err: any) => {
                throw err
            })
            .finally(() => {
                setTimeout(() => {
                    samples.forEach((item: any) => {
                        const pathFile = path.resolve(rootDir, process.env.SHARE_FOLDER_WITH_FLASK + item?.path)
                        checkFileExistence(pathFile)
                            .then((isCheck) => {
                                if (isCheck) {
                                    fs.unlink(pathFile)
                                        .catch((err) => console.log(err))
                                }
                            })
                            .catch((err: any) => {
                                console.log(err)
                            })
                    })
                }, 60 * 1000)
            })
    }

    imageRecognitionPredict(req: Request, res: Response): Promise<void> {
        const { index } = req.query

        return fetch(`${process.env.flaskAPI}/imageRecognitionPredict`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ index }),
        })
            .catch((err: any) => {
                throw err
            })
            .then((response: any) => response.json())
            .then((response: any) => {
                const prediction = response?.prediction[0]?.map((v: any) => Math.round(v * 100) / 100)
                successRes(res, '', { prediction, argMax: response.argMax })
            })
            .catch((err: any) => {
                throw err
            })
    }

    NERRandomSelections(req: Request, res: Response): Promise<void> {
        return fetch(`${process.env.flaskAPI}/getSamplesNER`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            }
        })
            .catch((err: any) => {
                throw err
            })
            .then((response: any) => response.json())
            .then((response: any) => {
                successRes(res, "", response)
            })
            .catch((err: any) => {
                throw err
            })
    }

    NERPredict(req: Request, res: Response): Promise<void> {
        const { index } = req.query

        return fetch(`${process.env.flaskAPI}/NERPredict`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ index }),
        })
            .catch((err: any) => {
                throw err
            })
            .then((response: any) => response.json())
            .then((response: any) => {
                successRes(res, '', response)
            })
            .catch((err: any) => {
                throw err
            })
    }

    cGanPredict(req: Request, res: Response): Promise<void> {
        const { index } = req.query
        let prediction: any = []
        return fetch(`${process.env.flaskAPI}/cGanPredict`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ index }),
        })
            .catch((err: any) => {
                throw err
            })
            .then((response: any) => response.json())
            .then((response: any) => {
                prediction = response?.result
                const resp = response?.result.map((item: any) => {
                    return {
                        path: process.env.GET_SHARE_FOLDER_WITH_FLASK + item?.path,
                        index: item?.index
                    }
                })
                successRes(res, "", resp)
            })
            .catch((err: any) => {
                throw err
            })
            .finally(() => {
                setTimeout(() => {
                    prediction.forEach(async (item: any) => {
                        const pathFile = path.resolve(rootDir, process.env.SHARE_FOLDER_WITH_FLASK + item?.path)
                        checkFileExistence(pathFile)
                            .then((isCheck) => {
                                if (isCheck) {
                                    fs.unlink(pathFile)
                                        .catch((err) => console.log(err))
                                }
                            })
                            .catch((err: any) => {
                                console.log(err)
                            })
                    })
                }, 60 * 1000)
            })
    }

    sentPredict(req: Request, res: Response): Promise<void> {
        const { sentence } = req.query

        return fetch(`${process.env.flaskAPI}/sentPredict`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ sentence }),
        })
            .catch((err: any) => {
                throw err
            })
            .then((response: any) => response.json())
            .then((response: any) => {
                const prediction = response?.result?.map((v: any) => Math.round(v * 100) / 100)
                successRes(res, "", prediction)
            })
            .catch((err: any) => {
                throw err
            })
    }

    emotionRecognitionPredict(req: Request, res: Response): Promise<void> {
        const { path: pathFile } = req.query
        let prediction: any = []
        return fs.readFile(path.resolve(rootDir, `./images/temp/${pathFile}`))
            .then((file) => {
                return fs.writeFile(path.resolve(rootDir, process.env.SHARE_FOLDER_WITH_FLASK as string + pathFile), file)
                    .then(() => {
                        return fetch(`${process.env.flaskAPI}/emotionRecognitionPredict`, {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            body: JSON.stringify({ path }),
                        })
                            .catch((err: any) => {
                                throw err
                            })
                            .then((response: any) => response.json())
                            .then((response: any) => {
                                prediction = response?.result.map((v: any) => {
                                    if (v !== 'None') {
                                        return {
                                            path: `${process.env.GET_SHARE_FOLDER_WITH_FLASK}pred-` + path
                                        }
                                    } else {
                                        const error = new myError(
                                            'There is no detection of a face in the picture!',
                                            400,
                                            11,
                                            'هیچ صورت انسانی در عکس یافت نشد!',
                                            'خطا رخ داد'
                                        )
                                        throw (error)
                                    }
                                })
                                successRes(res, "", prediction)
                            })
                            .catch((err: any) => {
                                throw err
                            })
                    })
                    .catch((err: any) => {
                        throw err
                    })
            })
            .catch((err: any) => {
                throw err
            })
            .finally(() => {
                setTimeout(() => {
                    prediction?.forEach(() => {
                        const newPath = path.resolve(rootDir, `${process.env.SHARE_FOLDER_WITH_FLASK}pred-` + pathFile)
                        checkFileExistence(newPath)
                            .then((isCheck) => {
                                if (isCheck) {
                                    fs.unlink(newPath)
                                        .catch((err) => console.log(err))
                                }
                            })
                            .catch((err: any) => {
                                console.log(err)
                            })
                    })
                    checkFileExistence(path.resolve(rootDir, process.env.SHARE_FOLDER_WITH_FLASK as string + pathFile))
                        .then((isCheck) => {
                            if (isCheck) {
                                fs.unlink(path.resolve(rootDir, process.env.SHARE_FOLDER_WITH_FLASK as string + pathFile))
                                    .catch((err) => console.log(err))
                            }
                        })
                        .catch((err: any) => {
                            console.log(err)
                        })
                }, 60 * 1000)
                checkFileExistence(path.resolve(rootDir, `./images/temp/${pathFile}`))
                    .then((isCheck) => {
                        if (isCheck) {
                            fs.unlink(path.resolve(rootDir, `./images/temp/${pathFile}`))
                                .catch((err) => console.log(err))
                        }
                    })
                    .catch((err: any) => {
                        console.log(err)
                    })
            })

    }

    textRecognitionRandomSelections(req: Request, res: Response): Promise<void> {
        let result: any = []
        const requiredCount = 4;

        return fs.readdir(path.resolve(rootDir, './images/HTR/samples'))
            .then((files: any) => {
                const length = files.length
                while (result.length < requiredCount) {
                    let r: any = Math.floor(Math.random() * length)
                    if (result.indexOf(`HTR/samples/${files[r]}`) === -1) {
                        result.push(`HTR/samples/${files[r]}`)
                    }
                }
                successRes(res, '', result)
            })
            .catch((err: any) => {
                throw err
            })

    }

    textRecognitionPredict(req: Request, res: Response): Promise<void> {
        const { path: pathFile } = req.query
        const random = Math.floor(Math.random() * 1000000)
        const fileName = random + '-' + (pathFile as string).split('/').slice(-1)
        return fs.readFile(path.resolve(rootDir, `./images/${pathFile}`))
            .then((file: any) => {
                return fs.writeFile(path.resolve(rootDir, process.env.SHARE_FOLDER_WITH_FLASK + fileName), file)
                    .then(() => {
                        return fetch(`${process.env.flaskAPI}/textRecognitionPredict`, {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            body: JSON.stringify({ path: fileName }),
                        })
                            .catch((err: any) => {
                                throw err
                            })
                            .then((response: any) => response.json())
                            .then((response: any) => {
                                successRes(res, '', response)
                            })
                            .catch((err: any) => {
                                throw err
                            })
                    })
                    .catch((err: any) => {
                        throw err
                    })
            })
            .catch((err: any) => {
                throw err
            })
            .finally(() => {
                setTimeout(() => {
                    checkFileExistence(path.resolve(rootDir, process.env.SHARE_FOLDER_WITH_FLASK + fileName))
                        .then((isCheck) => {
                            if (isCheck) {
                                fs.unlink(path.resolve(rootDir, process.env.SHARE_FOLDER_WITH_FLASK + fileName))
                                    .catch((err) => console.log(err))
                            }
                        })
                        .catch((err: any) => {
                            console.log(err)
                        })
                }, 60 * 1000)
            })
    }

    imageCaptioningPredict(req: Request, res: Response): Promise<void> {
        const { path: pathFile } = req.query
        return fs.readFile(path.resolve(rootDir, `./images/temp/${pathFile}`))
            .then((file) => {
                return fs.writeFile(path.resolve(rootDir, process.env.SHARE_FOLDER_WITH_FLASK as string + pathFile), file)
                    .then(() => {
                        return fetch(`${process.env.flaskAPI}/imageCaptioningPredict`, {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            body: JSON.stringify({ path }),
                        })
                            .catch((err: any) => {
                                throw err
                            })
                            .then((response: any) => response.json())
                            .then((response: any) => {
                                successRes(res, "", response?.result)
                            })
                            .catch((err: any) => {
                                throw err
                            })
                    })
                    .catch((err: any) => {
                        throw err
                    })
            })
            .catch((err: any) => {
                throw err
            })
            .finally(() => {
                setTimeout(() => {
                    checkFileExistence(path.resolve(rootDir, process.env.SHARE_FOLDER_WITH_FLASK as string + pathFile))
                        .then((isCheck) => {
                            if (isCheck) {
                                fs.unlink(path.resolve(rootDir, process.env.SHARE_FOLDER_WITH_FLASK as string + pathFile))
                                    .catch((err) => console.log(err))
                            }
                        })
                        .catch((err: any) => {
                            console.log(err)
                        })
                }, 60 * 1000)
                checkFileExistence(path.resolve(rootDir, `./images/temp/${pathFile}`))
                    .then((isCheck) => {
                        if (isCheck) {
                            fs.unlink(path.resolve(rootDir, `./images/temp/${pathFile}`))
                                .catch((err) => console.log(err))
                        }
                    })
                    .catch((err: any) => {
                        console.log(err)
                    })
            })

    }

    imageObjectDetectionPredict(req: Request, res: Response): Promise<void> {
        const { path: pathFile } = req.query
        let prediction: any = []
        return fs.readFile(path.resolve(rootDir, `./images/temp/${pathFile}`))
            .then((file) => {
                return fs.writeFile(path.resolve(rootDir, process.env.SHARE_FOLDER_WITH_FLASK as string + pathFile), file)
                    .then(() => {
                        return fetch(`${process.env.flaskAPI}/imageObjectDetectionPredict`, {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            body: JSON.stringify({ path }),
                        })
                            .catch((err: any) => {
                                throw err
                            })
                            .then((response: any) => response.json())
                            .then((response: any) => {
                                prediction = response?.result.map((v: any) => {
                                    if (v !== 'None') {
                                        return {
                                            path: `${process.env.GET_SHARE_FOLDER_WITH_FLASK}pred-` + pathFile
                                        }
                                    } else {
                                        const error = new myError(
                                            'There is no detection of a face in the picture!',
                                            400,
                                            11,
                                            'هیچ صورت انسانی در عکس یافت نشد!',
                                            'خطا رخ داد'
                                        )
                                        throw (error)
                                    }
                                })
                                successRes(res, "", prediction)
                            })
                            .catch((err: any) => {
                                throw err
                            })
                    })
                    .catch((err: any) => {
                        throw err
                    })
            })
            .catch((err: any) => {
                throw err
            })
            .finally(() => {
                setTimeout(() => {
                    prediction?.forEach(() => {
                        const newPath = path.resolve(rootDir, `${process.env.SHARE_FOLDER_WITH_FLASK}pred-` + pathFile)
                        checkFileExistence(newPath)
                            .then((isCheck) => {
                                if (isCheck) {
                                    fs.unlink(newPath)
                                        .catch((err) => console.log(err))
                                }
                            })
                            .catch((err: any) => {
                                console.log(err)
                            })
                    })
                    checkFileExistence(path.resolve(rootDir, process.env.SHARE_FOLDER_WITH_FLASK as string + pathFile))
                        .then((isCheck) => {
                            if (isCheck) {
                                fs.unlink(path.resolve(rootDir, process.env.SHARE_FOLDER_WITH_FLASK as string + pathFile))
                                    .catch((err) => console.log(err))
                            }
                        })
                        .catch((err: any) => {
                            console.log(err)
                        })
                }, 60 * 1000)
                checkFileExistence(path.resolve(rootDir, `./images/temp/${pathFile}`))
                    .then((isCheck) => {
                        if (isCheck) {
                            fs.unlink(path.resolve(rootDir, `./images/temp/${pathFile}`))
                                .catch((err) => console.log(err))
                        }
                    })
                    .catch((err: any) => {
                        console.log(err)
                    })
            })
    }

}