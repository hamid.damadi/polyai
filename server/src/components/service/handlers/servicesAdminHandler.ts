import { Request, Response } from 'express';

import _ from 'lodash'
import successRes from "../../../middlewares/response.js"
import myError from '../../../api/myError.js'

import { ServicesInfo } from '../../../db/servicesInfo.js'

export class ServicesAdminHandler {
    getServicesInfo(req: Request, res: Response): Promise<void> {
        return ServicesInfo.find()
            .then((infoPages) => {
                successRes(res, '', infoPages)
            })
            .catch((err: any) => {
                throw err
            })
    }

    getServicesInfoByName(req: Request, res: Response): Promise<void> {
        const { service } = req.query
        return ServicesInfo.findOne({ name: service })
            .then((infoPage) => {
                if (infoPage) {
                    successRes(res, '', infoPage)
                } else {
                    const error = new myError(
                        "The topic doesn't exist!",
                        400,
                        11,
                        'چنین عنوانی یافت نشد!',
                        'خطا رخ داد'
                    )
                    throw (error)
                }
            })
            .catch((err: any) => {
                throw err
            })
    }

    createServicesInfo(req: Request, res: Response): Promise<void> {
        const {
            name,
            title,
            content,
            image
        } = req.body

        return ServicesInfo.create([{
            name,
            title,
            content,
            image
        }])
            .then(() => {
                successRes(res, '')
            })
            .catch((err: any) => {
                throw err
            })
    }

    editServicesInfo(req: Request, res: Response): Promise<void> {
        const {
            id,
            title,
            content,
            image
        } = req.body

        return ServicesInfo.findById(id)
            .then((infoPage) => {
                if (infoPage) {
                    const body: Record<string, any> = {
                        title,
                        content,
                        image
                    }
                    Object.keys(body).forEach((element) => {
                        if (body[`${element}`] || body[`${element}`] === '' || body[`${element}`] == false) {
                            (infoPage as Record<string, any>)[`${element}`] = body[`${element}`]
                        }
                    })
                    return infoPage.save()
                        .then(() => {
                            successRes(res, '')
                        })
                        .catch((err: any) => {
                            throw err
                        })

                } else {
                    const error = new myError(
                        "The topic doesn't exist!",
                        400,
                        11,
                        'چنین عنوانی یافت نشد!',
                        'خطا رخ داد'
                    )
                    throw (error)
                }

            })
            .catch((err: any) => {
                throw err
            })
    }
}