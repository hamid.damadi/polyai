

/**
*@api {get} /getImages/ Request Upload
*
* @apiVersion 0.0.1
* @apiName getImages
* @apiGroup Upload
*
* @apiParam (Field=value) {String} imageType
* @apiParam (Field=value) {String} imagePath
* @apiParam (Field=value) {String} imageName
*
* @apiSuccess {File} {file}
* @apiSuccessExample Success-Response:
HTTP/1.1 200 OK

 file
*/

