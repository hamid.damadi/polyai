import { Router } from 'express';
export const servicesAdminRoutes = Router()
import tryCatch from '../../../../middlewares/tryCatch.js'
import { isAdmin } from '../../../../middlewares/auth.js'
import { ServicesAdminHandler } from '../../handlers/servicesAdminHandler.js';



const servicesHandler = new ServicesAdminHandler()

servicesAdminRoutes.get('/getServicesInfo',
    isAdmin,
    tryCatch(async (req, res) => {
        return servicesHandler.getServicesInfo(req, res)
    })
)

servicesAdminRoutes.get('/getServicesInfoByName',
    isAdmin,
    tryCatch(async (req, res) => {
        return servicesHandler.getServicesInfoByName(req, res)
    })
)

servicesAdminRoutes.post('/createServicesInfo',
    isAdmin,
    tryCatch(async (req, res) => {
        return servicesHandler.createServicesInfo(req, res)
    })
)

servicesAdminRoutes.put('/editServicesInfo',
    isAdmin,
    tryCatch(async (req, res) => {
        return servicesHandler.editServicesInfo(req, res)
    })
)