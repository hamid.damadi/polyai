import { Router } from 'express';
export const serviceUserRoutes = Router()
import tryCatch from '../../../../middlewares/tryCatch.js'
import { UploadHandler } from '../../handlers/uploadHandler.js'
import { validate } from '../../../../middlewares/validation.js';
import { validationRules } from '../../middlewares/validations.js';
import { GeneralHandler } from '../../handlers/general.js';
import { uploadEdit } from '../../../../middlewares/upload.js'



const uploadHandler = new UploadHandler()
const generalHandler = new GeneralHandler()

serviceUserRoutes.get('/getImages/:imageType/:imagePath/:imageName',
    // isAdmin,
    validationRules('param', 'imageType'),
    validationRules('param', 'imagePath'),
    validationRules('param', 'imageName'),
    validate,
    tryCatch(async (req, res) => {
        return uploadHandler.getImages(req, res)
    })
)

serviceUserRoutes.post('/editUpload',
    uploadEdit,
    tryCatch(async (req, res) => {
        return uploadHandler.editUpload(req, res)
    })
)

serviceUserRoutes.get('/getServicesInfoByName',
    tryCatch(async (req, res) => {
        return generalHandler.getServicesInfoByName(req, res)
    })
)

serviceUserRoutes.get('/imageRecognitionRandomSelections',
    tryCatch(async (req, res) => {
        return generalHandler.imageRecognitionRandomSelections(req, res)
    })
)

serviceUserRoutes.get('/imageRecognitionPredict',
    tryCatch(async (req, res) => {
        return generalHandler.imageRecognitionPredict(req, res)
    })
)

serviceUserRoutes.get('/NERRandomSelections',
    tryCatch(async (req, res) => {
        return generalHandler.NERRandomSelections(req, res)
    })
)

serviceUserRoutes.get('/NERPredict',
    tryCatch(async (req, res) => {
        return generalHandler.NERPredict(req, res)
    })
)

serviceUserRoutes.get('/cGanPredict',
    tryCatch(async (req, res) => {
        return generalHandler.cGanPredict(req, res)
    })
)

serviceUserRoutes.get('/sentPredict',
    tryCatch(async (req, res) => {
        return generalHandler.sentPredict(req, res)
    })
)

serviceUserRoutes.get('/emotionRecognitionPredict',
    tryCatch(async (req, res) => {
        return generalHandler.emotionRecognitionPredict(req, res)
    })
)

serviceUserRoutes.get('/textRecognitionRandomSelections',
    tryCatch(async (req, res) => {
        return generalHandler.textRecognitionRandomSelections(req, res)
    })
)

serviceUserRoutes.get('/textRecognitionPredict',
    tryCatch(async (req, res) => {
        return generalHandler.textRecognitionPredict(req, res)
    })
)

serviceUserRoutes.get('/imageCaptioningPredict',
    tryCatch(async (req, res) => {
        return generalHandler.imageCaptioningPredict(req, res)
    })
)

serviceUserRoutes.get('/imageObjectDetectionPredict',
    tryCatch(async (req, res) => {
        return generalHandler.imageObjectDetectionPredict(req, res)
    })
)



