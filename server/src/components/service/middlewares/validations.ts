import _ from 'lodash'
import { param } from 'express-validator'


export const validationRules: any = (type: string, input: string) => {

    if (type === 'param' && input === 'imageType') {
        return [
            param('imageType').isString().withMessage({ clientCode: 4, statusCode: 422, title: 'خطا رخ داد', messageEnglish: 'imageType is required!', clientMessage: 'مسیر عکس معتبر نیست!' }),
        ]
    } else if (type === 'param' && input === 'imagePath') {
        return [
            param('imagePath').isString().withMessage({ clientCode: 4, statusCode: 422, title: 'خطا رخ داد', messageEnglish: 'imagePath is required!', clientMessage: 'مسیر عکس معتبر نیست!' }),
        ]
    } else if (type === 'param' && input === 'imageName') {
        return [
            param('imageName').isString().withMessage({ clientCode: 4, statusCode: 422, title: 'خطا رخ داد', messageEnglish: 'imageName is required!', clientMessage: 'مسیر عکس معتبر نیست!' }),
        ]
    }
}
