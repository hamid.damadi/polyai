export default class myError extends Error {
    constructor(messageEnglish, statusCode, clientCode, clientMessage, title) {
        super(messageEnglish);
        this.messageEnglish = messageEnglish;
        this.statusCode = statusCode;
        this.clientCode = clientCode;
        this.clientMessage = clientMessage;
        this.title = title;
        Object.setPrototypeOf(this, new.target.prototype); // restore prototype chain
    }
}
