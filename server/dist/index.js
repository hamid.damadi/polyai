import './envLoader.js';
import express from 'express';
const app = express();
import http from 'http';
import session from 'express-session';
import MongoStore from 'connect-mongo';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import helmet from 'helmet';
import { connect } from 'mongoose';
import useragent from 'express-useragent';
import cors from 'cors';
import morgan from 'morgan';
import errorHandler from './middlewares/errorHandler.js';
import { LoggerStream } from './api/logger.js';
const server = http.createServer(app);
const clientPromise = connect(process.env.MONGO_DATABASE, {
    dbName: process.env.MONGO_DATABASE_NAME,
    directConnection: true
})
    .then(async (m) => {
    globalThis.clientConnection = m.connection;
    return m.connection.getClient();
})
    .catch((err) => {
    console.log('Error: ', err);
});
app.use(morgan(':remote-addr ":method :url HTTP/:http-version" :status :res[content-length] :response-time ":referrer" ":user-agent" ', { stream: new LoggerStream() })); //, { "stream": loggerStream() })
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
const corsOptions = {
    credentials: true,
    origin: [
        'http://localhost:3000'
    ],
    methods: [
        'GET',
        'POST',
        'PUT',
        'DELETE'
    ]
};
app.use(cors(corsOptions));
app.use(useragent.express());
app.use(helmet());
app.set('trust proxy', true);
const sess = {
    secret: process.env.SESSION_SECRET,
    resave: true,
    proxy: true,
    saveUninitialized: true,
    rolling: true,
    name: 'sessionId',
    cookie: {
        SameSite: 'None',
        httpOnly: true,
        path: '/',
        maxAge: 1000 * 60 * 60 * 9
    },
    store: MongoStore.create({
        clientPromise,
        dbName: process.env.MONGO_DATABASE_NAME,
    })
};
var sessAdmin = {
    secret: process.env.SESSION_SECRET,
    resave: true,
    proxy: true,
    saveUninitialized: false,
    rolling: true,
    name: 'sessionIdAdmin',
    cookie: {
        SameSite: 'None',
        httpOnly: true,
        path: '/',
        maxAge: 1000 * 60 * 60
    },
    store: MongoStore.create({
        clientPromise,
        dbName: process.env.MONGO_DATABASE_NAME,
    })
};
if (app.get('env') === 'production') {
    app.set('trust proxy', 1); // trust first proxy
    //sess.cookie.secure = true // serve secure cookies
}
let sessionMiddleware = session(sess);
let sessionAdminMiddleware = session(sessAdmin);
import { serviceUserRoutes } from './components/service/routes/user/index.js';
import { booksUserRoutes } from './components/books/routes/user/index.js';
import { booksAdminRoutes } from './components/books/routes/admin/index.js';
import { authAdminRoutes } from './components/auth/routes/admin/index.js';
import { servicesAdminRoutes } from './components/service/routes/admin/index.js';
app.use('/serviceUser', (_, res, next) => {
    res.set('Cross-Origin-Resource-Policy', 'cross-origin'); //same-site, same-origin, and cross-origin
    next();
});
app.use('/serviceUser', sessionMiddleware, serviceUserRoutes);
app.use('/books', sessionMiddleware, booksUserRoutes);
app.use('/adminBooks', sessionAdminMiddleware, booksAdminRoutes);
app.use('/adminAuth', sessionAdminMiddleware, authAdminRoutes);
app.use('/adminServices', sessionAdminMiddleware, servicesAdminRoutes);
app.use(errorHandler);
/**
 * Start Express server.
 */
const port = 3001;
const myServer = server.listen(port, () => {
    console.log("  App is running at http://localhost:%d in %s mode", port);
    console.log("  Press CTRL-C to stop\n");
});
myServer.setTimeout(1 * 60 * 1000);
export default myServer;
