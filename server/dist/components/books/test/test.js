import '../../../envLoader.js';
import supertest from 'supertest-session';
import { use } from 'chai';
import chaiDatetime from 'chai-datetime';
use(chaiDatetime);
const api = supertest('https://hamiddamadi.ir');
describe('Get/ getBooksInfo', () => {
    it('should return 200!', (done) => {
        api.get(`/books/getBooksInfo`)
            .expect(200)
            .end((err, res) => {
            if (err) {
                done(err);
            }
            done();
        });
    });
});
