import { Router } from 'express';
export const booksUserRoutes = Router();
import tryCatch from '../../../../middlewares/tryCatch.js';
import { BooksHandler } from '../../handler/BooksHandler.js';
const booksHandler = new BooksHandler();
booksUserRoutes.get('/getBooksInfo', tryCatch(async (req, res) => {
    return booksHandler.getBooksInfo(req, res);
}));
