import successRes from "../../../middlewares/response.js";
import myError from '../../../api/myError.js';
import { Books } from '../../../db/books.js';
export class BooksHandler {
    getBooksInfo(req, res) {
        return Books.find()
            .then((books) => {
            successRes(res, '', books);
        })
            .catch((err) => {
            throw err;
        });
    }
    createBook(req, res) {
        const { title, subTitle, image, publication, writers, description } = req.body;
        return Books.create([{
                title,
                subTitle,
                image,
                publication,
                writers,
                description
            }])
            .then(() => {
            successRes(res, '');
        })
            .catch((err) => {
            throw err;
        });
    }
    editBook(req, res) {
        const { id, title, subTitle, image, publication, writers, description } = req.body;
        return Books.findById(id)
            .then((book) => {
            if (book) {
                const body = {
                    title,
                    subTitle,
                    image,
                    publication,
                    writers,
                    description
                };
                Object.keys(body).forEach((element) => {
                    if (body[`${element}`] || body[`${element}`] === '' || body[`${element}`] == false) {
                        book[`${element}`] = body[`${element}`];
                    }
                });
                return book.save()
                    .then(() => {
                    successRes(res, '');
                })
                    .catch((err) => {
                    throw err;
                });
            }
            else {
                const error = new myError("The book doesn't exist!", 400, 11, 'چنین کتابی یافت نشد!', 'خطا رخ داد');
                throw (error);
            }
        })
            .catch((err) => {
            throw err;
        });
    }
}
