import successRes from "../../../middlewares/response.js";
import myError from '../../../api/myError.js';
import { ServicesInfo } from '../../../db/servicesInfo.js';
export class ServicesAdminHandler {
    getServicesInfo(req, res) {
        return ServicesInfo.find()
            .then((infoPages) => {
            successRes(res, '', infoPages);
        })
            .catch((err) => {
            throw err;
        });
    }
    getServicesInfoByName(req, res) {
        const { service } = req.query;
        return ServicesInfo.findOne({ name: service })
            .then((infoPage) => {
            if (infoPage) {
                successRes(res, '', infoPage);
            }
            else {
                const error = new myError("The topic doesn't exist!", 400, 11, 'چنین عنوانی یافت نشد!', 'خطا رخ داد');
                throw (error);
            }
        })
            .catch((err) => {
            throw err;
        });
    }
    createServicesInfo(req, res) {
        const { name, title, content, image } = req.body;
        return ServicesInfo.create([{
                name,
                title,
                content,
                image
            }])
            .then(() => {
            successRes(res, '');
        })
            .catch((err) => {
            throw err;
        });
    }
    editServicesInfo(req, res) {
        const { id, title, content, image } = req.body;
        return ServicesInfo.findById(id)
            .then((infoPage) => {
            if (infoPage) {
                const body = {
                    title,
                    content,
                    image
                };
                Object.keys(body).forEach((element) => {
                    if (body[`${element}`] || body[`${element}`] === '' || body[`${element}`] == false) {
                        infoPage[`${element}`] = body[`${element}`];
                    }
                });
                return infoPage.save()
                    .then(() => {
                    successRes(res, '');
                })
                    .catch((err) => {
                    throw err;
                });
            }
            else {
                const error = new myError("The topic doesn't exist!", 400, 11, 'چنین عنوانی یافت نشد!', 'خطا رخ داد');
                throw (error);
            }
        })
            .catch((err) => {
            throw err;
        });
    }
}
