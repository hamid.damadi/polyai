import fs from 'fs';
import path from 'path';
import successRes from "../../../middlewares/response.js";
import myError from '../../../api/myError.js';
const rootDir = path.resolve(process.cwd(), process.env.ROOT_DIR);
export class UploadHandler {
    getImages(req, res) {
        const imageType = req.params.imageType;
        const imagePath = req.params.imagePath;
        const imageName = req.params.imageName;
        const imageFile = fs.readFileSync(path.resolve(rootDir, `./images/${imageType}/${imagePath}/${imageName}`));
        const ext = path.extname(imageName);
        res.contentType(`image/${ext}`);
        res.send(imageFile);
    }
    async editUpload(req, res) {
        if (!req.file && !req.files) {
            const error = new myError('There is no image!', 400, 11, 'هیچ عکسی بارگذاری نشده است!', 'خطا رخ داد');
            throw (error);
        }
        else {
            // const path = `./images/${req.session.merchantId}`
            // const isMerchantExist = await checkFileExistence(path)
            // if (isMerchantExist) {
            // } else {
            successRes(res, '');
            // }
        }
    }
}
