import fetch from 'node-fetch';
import fs from 'fs/promises';
import path from 'path';
import successRes from "../../../middlewares/response.js";
import { checkFileExistence } from '../../../api/checkFileExistence.js';
import myError from '../../../api/myError.js';
import { ServicesInfo } from '../../../db/servicesInfo.js';
const rootDir = path.resolve(process.cwd(), process.env.ROOT_DIR);
export class GeneralHandler {
    getServicesInfoByName(req, res) {
        const { service } = req.query;
        return ServicesInfo.findOne({ name: service }).select({ _id: 0, name: 1, title: 1, content: 1, image: 1 })
            .then((infoPage) => {
            if (infoPage) {
                successRes(res, '', infoPage);
            }
            else {
                const error = new myError("The topic doesn't exist!", 400, 11, 'چنین عنوانی یافت نشد!', 'خطا رخ داد');
                throw (error);
            }
        })
            .catch((err) => {
            throw err;
        });
    }
    imageRecognitionRandomSelections(req, res) {
        let samples = [];
        return fetch(`${process.env.flaskAPI}/imageRecognitionRandomSelections`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            }
        })
            .catch((err) => {
            throw err;
        })
            .then((response) => response.json())
            .then((response) => {
            samples = response?.result;
            const resp = response?.result.map((item) => {
                return {
                    path: process.env.GET_SHARE_FOLDER_WITH_FLASK + item?.path,
                    index: item?.index
                };
            });
            successRes(res, "", resp);
        })
            .catch((err) => {
            throw err;
        })
            .finally(() => {
            setTimeout(() => {
                samples.forEach((item) => {
                    const pathFile = path.resolve(rootDir, process.env.SHARE_FOLDER_WITH_FLASK + item?.path);
                    checkFileExistence(pathFile)
                        .then((isCheck) => {
                        if (isCheck) {
                            fs.unlink(pathFile)
                                .catch((err) => console.log(err));
                        }
                    })
                        .catch((err) => {
                        console.log(err);
                    });
                });
            }, 60 * 1000);
        });
    }
    imageRecognitionPredict(req, res) {
        const { index } = req.query;
        return fetch(`${process.env.flaskAPI}/imageRecognitionPredict`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ index }),
        })
            .catch((err) => {
            throw err;
        })
            .then((response) => response.json())
            .then((response) => {
            const prediction = response?.prediction[0]?.map((v) => Math.round(v * 100) / 100);
            successRes(res, '', { prediction, argMax: response.argMax });
        })
            .catch((err) => {
            throw err;
        });
    }
    NERRandomSelections(req, res) {
        return fetch(`${process.env.flaskAPI}/getSamplesNER`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            }
        })
            .catch((err) => {
            throw err;
        })
            .then((response) => response.json())
            .then((response) => {
            successRes(res, "", response);
        })
            .catch((err) => {
            throw err;
        });
    }
    NERPredict(req, res) {
        const { index } = req.query;
        return fetch(`${process.env.flaskAPI}/NERPredict`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ index }),
        })
            .catch((err) => {
            throw err;
        })
            .then((response) => response.json())
            .then((response) => {
            successRes(res, '', response);
        })
            .catch((err) => {
            throw err;
        });
    }
    cGanPredict(req, res) {
        const { index } = req.query;
        let prediction = [];
        return fetch(`${process.env.flaskAPI}/cGanPredict`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ index }),
        })
            .catch((err) => {
            throw err;
        })
            .then((response) => response.json())
            .then((response) => {
            prediction = response?.result;
            const resp = response?.result.map((item) => {
                return {
                    path: process.env.GET_SHARE_FOLDER_WITH_FLASK + item?.path,
                    index: item?.index
                };
            });
            successRes(res, "", resp);
        })
            .catch((err) => {
            throw err;
        })
            .finally(() => {
            setTimeout(() => {
                prediction.forEach(async (item) => {
                    const pathFile = path.resolve(rootDir, process.env.SHARE_FOLDER_WITH_FLASK + item?.path);
                    checkFileExistence(pathFile)
                        .then((isCheck) => {
                        if (isCheck) {
                            fs.unlink(pathFile)
                                .catch((err) => console.log(err));
                        }
                    })
                        .catch((err) => {
                        console.log(err);
                    });
                });
            }, 60 * 1000);
        });
    }
    sentPredict(req, res) {
        const { sentence } = req.query;
        return fetch(`${process.env.flaskAPI}/sentPredict`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ sentence }),
        })
            .catch((err) => {
            throw err;
        })
            .then((response) => response.json())
            .then((response) => {
            const prediction = response?.result?.map((v) => Math.round(v * 100) / 100);
            successRes(res, "", prediction);
        })
            .catch((err) => {
            throw err;
        });
    }
    emotionRecognitionPredict(req, res) {
        const { path: pathFile } = req.query;
        let prediction = [];
        return fs.readFile(path.resolve(rootDir, `./images/temp/${pathFile}`))
            .then((file) => {
            return fs.writeFile(path.resolve(rootDir, process.env.SHARE_FOLDER_WITH_FLASK + pathFile), file)
                .then(() => {
                return fetch(`${process.env.flaskAPI}/emotionRecognitionPredict`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({ path }),
                })
                    .catch((err) => {
                    throw err;
                })
                    .then((response) => response.json())
                    .then((response) => {
                    prediction = response?.result.map((v) => {
                        if (v !== 'None') {
                            return {
                                path: `${process.env.GET_SHARE_FOLDER_WITH_FLASK}pred-` + path
                            };
                        }
                        else {
                            const error = new myError('There is no detection of a face in the picture!', 400, 11, 'هیچ صورت انسانی در عکس یافت نشد!', 'خطا رخ داد');
                            throw (error);
                        }
                    });
                    successRes(res, "", prediction);
                })
                    .catch((err) => {
                    throw err;
                });
            })
                .catch((err) => {
                throw err;
            });
        })
            .catch((err) => {
            throw err;
        })
            .finally(() => {
            setTimeout(() => {
                prediction?.forEach(() => {
                    const newPath = path.resolve(rootDir, `${process.env.SHARE_FOLDER_WITH_FLASK}pred-` + pathFile);
                    checkFileExistence(newPath)
                        .then((isCheck) => {
                        if (isCheck) {
                            fs.unlink(newPath)
                                .catch((err) => console.log(err));
                        }
                    })
                        .catch((err) => {
                        console.log(err);
                    });
                });
                checkFileExistence(path.resolve(rootDir, process.env.SHARE_FOLDER_WITH_FLASK + pathFile))
                    .then((isCheck) => {
                    if (isCheck) {
                        fs.unlink(path.resolve(rootDir, process.env.SHARE_FOLDER_WITH_FLASK + pathFile))
                            .catch((err) => console.log(err));
                    }
                })
                    .catch((err) => {
                    console.log(err);
                });
            }, 60 * 1000);
            checkFileExistence(path.resolve(rootDir, `./images/temp/${pathFile}`))
                .then((isCheck) => {
                if (isCheck) {
                    fs.unlink(path.resolve(rootDir, `./images/temp/${pathFile}`))
                        .catch((err) => console.log(err));
                }
            })
                .catch((err) => {
                console.log(err);
            });
        });
    }
    textRecognitionRandomSelections(req, res) {
        let result = [];
        const requiredCount = 4;
        return fs.readdir(path.resolve(rootDir, './images/HTR/samples'))
            .then((files) => {
            const length = files.length;
            while (result.length < requiredCount) {
                let r = Math.floor(Math.random() * length);
                if (result.indexOf(`HTR/samples/${files[r]}`) === -1) {
                    result.push(`HTR/samples/${files[r]}`);
                }
            }
            successRes(res, '', result);
        })
            .catch((err) => {
            throw err;
        });
    }
    textRecognitionPredict(req, res) {
        const { path: pathFile } = req.query;
        const random = Math.floor(Math.random() * 1000000);
        const fileName = random + '-' + pathFile.split('/').slice(-1);
        return fs.readFile(path.resolve(rootDir, `./images/${pathFile}`))
            .then((file) => {
            return fs.writeFile(path.resolve(rootDir, process.env.SHARE_FOLDER_WITH_FLASK + fileName), file)
                .then(() => {
                return fetch(`${process.env.flaskAPI}/textRecognitionPredict`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({ path: fileName }),
                })
                    .catch((err) => {
                    throw err;
                })
                    .then((response) => response.json())
                    .then((response) => {
                    successRes(res, '', response);
                })
                    .catch((err) => {
                    throw err;
                });
            })
                .catch((err) => {
                throw err;
            });
        })
            .catch((err) => {
            throw err;
        })
            .finally(() => {
            setTimeout(() => {
                checkFileExistence(path.resolve(rootDir, process.env.SHARE_FOLDER_WITH_FLASK + fileName))
                    .then((isCheck) => {
                    if (isCheck) {
                        fs.unlink(path.resolve(rootDir, process.env.SHARE_FOLDER_WITH_FLASK + fileName))
                            .catch((err) => console.log(err));
                    }
                })
                    .catch((err) => {
                    console.log(err);
                });
            }, 60 * 1000);
        });
    }
    imageCaptioningPredict(req, res) {
        const { path: pathFile } = req.query;
        return fs.readFile(path.resolve(rootDir, `./images/temp/${pathFile}`))
            .then((file) => {
            return fs.writeFile(path.resolve(rootDir, process.env.SHARE_FOLDER_WITH_FLASK + pathFile), file)
                .then(() => {
                return fetch(`${process.env.flaskAPI}/imageCaptioningPredict`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({ path }),
                })
                    .catch((err) => {
                    throw err;
                })
                    .then((response) => response.json())
                    .then((response) => {
                    successRes(res, "", response?.result);
                })
                    .catch((err) => {
                    throw err;
                });
            })
                .catch((err) => {
                throw err;
            });
        })
            .catch((err) => {
            throw err;
        })
            .finally(() => {
            setTimeout(() => {
                checkFileExistence(path.resolve(rootDir, process.env.SHARE_FOLDER_WITH_FLASK + pathFile))
                    .then((isCheck) => {
                    if (isCheck) {
                        fs.unlink(path.resolve(rootDir, process.env.SHARE_FOLDER_WITH_FLASK + pathFile))
                            .catch((err) => console.log(err));
                    }
                })
                    .catch((err) => {
                    console.log(err);
                });
            }, 60 * 1000);
            checkFileExistence(path.resolve(rootDir, `./images/temp/${pathFile}`))
                .then((isCheck) => {
                if (isCheck) {
                    fs.unlink(path.resolve(rootDir, `./images/temp/${pathFile}`))
                        .catch((err) => console.log(err));
                }
            })
                .catch((err) => {
                console.log(err);
            });
        });
    }
    imageObjectDetectionPredict(req, res) {
        const { path: pathFile } = req.query;
        let prediction = [];
        return fs.readFile(path.resolve(rootDir, `./images/temp/${pathFile}`))
            .then((file) => {
            return fs.writeFile(path.resolve(rootDir, process.env.SHARE_FOLDER_WITH_FLASK + pathFile), file)
                .then(() => {
                return fetch(`${process.env.flaskAPI}/imageObjectDetectionPredict`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({ path }),
                })
                    .catch((err) => {
                    throw err;
                })
                    .then((response) => response.json())
                    .then((response) => {
                    prediction = response?.result.map((v) => {
                        if (v !== 'None') {
                            return {
                                path: `${process.env.GET_SHARE_FOLDER_WITH_FLASK}pred-` + pathFile
                            };
                        }
                        else {
                            const error = new myError('There is no detection of a face in the picture!', 400, 11, 'هیچ صورت انسانی در عکس یافت نشد!', 'خطا رخ داد');
                            throw (error);
                        }
                    });
                    successRes(res, "", prediction);
                })
                    .catch((err) => {
                    throw err;
                });
            })
                .catch((err) => {
                throw err;
            });
        })
            .catch((err) => {
            throw err;
        })
            .finally(() => {
            setTimeout(() => {
                prediction?.forEach(() => {
                    const newPath = path.resolve(rootDir, `${process.env.SHARE_FOLDER_WITH_FLASK}pred-` + pathFile);
                    checkFileExistence(newPath)
                        .then((isCheck) => {
                        if (isCheck) {
                            fs.unlink(newPath)
                                .catch((err) => console.log(err));
                        }
                    })
                        .catch((err) => {
                        console.log(err);
                    });
                });
                checkFileExistence(path.resolve(rootDir, process.env.SHARE_FOLDER_WITH_FLASK + pathFile))
                    .then((isCheck) => {
                    if (isCheck) {
                        fs.unlink(path.resolve(rootDir, process.env.SHARE_FOLDER_WITH_FLASK + pathFile))
                            .catch((err) => console.log(err));
                    }
                })
                    .catch((err) => {
                    console.log(err);
                });
            }, 60 * 1000);
            checkFileExistence(path.resolve(rootDir, `./images/temp/${pathFile}`))
                .then((isCheck) => {
                if (isCheck) {
                    fs.unlink(path.resolve(rootDir, `./images/temp/${pathFile}`))
                        .catch((err) => console.log(err));
                }
            })
                .catch((err) => {
                console.log(err);
            });
        });
    }
}
