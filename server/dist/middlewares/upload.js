import multer from 'multer';
import path from 'path';
import myError from '../api/myError.js';
const rootDir = path.resolve(process.cwd(), process.env.ROOT_DIR);
const uploadPath = path.resolve(rootDir, './images/temp');
export const editStorage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, uploadPath);
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
});
export const uploadEdit = multer({
    storage: editStorage,
    fileFilter: function (req, file, callback) {
        var ext = path.extname(file.originalname);
        if (!['.png', '.jpg', '.jpeg', '.gif', '.webp', '.pdf'].includes(ext)) {
            return callback(new myError('Only images are allowed', 400, 5, 'لطفا فقط فایل‌های مجاز عکس را وارد نمایید.', 'خطا رخ داد'));
        }
        callback(null, true);
    },
    limits: {
        // fileSize: 2048 * 2048,
        files: 15,
        fields: 2
    }
}).fields([
    { name: 'images[]', maxCount: 12 },
    { name: 'image', maxCount: 1 }
]);
