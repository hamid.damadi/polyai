import myError from '../api/myError.js';
export default function tryCatch(cb) {
    return async (req, res, next) => {
        try {
            await cb(req, res, next);
        }
        catch (err) {
            // Handle MongoServerError for unique field violations
            if (err.name === 'MongoServerError' && err.code === 11000) {
                const perMessage = 'یکی از ورودی‌های منحصر به فرد قبلا توسط کاربر دیگری ثبت شده است.';
                const engMessage = 'Some unique fields are already entered by another user.';
                const error = new myError(engMessage, 400, 9, perMessage, 'خطا رخ داد');
                next(error);
            }
            else {
                next(err); // Forward other errors to the next middleware
            }
        }
    };
}
