import myError from '../api/myError.js';
export function isAdmin(req, res, next) {
    if (!req.session.adminId) {
        const error = new myError('unauthorized', 401, 3, 'خطا رخ داد!', 'شما اجازه دسترسی ندارید!');
        next(error);
    }
    else {
        next();
    }
}
