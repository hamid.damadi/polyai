import { validationResult } from 'express-validator';
import myError from '../api/myError.js';
export function isEmailValid(mail) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(mail);
}
export function isWebsiteValid(string) {
    const re = /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g;
    return !string || re.test(string);
}
export const isValidMobilePhone = (phone) => {
    return phone.length === 11 && (phone[0] === '0' || phone[0] === '۰') && (phone[1] === '9' || phone[1] === '۹') && (/^\d+$/.test(phone) || /^\d+$/.test(numbersFormatter(phone, 'en')));
};
// A helper for converting non-persian numbers to persian ones.
let arabicNumbers = ["١", "٢", "٣", "٤", "٥", "٦", "٧", "٨", "٩", "٠"], persianNumbers = ["۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹", "۰"], englishNumbers = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"];
function searchAndReplaceInNumbers(value, source, target) {
    for (let i = 0, len = target.length; i < len; i++) {
        value = value.replace(new RegExp(source[i], "g"), target[i]);
    }
    return value;
}
export const commaSeparator = (num) => {
    return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};
export function numbersFormatter(value, to = "fa") {
    value = typeof value === "number" ? String(value) : value;
    if (!value)
        return value;
    let output = value;
    if (to === "fa") {
        output = searchAndReplaceInNumbers(output, englishNumbers, persianNumbers);
        output = searchAndReplaceInNumbers(output, arabicNumbers, persianNumbers);
    }
    else if (to === "en") {
        output = searchAndReplaceInNumbers(output, persianNumbers, englishNumbers);
        output = searchAndReplaceInNumbers(output, arabicNumbers, englishNumbers);
    }
    return output;
}
export function priceFormatter(val) {
    return numbersFormatter(commaSeparator(String(val)), "fa");
}
export const validate = (req, res, next) => {
    const Result = validationResult(req);
    if (!Result["errors"] || Result["errors"].length === 0) {
        next();
    }
    else {
        const error = new myError(Result["errors"][0].msg.messageEnglish, Result["errors"][0].msg.statusCode, Result["errors"][0].msg.clientCode, Result["errors"][0].msg.clientMessage, Result["errors"][0].msg.title);
        next(error);
    }
};
