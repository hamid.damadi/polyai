import { Schema, model } from 'mongoose';
const schemaOptions = {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
};
const BooksSchema = new Schema({
    isShown: { type: Boolean, default: true, require: true },
    title: { type: String, required: true, unique: true },
    subTitle: { type: String, required: true },
    image: { type: String, required: true },
    description: { type: String, required: true },
    writers: [{ type: String }],
    publication: {
        name: { type: String },
        date: { type: String }
    }
}, schemaOptions);
export const Books = model('Books', BooksSchema);
